<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Perfil de usuario
            <small>Gestión</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Items</li>
        </ol>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-3">
    			<div class="box box-primary">
            		<div class="box-body box-profile">
              			<img class="profile-user-img img-responsive img-circle" id="imagen">
              			<div class="text-center">
              				<label class="btn btn-primary btn-xs" for="foto">
                        		<input id="foto" onChange="cargarImagen()" type="file" style="display:none;"/><i class="fa fa-upload" aria-hidden="true"></i>
                        	</label>	
              			</div>              			
                        <h3 class="profile-username text-center" id="perfil_nombre"></h3>
              			<p class="text-muted text-center" id="perfil_profesion"></p>
              			<hr>
              			<strong>
              				<i class="fa fa-user margin-r-5"></i> Usuario
              			</strong>
              			<p class="text-muted" id="perfil_login"></p>
              			<hr>
              			<strong>
              				<i class="fa fa-asterisk margin-r-5"></i> Perfil
              			</strong>
              			<p class="text-muted" id="perfil_perfil"></p>
              			<hr>
              			<strong>
              				<i class="fa fa-phone margin-r-5"></i> Telefono
              			</strong>
              			<p class="text-muted" id="perfil_telefono"></p>
              			<hr>
              			<strong>
              				<i class="fa fa-pencil margin-r-5"></i> Sobre mí
              			</strong>
              			<p class="text-muted" id="perfil_sobre_mi"></p>
            		</div>
          		</div>
    		</div>
    		<div class="col-md-9">
    			<div class="nav-tabs-custom">
            		<ul class="nav nav-tabs">
              			<li class="active">
              				<a href="#tab_1" data-toggle="tab">Perfil</a>
              			</li>
              			<li>
              				<a href="#tab_2" data-toggle="tab">Cambiar clave</a>
              			</li>
            		</ul>
            		<div class="tab-content">
              			<div class="tab-pane active" id="tab_1">
		                	<form id="formularioPerfil">
	                            <div class="form-group">
	                                <label for="nombre" class="control-label">Nombre</label>
	                                <input id="nombre" name="nombre" class="form-control" type="text" required>
	                            </div>
	                            <div class="row">
                        			<div class="col-md-4">
			                            <div class="form-group">
			                                <label for="tipo_doc" class="control-label">Tipo documento</label>
			                                <select id="tipo_doc" name="tipo_doc" class="form-control" required>
					                            <option value="">Seleccione...</option>
					                            <option value="CC">CC</option>
					                            <option value="CE">CE</option>
                                                <option value="Pasaporte">Pasaporte</option>
					                        </select>
			                            </div>
			                        </div>
			                        <div class="col-md-8">
			                        	<div class="form-group">
			                                <label for="numero" class="control-label">Número</label>
			                                <input id="numero" name="numero" class="form-control" type="number" required>
			                            </div>
			                        </div>
			                    </div>
			                    <div class="row">
                        			<div class="col-md-4">
			                            <div class="form-group">
			                                <label for="telefono" class="control-label">Telefono</label>
			                                <input id="telefono" name="telefono" class="form-control" type="number">
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                        	<div class="form-group">
			                                <label for="correo" class="control-label">Correo</label>
			                                <input id="correo" name="correo" class="form-control" type="email">
			                            </div>
			                        </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="skype" class="control-label">Skype</label>
                                            <input id="skype" name="skype" class="form-control" type="text">
                                        </div>
                                    </div>
			                    </div>
			                    <div class="row">
                        			<div class="col-md-6">
			                            <div class="form-group">
			                                <label for="genero" class="control-label">Genero</label>
			                                <select id="genero" name="genero" class="form-control">
					                            <option value="">Seleccione...</option>
					                            <option value="Femenino">Femenino</option>
					                            <option value="Masculino">Masculino</option>
					                        </select>
			                            </div>
			                        </div>
			                        <div class="col-md-6">
			                        	<div class="form-group">
			                                <label for="fecha_nacimiento" class="control-label">Fecha nacimiento</label>
			                                <input id="fecha_nacimiento" name="fecha_nacimiento" class="form-control calendario" type="text">
			                            </div>
			                        </div>
			                    </div>
			                    <div class="form-group">
	                                <label for="profesion" class="control-label">Profesión</label>
	                                <input id="profesion" name="profesion" class="form-control" type="text">
	                            </div>
	                            <div class="form-group">
                        			<label for="sobre_mi" class="control-label">Sobre mí:</label>
                        			<textarea id="sobre_mi" name="sobre_mi" class="form-control" cols="10" rows="2"></textarea>
                    			</div>
	                            <div class="form-group text-right">
	                            	<button class="btn btn-primary" type="submit">
                    					<i class="fa fa-refresh"></i> Actualizar
                					</button>
	                            </div>
                        	</form>
		              	</div>
              			<div class="tab-pane" id="tab_2">
                			<form id="formularioClave">
	                            <div class="form-group">
	                                <label for="clave1" class="control-label">Clave nueva</label>
	                                <input id="clave1" name="clave1" class="form-control" type="password" required>
	                            </div>
	                            <div class="form-group">
	                                <label for="clave2" class="control-label">Confirmar nueva</label>
	                                <input id="clave2" name="clave2" class="form-control" type="password" required>
	                            </div>
	                            <div class="form-group text-right">
	                            	<button class="btn btn-primary" type="submit">
                    					<i class="fa fa-refresh"></i> Cambiar
                					</button>
	                            </div>
                        	</form>
		              </div>              
            		</div>
          		</div>
    		</div>
    	</div>
    </section>
</div>

<?php require 'footer.php'; ?>
7
<script type="text/javascript">
	var idUsuario

    function initLogin(user){
    	idUsuario = user.usuario.id

    	$('.calendario').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        })

    	llenarFormulario('formularioPerfil','usuarios', 'select', {'id':user.usuario.id}, function(r){
            validarExistenciaImagen('#imagen', 'assets/img/usuarios/'+user.usuario.id+'.jpg', 'assets/img/usuarios/default.png')
    		$('#perfil_nombre').text(r.data[0].nombre)
    		$('#perfil_profesion').text(r.data[0].profesion)
    		$('#perfil_login').text(r.data[0].login)
    		$('#perfil_perfil').text(r.data[0].perfil)
    		$('#perfil_telefono').text(r.data[0].telefono)
    		$('#perfil_sobre_mi').text(r.data[0].sobre_mi)
    	})

    	$('#formularioPerfil').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.id = user.usuario.id
            procesarRegistro('usuarios', 'update', data, function(r){                    
                swal('Perfecto!', 'El usuario se actualizo correctamente', 'success')
                $('#perfil_nombre').text(data.nombre)
	    		$('#perfil_profesion').text(data.profesion)
	    		$('#perfil_telefono').text(data.telefono)
	    		$('#perfil_sobre_mi').text(data.sobre_mi)
            })            
        })

        $('#formularioClave').on('submit', function(e){
            e.preventDefault()
            if($('#clave1').val() != $('#clave2').val()){
                swal('Ups!', 'Las calves deben ser iguales', 'error')                
            }else{
                let data = {
                    id: user.usuario.id,
                    password: $('#clave1').val()
                }
                procesarRegistro('usuarios', 'setPassword', data, function(r){
                    swal('Perfecto!', 'La clave se cambio correctamente', 'success')
                })
            }            
        })
    }

    function cargarImagen(){
        var archivo = $('#foto')[0].files[0]            
        let ext = archivo.name.split('.')           
        if(ext[1] == 'jpg' || ext[1] == 'png' || ext[1] == 'jpeg'){
            //se carga la imagen
            var fd = new FormData()        
            fd.append('objeto','usuarios')
            fd.append('metodo','subirImagen')
            fd.append('datos',idUsuario)
            fd.append('file',archivo)
            $.ajax({
                url: 'class/frontController.php',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(r){
                	if(r.ejecuto){
                		window.location.href = 'views/perfil.php'
                	}else{
            			console.log("Error")
                	}
                },
                error: function(xhr,status){
                    console.log('Disculpe, existio un problema procesando')
                }
            })
        }else{
            swal('Ups!', 'Tipo de archivo no permitido', 'error')
        }
    }
</script>