<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span><?=$_GET['n']?></span>            
            <small>Gestión de servicios</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li><a href="views/proveedores.php"> Proveedores</a></li>
            <li class="active">Servicios</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalServicios" class="btn color-magical btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Web</th>
                                            <th class="text-center">Tipo</th>
                                            <th class="text-center">Destino</th>
                                            <th class="text-center">Dian en itinerario</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Web</th>
                                            <th class="text-center">Tipo</th>
                                            <th class="text-center">Destino</th>                                            
                                            <th class="text-center">Dian en itinerario</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>    

<div class="modal fade" id="modalServicios">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalServiciosTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioServicios">
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required="required">
                    </div>
                    <div class="form-group">
                        <label for="web" class="control-label">Sitio web</label>
                        <input id="web" name="web" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="fk_destinos" class="control-label">Destino</label>
                        <select id="fk_destinos" name="fk_destinos" class="form-control"></select>
                    </div>
                    <div class="form-group">
                        <label for="fk_tipos_servicio" class="control-label">Tipo servicio</label>
                        <select id="fk_tipos_servicio" name="fk_tipos_servicio" class="form-control"></select>
                    </div>
                    <div class="form-group">
                        <label for="dias_en_itinerario">Días en itinerario</label>
                        <input id="dias_en_itinerario" name="dias_en_itinerario" class="form-control" type="number" required="required">
                    </div>                    
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarServicios" class="btn color-magical btn-submit" type="submit" form="formularioServicios">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarServicios" class="btn btn-default btn-submit" type="submit" form="formularioServicios">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalOpciones">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalOpcionesTitulo"></span>
                    <small> Opciones</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioOpciones">                    
                    <div class="form-group">
                        <label for="opcion">Opción:</label>
                        <input id="opcion" name="opcion" class="form-control" type="text" required>
                    </div>
                    <div class="row">                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tipo_costo" class="control-label">Tipo costo</label>
                                <select id="tipo_costo" name="tipo_costo" class="form-control" required>
                                    <option value="Unitario">Unitario</option>
                                    <option value="Grupal">Grupal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="costo">Costo:</label>
                                <input id="costo" name="costo" class="form-control numero" type="text" required>
                            </div>
                        </div>                        
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button id="botonGuardarOpciones" class="btn color-magical btn-submit" type="submit" form="formularioOpciones">
                    <i class="fa fa-save"></i> Crear
                </button>
                <button id="botonEditarOpciones" class="btn btn-default btn-submit" type="submit" form="formularioOpciones">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th>Opción</th>                                
                                <th>Tipo de costo</th>
                                <th>Costo</th>                                
                                <th>Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoOpciones"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>

<div class="modal fade" id="modalDescripciones">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalDescripcionesTitulo"></span>
                    <small> Descripciones</small>
                </h3>
            </div> 
            <div class="modal-body">
                <form id="formularioDescripciones">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_es" data-toggle="tab">Español</a>
                            </li>
                            <li>
                                <a href="#tab_en" data-toggle="tab">Ingles</a>
                            </li>
                            <li>
                                <a href="#tab_it" data-toggle="tab">Italiano</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active tab-pane-obs" id="tab_es"></div>
                            <div class="tab-pane tab-pane-obs" id="tab_en"></div>
                            <div class="tab-pane tab-pane-obs" id="tab_it"></div>
                        </div>
                    </div>                
                </form>                     
            </div> 
            <div class="modal-footer">
                <button class="btn color-magical" type="submit" form="formularioDescripciones">
                    <i class="fa fa-save"></i> Guardar
                </button>
            </div>            
        </div>             
    </div>         
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var idProveedor = <?=$_REQUEST['p']?>;
    var id 
    var boton    
    var idO

    function initLogin(user){        
        $('.numero').number( true, 0,',','.')
        $('.ocultar').hide()

        llenarSelect('proveedoresDestinos', 'getDestinos2', {p:idProveedor}, 'fk_destinos', 'nombre', 0)
        llenarSelect('proveedoresOferta', 'getOferta2', {p:idProveedor}, 'fk_tipos_servicio', 'nombre', 0)

        cargarRegistros({fk_proveedores:idProveedor, estado:'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalServicios').on('click', function(){
            $('#formularioServicios')[0].reset()
            $('#modalServiciosTitulo').text('Crear servicio')            
            $('#botonEditarServicios').hide()
            $('#botonGuardarServicios').show()
            $('#modalServicios').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioServicios').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_proveedores = idProveedor            
            if(boton == "botonGuardarServicios"){
                data.descripciones = '{}'
                procesarRegistro('servicios', 'insert', data, function(r){
                    swal('Perfecto!', 'El servicio se creo correctamente', 'success')
                    cargarRegistros({fk_proveedores:idProveedor, id:r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalServicios').modal('hide')    
                    })                    
                })    
            }else{                
                data.id = id                
                procesarRegistro('servicios', 'update', data, function(r){                    
                    swal('Perfecto!', 'El servicio se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({fk_proveedores:idProveedor, id:id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalServicios').modal('hide')
                })
            }
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalServicios').show()
            cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalServicios').hide()
            cargarRegistros({'estado':'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })

        //Esta es la logica de opciones
        $('#formularioOpciones').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarOpciones"){
                data.fk_servicios = id                
                procesarRegistro('opciones', 'insert', data, function(r){
                    $('#formularioOpciones')[0].reset()
                    cargarRegistrosOpciones({'id':r.insertId}, 'crear')
                    $('#botonEditarOpciones').hide()
                })
            }else{                
                data.id = idO
                procesarRegistro('opciones', 'update', data, function(r){
                    cargarRegistrosOpciones({'id':idO}, 'actualizar')
                })
            }            
        })

        //Logica de itinerario
        $('#formularioDescripciones').on('submit', function(e){
            e.preventDefault()
            let descripciones = {
                es:[],
                en:[],
                it:[]
            }            
            let data =  $(this).serializeArray()
            let cantidad = data.length / 6            
            for(let i = 0; i < cantidad; i++){
                descripciones.es.push({
                    titulo: '',
                    descripcion: ''
                })
                descripciones.en.push({
                    titulo: '',
                    descripcion: ''
                })
                descripciones.it.push({
                    titulo: '',
                    descripcion: ''
                })
            }            
            let temp
            for(let i = 0; i < data.length; i++){
                temp = data[i].name.split('-')                
                descripciones[temp[0]][temp[1]][temp[2]] = data[i].value
            }            
            procesarRegistro('servicios', 'guardarDescripciones', {id:id, descripciones:descripciones}, function(r){
                swal('Perfecto!', 'Se guardo correctamente!!!', 'success')
                $('#modalDescripciones').modal('hide')
            })
        })
    }    

    function cargarRegistros(data, accion, elemento, callback){        
        procesarRegistro('servicios', 'getServicios', data, function(r){
            let fila = ''
            let opciones
            for(let i = 0; i < r.data.length; i++){
                opciones = ''
                if(r.data[i].estado == 'Activo'){
                    opciones += '<button class="btn btn-default btn-xs" title="Editar servicio" onClick="mostrarModalEditarServicios('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button> '
                    opciones += '<button class="btn btn-default btn-xs" title="Editar opciones" onClick="mostrarModalOpciones('+r.data[i].id+',\''+r.data[i].servicio+'\','+r.data[i].idts+')"><i class="fa fa-sliders"></i></button> '
                    if(r.data[i].idts == 2 || r.data[i].idts == 4){
                        opciones += '<button class="btn btn-default btn-xs" title="Observaciones predefinidas" onClick="mostrarModalDescripciones('+r.data[i].id+',\''+r.data[i].servicio+'\','+r.data[i].dias+')"><i class="fa fa-calendar"></i></button>'
                    }
                    
                }else{
                    opciones += '<button class="btn btn-default btn-xs" onClick="funcionReactivarServicios('+r.data[i].id+')" title="Reactivar servicio"><i class="fa fa-reply"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td>'+r.data[i].servicio+'</td>'+
                            '<td>'+r.data[i].web+'</td>'+
                            '<td>'+r.data[i].ts+'</td>'+
                            '<td>'+r.data[i].destino+'</td>'+                            
                            '<td class="text-center">'+r.data[i].dias+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>' 
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)       
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarServicios(idServicio){
        id = idServicio
        llenarFormulario('formularioServicios','servicios', 'select', {'id':idServicio}, function(r){
            $('#modalServiciosTitulo').text('Modificar servicio')
            $('#botonGuardarServicios').hide()
            $('#botonEditarServicios').show()            
            $('#modalServicios').modal('show')            
        })
    }

    function funcionReactivarServicios(idServicio){
        swal('¿Esta seguro de reactivar el servicio?')
        .then((value) => {
            if (value) {
                procesarRegistro('servicios', 'update', {'id':idServicio, 'estado':'Activo'}, function(r){                    
                    $('#'+idServicio).remove()
                })
            }
        })
    }

    /*Este bloque es el de opciones*/
    function mostrarModalOpciones(idServicio, nombre, idts){
        id = idServicio
        $('#modalOpcionesTitulo').text(nombre)
        $('#formularioOpciones')[0].reset()
        $('#botonGuardarOpciones').show()
        $('#botonEditarOpciones').hide()
        $('#contenidoOpciones').html('')        
        cargarRegistrosOpciones({'fk_servicios':idServicio}, 'crear')
        $('#modalOpciones').modal('show')
    }

    function cargarRegistrosOpciones(data, accion){
        procesarRegistro('opciones', 'select', data, function(r){            
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="o_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].opcion+'</td>'+                    
                    '<td class="text-center">'+r.data[i].tipo_costo+'</td>'+
                    '<td class="text-right">$'+currency(r.data[i].costo,0)+'</td>'+                    
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="funcionMostrarEditarOpciones('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button>'+
                    '</td>'+
                '</tr>'                    
            }
            if(accion == 'crear'){ 
                $('#contenidoOpciones').append(fila)
            }else{ 
                $('#o_'+r.data[0].id).replaceWith(fila) 
            }            
        })
    }

    function funcionMostrarEditarOpciones(idOpcion){ 
        idO = idOpcion
        llenarFormulario('formularioOpciones', 'opciones', 'select', {'id':idOpcion}, function(r){          
            $('#botonEditarOpciones').show('slow')
            $('#botonGuardarOpciones').hide('slow')
        }) 
    }

    //Esta es la parte de itinerario
    function mostrarModalDescripciones(idServicio, nombre, dias){
        id = idServicio
        let fila_es = ''
        let fila_en = ''
        let fila_it = ''
        $('.tab-pane-obs').empty()
        for (let i = 0; i < dias; i++) {
            fila_es +=  '<div class="box box-default box-solid">'+
                            '<div class="box-header with-border">'+
                                '<h3 class="box-title">Día '+(i+1)+'</h3>'+
                            '</div>'+
                            '<div class="box-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-4">'+                            
                                        '<div class="form-group">'+                                
                                            '<label class="control-label">Título</label>'+
                                            '<input id="es-'+i+'-titulo" name="es-'+i+'-titulo" class="form-control" type="text">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-8">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Descripción</label>'+
                                            '<textarea id="es-'+i+'-descripcion" name="es-'+i+'-descripcion" class="form-control" cols="10" rows="3"></textarea>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+                                
                        '</div>'
            fila_en +=  '<div class="box box-default box-solid">'+
                            '<div class="box-header with-border">'+
                                '<h3 class="box-title">Día '+(i+1)+'</h3>'+
                            '</div>'+
                            '<div class="box-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-4">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Título</label>'+
                                            '<input id="en-'+i+'-titulo" name="en-'+i+'-titulo" class="form-control" type="text">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-8">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Descripción</label>'+
                                            '<textarea id="en-'+i+'-descripcion" name="en-'+i+'-descripcion" class="form-control" cols="10" rows="3"></textarea>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'
            fila_it +=  '<div class="box box-default box-solid">'+
                            '<div class="box-header with-border">'+
                                '<h3 class="box-title">Día '+(i+1)+'</h3>'+
                            '</div>'+
                            '<div class="box-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-4">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Título</label>'+
                                            '<input id="it-'+i+'-titulo" name="it-'+i+'-titulo" class="form-control" type="text">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-8">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Descripción</label>'+
                                            '<textarea id="it-'+i+'-descripcion" name="it-'+i+'-descripcion" class="form-control" cols="10" rows="3"></textarea>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'            
        }
        $('#tab_es').append(fila_es)
        $('#tab_en').append(fila_en)
        $('#tab_it').append(fila_it)
        procesarRegistro('servicios', 'getDescripciones', {id:idServicio}, function(r){            
            let descripciones = JSON.parse(r.data[0].descripciones)
            $.each(descripciones, function(key, val){
                for (let i = 0; i < val.length; i++) {
                    $('#'+key+'-'+i+'-titulo').val(val[i].titulo)
                    $('#'+key+'-'+i+'-descripcion').val(val[i].descripcion)
                }
            })
        })        
        $('#modalDescripcionesTitulo').text(nombre)        
        $('#modalDescripciones').modal('show')
    }
</script>
</body>
</html>