<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Magical</title>
    <base href="../">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Datetimepicker -->
    <link rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <!-- DataTables -->
    <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- App style -->
    <link rel="stylesheet" href="assets/css/app.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
    page. However, you can choose any other skin. Make sure you
    apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="dist/css/skins/skin-yellow.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">    
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-yellow layout-top-nav">
    <div class="wrapper">
        <header class="main-header">
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="views/index.php" class="navbar-brand"><b>M</b>agical</a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav" id="menu">
                            <li class="dropdown" id="menuConfiguracion">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuración <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="views/usuarios.php">Usuarios</a></li>
                                    <li class="divider"></li>
                                    <li><a href="views/paises.php">Paises</a></li>
                                    <li><a href="views/destinos.php">Destinos</a></li>
                                    <li class="divider"></li>
                                    <li><a href="views/canales.php">Canales de venta</a></li>
                                    <li><a href="views/causales.php">Causales de anulación</a></li>
                                    <li><a href="views/proveedores.php">Proveedores</a></li>
                                </ul>
                            </li>
                            <li id="menuClientes"><a href="views/clientes.php">Clientes</a></li>
                            <li class="dropdown" id="menuComercial">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Comercial <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="views/comercialCotizaciones.php">Cotizaciones</a></li>
                                    <li><a href="views/comercialReservas.php">Reservas</a></li>
                                </ul>
                            </li>
                            <li class="dropdown" id="menuOperaciones">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Operaciones <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="views/operacionesCotizaciones.php">Cotizaciones</a></li>
                                    <li><a href="views/operacionesReservas.php">Reservas</a></li>
                                </ul>
                            </li>
                            <li class="dropdown" id="menuCxC">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">CxC <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="views/cxcPendientes.php">Pendientes</a></li>
                                    <li class="divider"></li>
                                    <li><a href="views/cxcHistorico.php">Histórico</a></li>
                                </ul>
                            </li>
                            <li class="dropdown" id="menuCxP">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">CxP <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="views/cxpConciliar.php">Por conciliar</a></li>
                                    <li><a href="views/cxpPendientes.php">Pendientes</a></li>
                                    <li class="divider"></li>
                                    <li><a href="views/cxpHistorico.php">Histórico</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img class="user-image menu_foto" alt="User Image">
                                    <span class="hidden-xs nombreUsuario"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img class="img-circle menu_foto" alt="User Image">
                                        <p>
                                            <span class="nombreUsuario"></span>
                                            <small id="perfilUsuario"></small>
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a id="perfil" href="views/perfil.php" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            <a id="salir" href="views/logout.php" class="btn btn-default btn-flat">Salir</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>                    
                        </ul>
                    </div>
                </div>
            </nav>
        </header>