<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Proveedores
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Proveedores</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalProveedores" class="btn color-magical btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Tipo ID</th>
                                            <th class="text-center">Número</th>
                                            <th class="text-center">RNT</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>                                            
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Tipo ID</th>
                                            <th class="text-center">Número</th>
                                            <th class="text-center">RNT</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalProveedores">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalProveedoresTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioProveedores">                    
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo_doc" class="control-label">Tipo identificación</label>
                                <select id="tipo_doc" name="tipo_doc" class="form-control" required>
                                    <option value="">Seleccione...</option>
                                    <option value="CC">CC</option>
                                    <option value="NIT">NIT</option>                                    
                                    <option value="CE">CE</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="numero" class="control-label">Numero de identificación</label>
                                <input id="numero" name="numero" class="form-control" type="text" required>
                            </div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label for="rnt" class="control-label">RNT</label>
                        <input id="rnt" name="rnt" class="form-control" type="number">
                    </div>
                    <div class="form-group">
                        <label for="direccion" class="control-label">Dirección</label>
                        <input id="direccion" name="direccion" class="form-control" type="text">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono" class="control-label">Teléfono</label>
                                <input id="telefono" name="telefono" class="form-control" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="correo" class="control-label">Correo</label>
                                <input id="correo" name="correo" class="form-control" type="email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarProveedores" class="btn color-magical btn-submit" type="submit" form="formularioProveedores">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarProveedores" class="btn btn-default btn-submit" type="submit" form="formularioProveedores">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalContactos">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalContactosTitulo"></span>
                    <small> Contactos</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioContactos">                    
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="telefono" class="control-label">Telefono</label>
                                <input id="telefono" name="telefono" class="form-control" type="number" required>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="correo" class="control-label">Correo</label>
                                <input id="correo" name="correo" class="form-control" type="email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button id="botonGuardarContactos" class="btn color-magical btn-submit" type="submit" form="formularioContactos">
                    <i class="fa fa-save"></i> Crear
                </button>
                <button id="botonEditarContactos" class="btn btn-default btn-submit" type="submit" form="formularioContactos">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Telefono</th>
                                <th class="text-center">Correo</th>
                                <th class="text-center">Observación</th>
                                <th class="text-center">Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoContactos"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>
<div class="modal fade" id="modalDestinos">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalDestinosTitulo"></span>
                    <small> Destinos</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioDestinos">
                    <div class="form-group">
                        <label for="fk_destinos" class="control-label">Destino:</label>
                        <select id="fk_destinos" name="fk_destinos" class="form-control" required></select>
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button class="btn color-magical" type="submit" form="formularioDestinos">
                    <i class="fa fa-save"></i> Crear
                </button>                
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th class="text-center">Destino</th>                                
                                <th class="text-center">Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoDestinos"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>
<div class="modal fade" id="modalOferta">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalOfertaTitulo"></span>
                    <small> Oferta</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioOferta">
                    <div class="form-group">
                        <label for="fk_tipos_servicio" class="control-label">Tipo de servicio:</label>
                        <select id="fk_tipos_servicio" name="fk_tipos_servicio" class="form-control" required></select>
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button class="btn color-magical" type="submit" form="formularioOferta">
                    <i class="fa fa-save"></i> Crear
                </button>                
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th class="text-center">Destino</th>                                
                                <th class="text-center">Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoOferta"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id
    var idC
    var boton

    function initLogin(user){
        //Llenar select de departamentos y municipios y categorias        
        llenarSelect('destinos', 'getDestinos', {estado:'Activo'}, 'fk_destinos', 'nombre', 1)
        llenarSelect('tiposServicio', 'getTiposServicio', {estado:'Activo'}, 'fk_tipos_servicio', 'nombre', 1)

        cargarRegistros({estado:'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalProveedores').on('click', function(){
            $('#formularioProveedores')[0].reset()
            $('#modalProveedoresTitulo').text('Crear proveedor')            
            $('#botonEditarProveedores').hide()
            $('#botonGuardarProveedores').show()
            $('#modalProveedores').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioProveedores').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarProveedores"){
                procesarRegistro('proveedores', 'insert', data, function(r){
                    swal('Perfecto!', 'El proveedor se creo correctamente', 'success')
                    cargarRegistros({id:r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalProveedores').modal('hide')    
                    })                    
                })    
            }else{                
                data.id = id
                procesarRegistro('proveedores', 'update', data, function(r){                    
                    swal('Perfecto!', 'El proveedor se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({id:id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalProveedores').modal('hide')
                })
            }
        })

        //Esta es la parte de contactos
        $('#formularioContactos').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_proveedores = id
            if(boton == "botonGuardarContactos"){
                procesarRegistro('proveedoresContactos', 'insert', data, function(r){
                    $('#formularioContactos')[0].reset()
                    cargarRegistrosContactos({'id':r.insertId}, 'crear')                    
                })
            }else{
                data.id = idC
                procesarRegistro('proveedoresContactos', 'update', data, function(r){                    
                    cargarRegistrosContactos({'id':idC}, 'actualizar')                    
                })
            }            
        })

        //Esta es la parte de destinos
        $('#formularioDestinos').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.fk_proveedores = id                
            procesarRegistro('proveedoresDestinos', 'insert', data, function(r){
                $('#formularioDestinos')[0].reset()
                cargarRegistrosDestinos({campo:'id', valor:r.insertId}, 'crear')                    
            })            
        })

        //Esta es la parte de oferta
        $('#formularioOferta').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.fk_proveedores = id                
            procesarRegistro('proveedoresOferta', 'insert', data, function(r){
                $('#formularioOferta')[0].reset()
                cargarRegistrosOferta({campo:'id', valor:r.insertId}, 'crear')                    
            })            
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalProveedores').show()
            cargarRegistros({estado:'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalProveedores').hide()
            cargarRegistros({estado:'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })
    }

    function cargarRegistros(data, accion, elemento, callback){
        procesarRegistro('proveedores', 'select', data, function(r){            
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){
                opciones = '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarProveedores('+r.data[i].id+')" title="Editar proveedor"><i class="fa fa-pencil-square-o"></i></button> '
                if(r.data[i].estado == 'Cancelado'){
                    opciones = '<button class="btn btn-default btn-xs" onClick="funcionReactivarProveedor('+r.data[i].id+')" title="Reactivar proveedor"><i class="fa fa-reply"></i></button> '
                }
                fila += '<tr id="'+r.data[i].id+'">'+                            
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].tipo_doc+'</td>'+
                            '<td>'+r.data[i].numero+'</td>'+
                            '<td>'+r.data[i].rnt+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                                '<button class="btn btn-default btn-xs" title="Gestionar contactos" onClick="mostrarModalContactos('+r.data[i].id+',\''+r.data[i].nombre+'\')"><i class="fa fa-phone"></i></button> '+
                                '<button class="btn btn-default btn-xs" title="Zonificar proveedor" onClick="mostrarModalDestinos('+r.data[i].id+',\''+r.data[i].nombre+'\')"><i class="fa fa-map-marker"></i></button> '+
                                '<button class="btn btn-default btn-xs" title="Gestionar oferta" onClick="mostrarModalOferta('+r.data[i].id+',\''+r.data[i].nombre+'\')"><i class="fa fa-cubes"></i></button> '+
                                '<a class="btn btn-default btn-xs" title="Gestionar servicios" href="views/servicios.php?p='+r.data[i].id+'&n='+r.data[i].nombre+'"><i class="fa fa-navicon"></i></a>'+
                            '</td>'+
                        '</tr>' 
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)                        
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarProveedores(idProveedor){
        id = idProveedor
        llenarFormulario('formularioProveedores','proveedores', 'select', {'id':idProveedor}, function(r){
            $('#modalProveedoresTitulo').text('Modificar Proveedor')
            $('#botonGuardarProveedores').hide()
            $('#botonEditarProveedores').show()            
            $('#modalProveedores').modal('show')            
        })
    }    

    function funcionReactivarProveedor(idProveedor){
        swal('¿Esta seguro de reactivar el proveedor?')
        .then((value) => {
            if (value) {
                procesarRegistro('proveedores', 'update', {'id':idProveedor, 'estado':'Activo'}, function(r){
                    $('#'+idProveedor).remove()
                })
            }
        })
    }

    /*
    Este bloque es para el modulo de contactos
    */
    function mostrarModalContactos(idProveedor, nombre){
        id = idProveedor
        $('#modalContactosTitulo').text(nombre)
        $('#formularioContactos')[0].reset()
        $('#botonGuardarContactos').show()
        $('#botonEditarContactos').hide()
        $('#contenidoContactos').html('')        
        cargarRegistrosContactos({'fk_proveedores':idProveedor}, 'crear')        
        $('#modalContactos').modal('show')
    }

    function cargarRegistrosContactos(data, accion){
        procesarRegistro('proveedoresContactos', 'select', data, function(r){            
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="c_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].nombre+'</td>'+ 
                    '<td>'+r.data[i].telefono+'</td>'+ 
                    '<td>'+r.data[i].correo+'</td>'+
                    '<td>'+r.data[i].observaciones+'</td>'+
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="mostrarEditarContactos('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button> '+
                        '<button class="btn btn-default btn-xs" onClick="borrarContactos('+r.data[i].id+')"><i class="fa fa-times"></i></button>'+
                    '</td>'+
                '</tr>'                    
            }
            if(accion == 'crear'){ 
                $('#contenidoContactos').append(fila)
            }else{ 
                $('#c_'+r.data[0].id).replaceWith(fila)
            }            
        })
    }

    function mostrarEditarContactos(idContacto){
        idC = idContacto
        llenarFormulario('formularioContactos', 'proveedoresContactos', 'select', {'id':idContacto}, function(r){            
            $('#botonGuardarContactos').hide()
            $('#botonEditarContactos').show()            
        }) 
    }

    function borrarContactos(idContacto){
        swal('¿Esta seguro de borrar el contacto?')
        .then((value) => {
            if (value) {
                procesarRegistro('proveedoresContactos', 'delete', {'id':idContacto}, function(r){
                    $('#c_'+idContacto).hide('slow')
                })
            }
        })
    }

    /*
    Este es el bloque de zonificación del proveedor
    */
    function mostrarModalDestinos(idProveedor, nombre){
        id = idProveedor
        $('#modalDestinosTitulo').text(nombre)
        $('#formularioDestinos')[0].reset()        
        $('#contenidoDestinos').html('')        
        cargarRegistrosDestinos({campo: 'fk_proveedores', valor:idProveedor}, 'crear')        
        $('#modalDestinos').modal('show')
    }

    function cargarRegistrosDestinos(data, accion){
        procesarRegistro('proveedoresDestinos', 'getDestinos', data, function(r){            
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="d_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].nombre+'</td>'+
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="borrarDestinos('+r.data[i].id+')"><i class="fa fa-times"></i></button>'+
                    '</td>'+
                '</tr>'                    
            }            
            $('#contenidoDestinos').append(fila)            
        })
    }

    function borrarDestinos(idDestino){
        swal('¿Esta seguro de borrar el destino?')
        .then((value) => {
            if (value) {
                procesarRegistro('proveedoresDestinos', 'delete', {'id':idDestino}, function(r){
                    $('#d_'+idDestino).hide('slow')
                })
            }
        })
    }

    /*
    Este es el bloque de oferta
    */
    function mostrarModalOferta(idProveedor, nombre){
        id = idProveedor
        $('#modalOfertaTitulo').text(nombre)
        $('#formularioOferta')[0].reset()        
        $('#contenidoOferta').html('')        
        cargarRegistrosOferta({campo: 'fk_proveedores', valor:idProveedor}, 'crear')        
        $('#modalOferta').modal('show')
    }

    function cargarRegistrosOferta(data, accion){
        procesarRegistro('proveedoresOferta', 'getOferta', data, function(r){
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="d_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].nombre+'</td>'+
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="borrarOferta('+r.data[i].id+')"><i class="fa fa-times"></i></button>'+
                    '</td>'+
                '</tr>'                    
            }            
            $('#contenidoOferta').append(fila)            
        })
    }

    function borrarOferta(idOferta){
        swal('¿Esta seguro de borrar la oferta?')
        .then((value) => {
            if (value) {
                procesarRegistro('proveedoresOferta', 'delete', {'id':idOferta}, function(r){
                    $('#d_'+idOferta).hide('slow')
                })
            }
        })
    }
</script>
</body>
</html>