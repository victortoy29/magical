<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?=$_GET['nombre']?>
            <small>Galería de imagenes</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li><a href="views/destinos.php"> Destinos</a></li>
            <li class="active"> Galeria</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row" id="contenido"></div>
        <div class="row text-center">
            <label class="btn btn-default btn-xs" for="foto">
                <input id="foto" onChange="cargarImagen()" type="file"  style="display:none;"/>
                <i class="fa fa-camera fa-2x" aria-hidden="true"></i>
            </label>
        </div>
    </section>
</div>    

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id = <?=$_REQUEST['idD']?>;
    function initLogin(user){
        cargarRegistros()
    }

    function cargarRegistros(){
        procesarRegistro('destinos', 'getImages', {id:id}, function(r){
            let fila = ''
            for(let i = 0; i < r.data.length; i++){                
                fila += '<div class="col-sm-3" id="'+i+'">'+
                            '<div class="box box-solid">'+
                                '<div class="box-header with-border text-center">'+
                                    '<h3 class="box-title">'+r.data[i]+'</h3>'+
                                    '<div class="box-tools pull-right">'+                                        
                                        '<button class="btn btn-default btn-xs" title="Borrar imagen" onClick="borrarImagen('+i+',\''+r.data[i]+'\')"><i class="fa fa-close"></i></button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<p class="text-center">'+
                                        '<img class="img-responsive" src="assets/img/destinos/destino_'+id+'/'+r.data[i]+'" id="f_'+i+'" width="400" height="300">'+
                                    '</p>'+
                                '</div>'+                                
                            '</div>'+
                        '</div>'
            }            
            $('#contenido').append(fila)
        })
    }

    function cargarImagen(idD){
        let archivo = $('#foto')[0].files[0]
        let ext = archivo.name.split('.')
        if(ext[1] == 'jpg' || ext[1] == 'png' || ext[1] == 'jpeg' || ext[1] == 'JPG'){
            //se carga la imagen
            var fd = new FormData()        
            fd.append('objeto','destinos')
            fd.append('metodo','subirImagen')
            fd.append('datos',id)
            fd.append('file',archivo)
            $.ajax({
                url: 'class/frontController.php',
                type: 'post',
                data: fd,
                contentType: false,                
                processData: false,
                dataType: 'json',
                success: function(r){
                    $('#contenido').html('')
                    cargarRegistros()
                },
                error: function(xhr,status){
                    console.log('Disculpe, existio un problema procesando')
                }
            })
        }else{
            swal('Ups!', 'Tipo de archivo no permitido', 'error')
        }
    }

    function borrarImagen(codigo, nombre){
        procesarRegistro('destinos', 'borrarImagen', {id:id, imagen:nombre}, function(r){
            $('#'+codigo).remove()
        })
    }
</script>
</body>
</html>