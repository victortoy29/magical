<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Paises
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Paises</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalPaises" class="btn color-magical btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nombre</th>                                            
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nombre</th>                                            
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPaises">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalPaisesTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioPaises">                    
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarPaises" class="btn color-magical btn-submit" type="submit" form="formularioPaises">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarPaises" class="btn btn-default btn-submit" type="submit" form="formularioPaises">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton

    function initLogin(user){
        cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalPaises').on('click', function(){
            $('#formularioPaises')[0].reset()
            $('#modalPaisesTitulo').text('Crear pais')            
            $('#botonEditarPaises').hide()
            $('#botonGuardarPaises').show()
            $('#modalPaises').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioPaises').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarPaises"){
                procesarRegistro('paises', 'insert', data, function(r){
                    swal('Perfecto!', 'El pais se creo correctamente', 'success')
                    cargarRegistros({'id':r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalPaises').modal('hide')    
                    })                    
                })    
            }else{                
                data.id = id
                procesarRegistro('paises', 'update', data, function(r){                    
                    swal('Perfecto!', 'El pais se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({'id':id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalPaises').modal('hide')
                })
            }
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalPaises').show()
            cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalPaises').hide()
            cargarRegistros({'estado':'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })
    }

    function cargarRegistros(data, accion, elemento, callback){
        procesarRegistro('paises', 'select', data, function(r){            
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){                
                opciones = '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarPaises('+r.data[i].id+')" title="Editar pais"><i class="fa fa-pencil-square-o"></i></button>'                
                if(r.data[i].estado == 'Cancelado'){
                    opciones = '<button class="btn btn-default btn-xs" onClick="funcionReactivarPaises('+r.data[i].id+')" title="Reactivar pais"><i class="fa fa-reply"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'">'+                            
                            '<td>'+r.data[i].nombre+'</td>'+                            
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>' 
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)       
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarPaises(idPais){
        id = idPais
        llenarFormulario('formularioPaises','paises', 'select', {'id':idPais}, function(r){
            $('#modalPaisesTitulo').text('Modificar pais')
            $('#botonGuardarPaises').hide()
            $('#botonEditarPaises').show()            
            $('#modalPaises').modal('show')            
        })
    }    

    function funcionReactivarPaises(idPais){
        swal('¿Esta seguro de reactivar el pais?')
        .then((value) => {
            if (value) {
                procesarRegistro('Paises', 'update', {'id':idPais, 'estado':'Activo'}, function(r){                    
                    $('#'+idPais).remove()
                })
            }
        })
    }
</script>
</body>
</html>