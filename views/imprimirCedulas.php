<?php
require_once '../class/viajes.php';
require_once '../class/pasajeros.php';
require_once '../plugins/fpdf/fpdf.php';

$objViaje = new viajes();
$viaje = $objViaje->select(['id'=> $_GET['idV']]);

$objPasajeros = new pasajeros();
$pasajeros = $objPasajeros->select(['fk_viajes'=> $_GET['idV']]);

class PDF extends FPDF{
	// Cabecera de página
	function Header(){
		global $viaje;
	    // Logos
	    $this->SetFont('Arial','B',12);
	    $this->Cell(60,20,$this->Image('../assets/img/logo2.png',$this->GetX(),$this->GetY()+1,60),0,0,'C');
	    $this->Cell(70,20,$viaje['data'][0]['nombre'],0,0,'C');
	    $this->Cell(60,20,'R'.$viaje['data'][0]['codigo_reserva'],10,0,'C');
		$this->Ln(23);
	}

	// Pie de página
	function Footer(){
	    // Posición: a 1,5 cm del final
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Número de página
	    $this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'C');
	}
}

$pdf = new PDF();
$centinela = 0;
for ($i=0; $i < count($pasajeros['data']); $i++) {
	if($centinela % 2 == 0){
		$pdf->AddPage();
	}	
	$pdf->SetFont('Arial','B',10);	
	$pdf->Cell(190,5,utf8_decode($pasajeros['data'][$i]['nombre']),1,1,'C');	
	if(file_exists('../assets/img/documentos/'.$pasajeros['data'][$i]['id'].'.jpg')){
		$pdf->Cell(190,80,$pdf->Image('../assets/img/documentos/'.$pasajeros['data'][$i]['id'].'.jpg',$pdf->GetX()+30,$pdf->GetY(),140,80),1,1,'C');
	}else{
		$pdf->Cell(190,80,'No existe imagen',1,1,'C');
	}
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(90,5,'Tipo de documento',1,0);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(100,5,$pasajeros['data'][$i]['tipo_doc'],1,1);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(90,5,utf8_decode('Número'),1,0);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(100,5,$pasajeros['data'][$i]['numero'],1,1);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(90,5,'Fecha de vencimiento',1,0);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(100,5,$pasajeros['data'][$i]['fecha_vencimiento'],1,1);
	$pdf->Ln();
	$centinela++;
}

$pdf->Output();
?>