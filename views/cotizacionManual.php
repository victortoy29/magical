<?php require 'header.php'; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span id="nombreViaje"></span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Comercial</a></li>
            <li><a href="views/comercialCotizaciones.php"> Cotizaciones</a></li>
            <li><a href="views/comercialCotizacionesDetalle.php?idV=<?=$_REQUEST['idV']?>"> Detalle</a></li>
            <li class="active"> Manual</li>
        </ol>
    </section>
    <section class="content container-fluid">
    	<div class="row">
    		<div class="col-md-4">
    			<ul class="timeline" id="contenidoItinerario"></ul>
    		</div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formularioItinerario"></form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn color-magical btn-block" type="submit" form="formularioItinerario">
                            <i class="fa fa-save"></i> Guardar
                        </button>
                    </div>
                    <div class="col-md-6">
                        <!--a class="btn btn-info btn-block" href="views/imprimirCotizacionManual.php?idV=<?=$_GET['idV']?>" target=_blank-->
                        <a class="btn btn-info btn-block" href="class/imprimir.php?idV=<?=$_GET['idV']?>" target=_blank>
                            <i class="fa fa-file-pdf-o"></i> Imprimir
                        </a>
                    </div>
        		</div>
            </div>
    	</div>
    </section>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
	var idViaje = <?=$_REQUEST['idV']?>;
    var itinerario = {
        "foto_portada":"",
        "foto_contraportada":"",
        "condiciones_pago":"",
        "condiciones_cancelacion":"",
        "itinerario":[]
    }

	function initLogin(user){
		construirItinerario(idViaje, function(respuesta){            
    		//consultar el viaje para obtener nombre y fechas para armar los días            
        	$('#nombreViaje').text(respuesta[0].data[0].nombre)

        	//Calcular el numero de días
            let inicio = moment(respuesta[0].data[0].fecha_inicio, 'YYYY-MM-DD')
            let fecha_inicial_viaje = moment(respuesta[0].data[0].fecha_inicio, 'YYYY-MM-DD')            
            let fin = moment(respuesta[0].data[0].fecha_fin, 'YYYY-MM-DD')            
            let dias = fin.diff(inicio, 'days')            
            for (let i = 0; i <= dias; i++) {
                itinerario.itinerario.push({
                    "fecha": inicio.format('YYYY-MM-DD'),
                    "foto1": "",
                    "foto2": "",
                    "titulo": "",
                    "etapa": "",
                    "destino": "",
                    "comidas": [],
                    "alojamiento": "",
                    "descripcion": ""
                })
                //Crear formulario
                $('#formularioItinerario').append('<div class="box box-primary">'+
                			'<div class="box-header with-border">'+
              					'<h3 class="box-title">'+inicio.format('DD-MMM-YYYY')+'</h3>'+
            				'</div>'+
            				'<div class="box-body">'+
                                //Foto 1
            					'<div class="col-md-5">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label">Destino:</label>'+
                                                '<select id="destino-'+i+'" name="destino-'+i+'" class="form-control destinos" data-id='+i+'></select>'+
                                                '<input id="foto1-'+i+'" name="foto1-'+i+'" class="form-control" type="hidden">'+
                                                '<input id="foto2-'+i+'" name="foto2-'+i+'" class="form-control" type="hidden">'+
                                                '<input name="fecha-'+i+'" class="form-control" type="hidden" value="'+inicio.format('YYYY-MM-DD')+'">'+                                        
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+    
                                    '<div class="row">'+                                        
                                        '<div class="col-md-6">'+
                                            '<div id="carousel1-'+i+'" data-interval="false" class="carousel slide" data-ride="carousel">'+
                                                '<div id="carouselItems1-'+i+'" class="carousel-inner" role="listbox">'+
                                                '</div>'+
                                                '<a class="left carousel-control" href="#carousel1-'+i+'" role="button" data-slide="prev">'+
                                                    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'+
                                                    '<span class="sr-only">Previous</span>'+
                                                '</a>'+
                                                '<a class="right carousel-control" href="#carousel1-'+i+'" role="button" data-slide="next">'+
                                                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+
                                                    '<span class="sr-only">Next</span>'+
                                                '</a>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div id="carousel2-'+i+'" data-interval="false" class="carousel slide" data-ride="carousel">'+
                                                '<div id="carouselItems2-'+i+'" class="carousel-inner" role="listbox">'+
                                                '</div>'+
                                                '<a class="left carousel-control" href="#carousel2-'+i+'" role="button" data-slide="prev">'+
                                                    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'+
                                                    '<span class="sr-only">Previous</span>'+
                                                '</a>'+
                                                '<a class="right carousel-control" href="#carousel2-'+i+'" role="button" data-slide="next">'+
                                                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+
                                                    '<span class="sr-only">Next</span>'+
                                                '</a>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
            					'<div class="col-md-4">'+
	            					'<div class="form-group">'+
	                        			'<label class="control-label">Titulo</label>'+
	                        			'<input id="titulo-'+i+'" name="titulo-'+i+'" class="form-control" type="text">'+
	                    			'</div>'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Etapa</label>'+
                                        '<input id="etapa-'+i+'" name="etapa-'+i+'" class="form-control" type="text">'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Alojamiento</label>'+
                                        '<input id="alojamiento-'+i+'" name="alojamiento-'+i+'" class="form-control" type="text">'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="checkbox">'+
                                            '<label>'+
                                                '<input type="checkbox" id="comidas-'+i+'-Desayuno" name="comidas-'+i+'-Desayuno" value="Desayuno">Desayuno'+
                                            '</label>'+
                                        '</div>'+
                                        '<div class="checkbox">'+
                                            '<label>'+
                                                '<input type="checkbox" id="comidas-'+i+'-Almuerzo" name="comidas-'+i+'-Almuerzo" value="Almuerzo">Almuerzo'+
                                            '</label>'+
                                        '</div>'+
                                        '<div class="checkbox">'+
                                            '<label>'+
                                                '<input type="checkbox" id="comidas-'+i+'-Cena" name="comidas-'+i+'-Cena" value="Cena">Cena'+
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
	                            '</div>'+
                                '<div class="col-md-3">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Descripción</label>'+
                                        '<textarea id="descripcion-'+i+'" name="descripcion-'+i+'" class="form-control" cols="10" rows="8"></textarea>'+
                                    '</div>'+
                                '</div>'+
            				'</div>'+
            			'</div>')
                inicio.add(1, 'days')
            }
            $('#formularioItinerario').append('<div class="box box-primary">'+
                            '<div class="box-header with-border">'+
                                '<h3 class="box-title">Condiciones de pago</h3>'+
                            '</div>'+
                            '<div class="box-body">'+
                                '<input id="condiciones_pago" name="condiciones_pago" class="form-control" type="text">'+
                            '</div>'+
                        '</div>'+
                        '<div class="box box-primary">'+
                            '<div class="box-header with-border">'+
                                '<h3 class="box-title">Condiciones de cancelación</h3>'+
                            '</div>'+
                            '<div class="box-body">'+
                                '<textarea id="condiciones_cancelacion" name="condiciones_cancelacion" class="form-control" cols="10" rows="7"></textarea>'+
                            '</div>'+
                        '</div>')
            //LLenar destinos
            procesarRegistro('destinos', 'getDestinos', {estado:'Activo'}, function(r){
                $('.destinos').empty();
                $('.destinos').append('<option value="">Seleccione...</option>');
                for (i = 0; i < r.data.length; i++){
                    $('.destinos').append('<option value='+r.data[i].id+'>'+r.data[i].nombre+'</option>')
                }
                $('.destinos').on('change', function(){
                    let destino = $(this).val()
                    let id = $(this).data('id')
                    $('#carouselItems1-'+id).empty()
                    $('#carouselItems2-'+id).empty()
                    procesarRegistro('destinos', 'getImages', {id:destino}, function(r){
                        //Se asigna la primera imagen al hidden
                        $('#foto1-'+id).val('assets/img/destinos/destino_'+destino+'/'+r.data[0])
                        $('#foto2-'+id).val('assets/img/destinos/destino_'+destino+'/'+r.data[0])
                        for(i = 0; i < r.data.length; i++){
                            $('#carouselItems1-'+id).append('<div class="item items1-'+id+'" data-image="assets/img/destinos/destino_'+destino+'/'+r.data[i]+'">'+
                                '<img class="img-responsive" src="assets/img/destinos/destino_'+destino+'/'+r.data[i]+'">'+
                            '</div>')
                            $('#carouselItems2-'+id).append('<div class="item items2-'+id+'" data-image="assets/img/destinos/destino_'+destino+'/'+r.data[i]+'">'+
                                '<img class="img-responsive" src="assets/img/destinos/destino_'+destino+'/'+r.data[i]+'">'+
                            '</div>')
                        }
                        $('.items1-'+id).first().addClass('active')
                        $('.items2-'+id).first().addClass('active')
                        //Capturar el data de la imagen que esta activa
                        $('#carousel1-'+id).on('slid.bs.carousel', function(){          
                            $('#foto1-'+id).val($('.items1-'+id+'.active').data('image'))
                        })
                        $('#carousel2-'+id).on('slid.bs.carousel', function(){                            
                            $('#foto2-'+id).val($('.items2-'+id+'.active').data('image'))
                        })
                    })
                })
            })
            if(respuesta[0].data[0].itinerario == ''){//En este caso se trae las observaciones de referencia                
                let descripciones
                let dias_en_itinerario                
                let comienzo                
                for(let i = 0; i < respuesta[1].data.length; i++){                    
                    comienzo = moment(respuesta[1].data[i].fecha_inicio, 'YYYY-MM-DD')                    
                    posicion_dia_inicial = comienzo.diff(fecha_inicial_viaje, 'days')                    
                    if(respuesta[1].data[i].ts == 3){
                        dias_en_itinerario = respuesta[1].data[i].di * respuesta[1].data[i].cantidad                        
                        for(j = 0; j< dias_en_itinerario; j++){
                            $('#etapa-'+posicion_dia_inicial).val(respuesta[1].data[i].destino)                            
                            $('#alojamiento-'+posicion_dia_inicial).val(respuesta[1].data[i].servicio)
                            posicion_dia_inicial++
                        }
                    }else if(respuesta[1].data[i].ts == 2 || respuesta[1].data[i].ts == 4){
                        descripciones = JSON.parse(respuesta[1].data[i].descripciones)
                        if(!jQuery.isEmptyObject(descripciones)){
                            idioma = descripciones[respuesta[0].data[0].idioma]
                            for(j = 0; j < idioma.length; j++){
                                $('#etapa-'+posicion_dia_inicial).val(respuesta[1].data[i].destino)                            
                                $('#titulo-'+posicion_dia_inicial).val(idioma[j].titulo)
                                $('#descripcion-'+posicion_dia_inicial).val(idioma[j].descripcion)
                                posicion_dia_inicial++
                            }
                        }                    
                    }
                }
                $('#condiciones_pago').val('Para confirmar la reserva es necesario un abono del 30% y el saldo total 45 días antes del viaje')
                $('#condiciones_cancelacion').val('Todas las cancelaciones tienen que ser comunicadas por escrito al email info@magicalcolombia.com. Los tiquetes aéreos nacionales tienen una penalidad del 100%. Para los servicios terrestres aplican las siguientes condiciones:\n'+
                    'A más de 45 días de la fecha de inicio del viaje: 25%\n'+
                    'Entre 44 y 30 días de la fecha de inicio del viaje: 50 %\n'+
                    'Entre 30 y 22 días de la fecha de inicio del viaje: 75 %\n'+
                    'A menos de 21 días de la fecha de inicio del viaje: 100 %')
            }else{//En este caso se traen las observaciones del json de viajes
                let itinerarioDB = JSON.parse(respuesta[0].data[0].itinerario)
                $('#condiciones_pago').val(itinerarioDB.condiciones_pago)
                $('#condiciones_cancelacion').val(itinerarioDB.condiciones_cancelacion)
                for(let i = 0; i < itinerarioDB.itinerario.length; i++){
                    $.each(itinerarioDB.itinerario[i], function(campo, valor){
                        switch(campo){
                            case 'foto1':
                                $('#foto1-'+i).val(valor)
                                $('#carouselItems1-'+i).append('<div class="item items1-'+i+'" data-image="'+valor+'">'+
                                        '<img class="img-responsive" src="'+valor+'">'+
                                    '</div>')
                                $('.items1-'+i).first().addClass('active')
                                break
                            case 'foto2':
                                $('#foto2-'+i).val(valor)
                                $('#carouselItems2-'+i).append('<div class="item items2-'+i+'" data-image="'+valor+'">'+
                                        '<img class="img-responsive" src="'+valor+'">'+
                                    '</div>')
                                $('.items2-'+i).first().addClass('active')
                                break
                            case 'comidas':
                                for(j = 0; j < valor.length; j++){
                                    $('#comidas-'+i+'-'+valor[j]).prop("checked","true")
                                }
                            default:
                                $('#'+campo+'-'+i).val(valor)
                                break
                        }
                    })
                }
            }
        })

        $('#formularioItinerario').on('submit', function(e){
            e.preventDefault()
            let data = $(this).serializeArray();
            let temp
            //Limpiar campo comidas
            for(let i = 0; i < data.length; i++){
                temp = data[i].name.split('-')
                if(temp.length == 2){                    
                    itinerario.itinerario[temp[1]].comidas = []
                }                
            }            

            for(let i = 0; i < data.length; i++){
                temp = data[i].name.split('-')
                switch(temp.length){
                    case 1://Nivel 1 el json
                        itinerario[temp[0]] = data[i].value 
                        break
                    case 2://Nivel 2 del json
                        itinerario.itinerario[temp[1]][temp[0]] = data[i].value
                        break
                    case 3://comidas
                        itinerario.itinerario[temp[1]].comidas.push(data[i].value)
                        break
                }
            }
            procesarRegistro('viajes', 'guardarItinerario', {id:idViaje, itinerario:itinerario}, function(r){
                swal('Perfecto!', 'Se guardo correctamente', 'success')
            })
        })	
	}
</script>
</body>
</html>