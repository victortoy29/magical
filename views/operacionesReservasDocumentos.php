<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span id="nombreViaje"></span>
            <small>
                <span class="label label-success color-magical" id="reserva"></span>
                <span class="label label-success color-magical"><i class="fa fa-users"></i> X <span class="tituloPasajeros"></span></span>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Operaciones</a></li>
            <li><a href="views/operacionesReservas.php"> Reservas</a></li>
            <li class="active"> Documentos</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de documentos</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>                                        
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Observaciones</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalProcesar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalProcesarTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioProcesar">
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" form="formularioProcesar" id="boton">
                    <i class="fa fa-save"></i> Guardar
                </button>                
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var idViaje = <?=$_REQUEST['idV']?>;
    var id
    var estado

    function initLogin(user){
        procesarRegistro('viajes', 'select', {id: idViaje}, function(r){
            $('#nombreViaje').text(r.data[0].nombre)
            $('#reserva').text('R-'+r.data[0].codigo_reserva)
            pasajeros = r.data[0].pasajeros
            $('.tituloPasajeros').text(r.data[0].pasajeros)
        })

        cargarRegistros({fk_viajes:idViaje}, 'crear')

        $('#formularioProcesar').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.id = id
            data.estado = estado
            procesarRegistro('documentos', 'update', data, function(r){
                cargarRegistros({id: id}, 'actualizar')
                $('#modalProcesar').modal('hide')                
            })
        })
    }

    function cargarRegistros(data, accion){
        let colores = {
            Pendiente: '',
            Eventualidad: 'bg-yellow',
            Enviado: 'bg-green'
        }
        procesarRegistro('documentos', 'select', data, function(r){
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){
                opciones = ''
                if(r.data[i].estado == 'Pendiente'){
                    opciones = '<button class="btn btn-success btn-xs" onClick="procesar('+r.data[i].id+',\'Enviado\',\''+r.data[i].nombre+'\')"><i class="fa fa-check"></i></button> '
                    opciones += '<button class="btn btn-warning btn-xs" onClick="procesar('+r.data[i].id+',\'Eventualidad\',\''+r.data[i].nombre+'\')"><i class="fa fa-warning"></i></button>'
                }else if(r.data[i].estado == 'Eventualidad'){
                    opciones = '<button class="btn btn-success btn-xs" onClick="procesar('+r.data[i].id+',\'Enviado\',\''+r.data[i].nombre+'\')"><i class="fa fa-check"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'" class="'+colores[r.data[i].estado]+'">'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+                            
                            '<td class="text-center">'+                                
                                opciones+
                            '</td>'+
                        '</tr>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
        })
    }

    function procesar(idDoc, est, documento){
        id = idDoc
        estado = est        
        if(est == 'Enviado'){
            $('#modalProcesarTitulo').html('<p class="text-green">Reporte de envio de '+documento+'</p>')
            $('#boton').removeClass('btn-warning').addClass('btn-success')
        }else{
            $('#modalProcesarTitulo').html('<p class="text-yellow">Reporte de novedad de '+documento+'</p>')
            $('#boton').removeClass('btn-success').addClass('btn-warning')
        }
        $('#formularioProcesar')[0].reset()        
        $('#modalProcesar').modal('show')
    }
</script>
</body>
</html>