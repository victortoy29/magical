<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            CxP
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> CxP</a></li>
            <li> Pendientes</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Reserva</th>
                                        <th class="text-center">Nombre reserva</th>
                                        <th class="text-center">Proveedor</th>
                                        <th class="text-center">Valor calculado</th>
                                        <th class="text-center">Valor conciliado</th>
                                        <th class="text-center">Saldo</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPagos">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalPagosTitulo"></span>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioPagos" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="valor" class="control-label">Valor</label>
                        <input id="valor" name="valor" class="form-control numero" type="text" required="required">
                    </div>
                    <div class="form-group">
                        <label for="archivo" class="control-label">Comprobante de ingreso</label>
                        <input  type="file" id="archivo" name="archivo" required="required">
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" form="formularioPagos">
                    <i class="fa fa-save"></i> Crear
                </button>
            </div>            
        </div>             
    </div>         
</div>

<div class="modal fade" id="modalHistoricoPagos">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span id="modalHistoricoPagosTitulo"></span>
                    <small> Historico de pagos</small>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Valor</th>                                
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Comprobante</th>
                        </tr>
                    </thead>
                    <tbody id="contenidoHistoricoPagos"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalProveedores">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span>Información proveedor</span>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody id="contenidoProveedores"></tbody>
                </table>
                <div class="text-center">
                    <button id="mostrarContactos" class="btn btn-primary" type="button">
                        <i class="fa fa-phone"></i>
                    </button>
                </div>
                <br>
                <div id="contenidoContactos"></div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id
    var maximo

    function initLogin(user){
        $('.numero').number( true, 0,',','.')

        cargarRegistros({estado:'Pendiente'}, 'crear')

        $('#formularioPagos').on('submit',function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(data.valor <= maximo){
                let archivo = $('#archivo')[0].files[0]
                let fd = new FormData()
                fd.append('objeto', 'cxpPagos')
                fd.append('metodo','acentarPago')
                fd.append('file',archivo)
                fd.append('datos[cxp]',id)
                fd.append('datos[valor]',data.valor)                
                $.ajax({
                    url: 'class/frontController.php',
                    type: 'post',
                    dataType: 'json',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(r){
                        if(r.ejecuto == true){                        
                            $('#formularioPagos')[0].reset()
                            cargarRegistros({id:id}, 'actualizar')
                            $('#modalPagos').modal('hide')
                        }    
                    }
                })
            }else{
                swal('Ups!', 'No se puede crear el pago, supero el saldo', 'error')
            }
        })

        //Mostrar contactos de proveedor
        $('#mostrarContactos').on('click', function(){
            let fila = ''
            procesarRegistro('proveedoresContactos', 'select', {fk_proveedores: idPro}, function(r){
                fila = '<table class="table table-bordered"><tr><th>Nombre</th><th>Telefono</th><th>Correo</th><th>Observación</th></tr>'
                for(let i = 0; i < r.data.length; i++){
                    fila += '<tr>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].telefono+'</td>'+
                            '<td>'+r.data[i].correo+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+
                            '</tr>'
                }
                fila += '</table>'
                $('#contenidoContactos').append(fila)
            })
        })
    }

    function cargarRegistros(data, accion){
        procesarRegistro('cxp', 'getCuentas', data, function(r){
            let fila
            let opciones
            let color = {
                Pendiente: 'text-red',
                Finalizado: 'text-green'
            }
            for(let i = 0; i < r.data.length; i++){
                opciones = ''
                if(r.data[i].estado == 'Pendiente'){
                    opciones = '<button class="btn btn-default btn-xs" title="Cargar pago" onClick="cargarPago('+r.data[i].id+','+r.data[i].codigo_reserva+','+r.data[i].saldo+')"><i class="fa fa-plus"></i></button> '
                }
                fila += '<tr id="'+r.data[i].id+'">'+                            
                            '<td class="text-center">R-'+r.data[i].codigo_reserva+'</td>'+
                            '<td class="text-center">'+r.data[i].reserva+'</td>'+
                            '<td><button type="button" class="btn btn-link" onClick="infoProveedores('+r.data[i].idp+')">'+r.data[i].nombre+'</button></td>'+
                            '<td class="text-right">$'+currency(r.data[i].valor_calculado,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].valor_conciliado,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].saldo,0)+'</td>'+
                            '<td class="text-center '+color[r.data[i].estado]+'">'+r.data[i].estado+'</td>'+
                            '<td class="text-center">'+
                                '<a class="btn btn-default btn-xs" href="assets/img/cxp/'+r.data[i].id+'.jpg" target="_blank"><i class="fa fa-image"></i></a> '+
                                opciones+
                                '<button class="btn btn-default btn-xs" title="Historico pagos" onClick="verHistorico('+r.data[i].id+','+r.data[i].codigo_reserva+')"><i class="fa fa-history"></i></button>'+
                            '</td>'+
                        '</tr>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
        })
    }

    function infoProveedores(idProveedor){
        idPro = idProveedor
        $('#contenidoProveedores').empty()
        $('#contenidoContactos').empty()
        procesarRegistro('proveedores', 'select', {id:idProveedor}, function(r){
            $('#contenidoProveedores').append(
                '<tr><td>Nombre:</td><td>'+r.data[0].nombre+'</td></tr>'+
                '<tr><td>Tipo documento:</td><td>'+r.data[0].tipo_doc+'</td></tr>'+
                '<tr><td>Número:</td><td>'+r.data[0].numero+'</td></tr>'+
                '<tr><td>Telefono:</td><td>'+r.data[0].telefono+'</td></tr>'+
                '<tr><td>Correo:</td><td>'+r.data[0].correo+'</td></tr>'
            )
        })
        $('#modalProveedores').modal('show')
    }

    /*Logica de creación de pagos*/
    function cargarPago(idcxp, cr, max){
        id = idcxp
        maximo = max
        $('#modalPagosTitulo').text('Cargar pago para R-'+cr)
        $('#formularioPagos')[0].reset()
        $('#modalPagos').modal('show')
    }

    function verHistorico(idcxp, cr){
        $('#modalHistoricoPagosTitulo').text('R-'+cr)
        $('#contenidoHistoricoPagos').empty()
        cargarRegistrosPagos({'fk_cxp':idcxp})
        $('#modalHistoricoPagos').modal('show')
    }

    function cargarRegistrosPagos(data){
        procesarRegistro('cxpPagos', 'select', data, function(r){
            let fila = ''            
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr>'+                            
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+                            
                            '<td class="text-center">'+r.data[i].fecha_creacion+'</td>'+
                            '<td class="text-center">'+                                
                                '<a class="btn btn-default btn-xs" href="assets/img/comprobantes_cxp/'+r.data[i].id+'.jpg" target="_blank"><i class="fa fa-image"></i></a>'+
                            '</td>'+
                        '</tr>'
            }            
            $('#contenidoHistoricoPagos').append(fila)
        })
    }
</script>
</body>
</html>