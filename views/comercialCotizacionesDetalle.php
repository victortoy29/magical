<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span id="nombreViaje"></span>
            <small>
                <span class="label bg-black">C-<?=$_GET['idV']?></span>
                <span class="label bg-black"><i class="fa fa-users"></i> X <span class="tituloPasajeros"></span></span>
                <span class="label bg-black">Moneda: <span class="moneda"></span></span>
                <span class="label bg-black">Tasa: $<span id="tasa"></span></span>
                <span class="label bg-black">Margen: <span id="margen"></span>%</span>
                &nbsp;
                <a class="btn btn-info btn-xs" href="views/cotizacionManual.php?idV=<?=$_GET['idV']?>">
                    <i class="fa fa-file-pdf-o"></i> Generar
                </a>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Comercial</a></li>
            <li><a href="views/comercialCotizaciones.php"> Cotizaciones</a></li>
            <li class="active"> Detalle</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Servicios</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">                                    
                                <form id="formularioDetalle">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="fecha_inicio" class="control-label">Día inicio</label>
                                                <select id="fecha_inicio" name="fecha_inicio" class="form-control" required="required"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Hora:</label>
                                                <div class="input-group">
                                                    <div class='input-group date hora_inicio'>
                                                        <input id="hora_inicio" name="hora_inicio" class="form-control" type='text'>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="destinos" class="control-label">Destino</label>
                                                <select id="destinos" class="form-control filtroServicios filtroProveedor"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="tipos_servicio" class="control-label">Tipo servicio</label>
                                                <select id="tipos_servicio" class="form-control filtroServicios filtroProveedor"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="proveedor" class="control-label">Proveedor</label>
                                                <select id="proveedor" class="form-control filtroServicios"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="servicios" class="control-label">Servicio</label>
                                                <select id="servicios" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="fk_opciones" class="control-label">Opciones</label>
                                                <select id="fk_opciones" name="fk_opciones" class="form-control" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label id="indicativoTipo" class="control-label">.</label>
                                                <input id="indicativoCosto" class="form-control numero" type="text" readonly="readonly">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="costo" class="control-label">Costo por persona</label>
                                                <input id="costo" name="costo" class="form-control numero" type="text" required="required">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="pasajeros" class="control-label">Pasajeros</label>
                                                <input id="pasajeros" name="pasajeros" class="form-control" type="number" required>
                                            </div>
                                        </div>
                                         <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cantidad" id="unidad_medida" class="control-label">.</label>
                                                <input id="cantidad" name="cantidad" class="form-control numero" type="text" required="required" value=1>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="total" class="control-label">Total</label>
                                                <input id="total" class="form-control numero" type="text" readonly="readonly">
                                            </div>                        
                                        </div>                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="detalle" class="control-label">Detalle</label>
                                                <textarea id="detalle" name="detalle" class="form-control" rows="1"></textarea>
                                            </div>
                                        </div>
                                    </div>                                    
                                </form>                                                  
                            </div>
                            <div class="box-footer text-center">
                                <button id="botonGuardarDetalle" class="btn btn-primary btn-submit" type="submit" form="formularioDetalle">
                                    <i class="fa fa-arrow-down"></i>
                                </button>
                                &nbsp;
                                <button id="botonEditarDetalle" class="btn btn-default btn-submit" type="submit" form="formularioDetalle">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Servicios</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Fecha</th>
                                                <th class="text-center">Hora</th>
                                                <th class="text-center">Destino</th>
                                                <th class="text-center">Servicio</th>
                                                <th class="text-center">Opción</th>
                                                <th class="text-center">Detalle</th>
                                                <th class="text-center">Precio</th>
                                                <th class="text-center">Pasajeros</th>
                                                <th class="text-center">Cantidad</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Observación</th>
                                                <th class="text-center">Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="contenido"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-5 col-md-7">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Totales</h3>
                            </div>
                            <div class="box-body">
                                <form id="formularioTotales">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Pasajero</th>
                                                <th class="text-center">Costo COP</th>
                                                <th class="text-center">Costo <span class="moneda"></span></th>
                                                <th class="text-center">Precio <span class="moneda"></span></th>
                                                <th class="text-center">Redondeo <span class="moneda"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody id="contenidoDetallePax"></tbody>
                                    </table>
                                </form>
                            </div>
                            <div class="box-footer text-right">
                                <button class="btn color-magical" type="submit" form="formularioTotales">
                                    <i class="fa fa-save"></i> Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="col-md-4">
                <ul class="timeline" id="contenidoItinerario"></ul>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPasajeros">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>                 
                <h3 class="modal-title">
                    <span id="modalPasajerosTitulo"></span>
                    <small> Asignar pasajeros</small>
                </h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioPasajeros">
                    <div class="form-group">
                        <label for="pasajero" class="control-label">Pasajero:</label>
                        <select id="pasajero" name="pasajero" class="form-control" required></select>
                    </div>
                </form>
                <div class="text-center">
                    <button class="btn color-magical" type="submit" form="formularioPasajeros">
                        <i class="fa fa-arrow-down"></i>
                    </button>
                    <button id="agregarTodos" class="btn color-magical" type="button">
                        Todos
                    </button>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr>
                                <th class="text-center">Pasajero</th>
                                <th class="text-center">Opciones</th>
                            </tr> 
                        </thead> 
                        <tbody id="contenidoPasajeros"></tbody> 
                    </table>
                </div>
            </div>
        </div>             
    </div>         
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var idViaje = <?=$_REQUEST['idV']?>;
    var id
    var boton
    var pasajeros
    var valores = []
    var opcionEscojida
    var moneda
    var tasa
    var margen
    var estado

    function initLogin(user){
        $('.numero').number( true, 0,',','.')
        $('.hora_inicio').datetimepicker({
            format: 'HH:mm'
        })
        $('#botonEditarDetalle').hide()

        //consultar el viaje para obtener nombre y fechas para armar los días
        procesarRegistro('viajes', 'select', {id: idViaje}, function(r){
            //Coloco el nombre de la reserva en el titulo
            pasajeros = r.data[0].pasajeros
            $('#nombreViaje').text(r.data[0].nombre)
            $('.tituloPasajeros').text(r.data[0].pasajeros)
            $('#pasajeros').val(pasajeros)
            moneda = r.data[0].moneda
            $('.moneda').text(r.data[0].moneda)
            tasa = r.data[0].tasa
            $('#tasa').text(currency(r.data[0].tasa,0))
            margen = r.data[0].margen
            $('#margen').text(r.data[0].margen)            
            estado = r.data[0].estado

            //llenar dinamicamente pasajeros para detalle px
            for (let i = 1; i <= r.data[0].pasajeros; i++) {
                $('#pasajero').append('<option value="Pasajero '+i+'">Pasajero '+i+'</option>')
            }
            
            //Calcular el numero de días
            let inicio = moment(r.data[0].fecha_inicio, 'YYYY-MM-DD')
            let fin = moment(r.data[0].fecha_fin, 'YYYY-MM-DD')
            let dias = fin.diff(inicio, 'days')
            //iteración para llenar combo de días
            for (let i = 0; i <= dias; i++) {               
                $('#fecha_inicio').append("<option value="+inicio.format('YYYY-MM-DD')+">"+inicio.format('DD-MMM-YYYY')+"</option>")
                inicio.add(1, 'days')
            }
        })        

        llenarSelect('destinos', 'getDestinos', {estado:'Activo'}, 'destinos', 'nombre', 1)
        llenarSelect('tiposServicio', 'getTiposServicio', {estado:'Activo'}, 'tipos_servicio', 'nombre', 1)

        //Cuando se escoge hospedaje o paquete se bloquea la hora
        $('#tipos_servicio').on('change', function(){            
            if($(this).val() == 3 || $(this).val() == 4 || $(this).val() == 7 || $(this).val() == 8){
                $('#hora_inicio').val('')
                $('#hora_inicio').prop('disabled', 'disabled')                
            }else{
                $('#hora_inicio').prop('disabled', false)
            }
        })

        //Aqui se llena el select de proveedor
        $('.filtroProveedor').on('change', function(){
            let datos = {
                destino:$('#destinos').val(),
                tipo_servicio:$('#tipos_servicio').val()
            }
            llenarSelect('proveedores', 'getProveedoresPorFiltro', datos, 'proveedor', 'nombre', 1)
        })

        //Aqui se hace el multifiltro del servicio
        $('.filtroServicios').on('change', function(){
            let datos = {
                destino:$('#destinos').val(),
                tipo_servicio:$('#tipos_servicio').val(),
                proveedor:$('#proveedor').val()
            }
            llenarSelect('servicios', 'getServiciosPorFiltro', datos, 'servicios', 'nombre', 1)
        })
        $('#servicios').on('change', function(){
            procesarRegistro('servicios', 'getUnidad', {id:$(this).val()}, function(r){
                $('#unidad_medida').text(r.data[0].um)
            })
            procesarRegistro('opciones', 'select', {fk_servicios: $(this).val()}, function(r){
                valores = []
                $('#fk_opciones').empty()
                $("#fk_opciones").append("<option value=''>Seleccione...</option>")
                for (i = 0; i < r.data.length; i++){
                    valores[r.data[i].id] = {
                        tipo: r.data[i].tipo_costo,
                        costo: r.data[i].costo
                    }
                    $("#fk_opciones").append("<option value="+r.data[i].id+">"+r.data[i].opcion+"</option>")
                }
            })
        })

        cargarRegistros({viaje: idViaje, estado: 'Activo'}, 'crear')

        //Calculo automatico de costos
        $('#fk_opciones').on('change', function(){
            $('#indicativoTipo').text(valores[$(this).val()].tipo)
            $('#indicativoCosto').val(valores[$(this).val()].costo)            
            opcionEscojida = $(this).val()
            calcularTotalDetalle()
        })
        $('#costo').on('change', function(){
            $('#total').val($('#costo').val()*$('#pasajeros').val()*$('#cantidad').val())
        })
        $('#pasajeros').on('change', function(){
            calcularTotalDetalle()
        })
        $('#cantidad').on('change', function(){
            $('#total').val($('#costo').val()*$('#pasajeros').val()*$('#cantidad').val())
        })

        //Logica de creación de servicios
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioDetalle').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_viajes = idViaje
            data.estado = 'Gestionar'
            if(boton == "botonGuardarDetalle"){
                procesarRegistro('viajesDetalle', 'insert', data, function(r){
                    cargarRegistros({id:r.insertId}, 'crear')
                    $('#formularioDetalle')[0].reset()
                    $('#hora_inicio').prop('disabled', false)
                    $('#pasajeros').val(pasajeros)
                    $('#modalDetalle').modal('hide')
                })    
            }else{                
                data.id = id
                data.estado = 'Gestionar'
                procesarRegistro('viajesDetalle', 'update', data, function(r){
                    cargarRegistros({id:id}, 'actualizar')
                    $('#formularioDetalle')[0].reset()
                    $('#hora_inicio').prop('disabled', false)
                    $('#pasajeros').val(pasajeros)
                    $('#botonGuardarDetalle').show('slow')
                    $('#botonEditarDetalle').hide('slow')
                    $('#modalDetalle').modal('hide')
                })
            }
        })

        //Esta es la parte de pasajeros
        $('#formularioPasajeros').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_viajes_detalle = id                
            procesarRegistro('viajesDetallePax', 'insert', data, function(r){                
                $('#formularioPasajeros')[0].reset()
                cargarRegistrosPasajeros({id: r.insertId})
                calcularTotalesByPax()
                alertaPasajerosFaltantes()
            })            
        })

        $('#agregarTodos').on('click', function(){
            procesarRegistro('viajesDetallePax', 'agregarTodos', {detalle: id, cantidad: pasajeros}, function(r){
                cargarRegistrosPasajeros({fk_viajes_detalle: id})
                calcularTotalesByPax()
                alertaPasajerosFaltantes()
            })
        })

        //Guardar totales
        $('#formularioTotales').on('submit', function(e){
            e.preventDefault()
            let data =  parsearFormulario($(this))            
            procesarRegistro('viajes', 'fijarValores', {id:idViaje, redondeo_pasajeros: data, precio:$('#redondeo_total').val(), estado: estado}, function(r){
                swal('Perfecto!', 'Se fijaron correctamente los valores redondeados', 'success')
            })
        })
    }    

    function cargarRegistros(data, accion){
        procesarRegistro('viajesDetalle', 'getDetalle', data, function(r){
            let fila
            let total
            let fondo = {
                Gestionar: 'bg-gray',
                Bloqueado: 'bg-amarillo',
                Reservado: 'bg-green',
                Devuelto: 'bg-red'
            }            
            for(let i = 0; i < r.data.length; i++){                
                total = r.data[i].pasajeros * r.data[i].costo * r.data[i].cantidad
                fila += '<tr id="'+r.data[i].id+'" class="'+fondo[r.data[i].estado]+'">'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].hora_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].destino+'</td>'+                            
                            '<td>'+r.data[i].servicio+'</td>'+
                            '<td>'+r.data[i].opcion+'</td>'+
                            '<td>'+r.data[i].detalle+'</td>'+                            
                            '<td class="text-center">$'+currency(r.data[i].costo,0)+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].cantidad+'</td>'+
                            '<td class="text-center">$'+currency(total,0)+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+
                            '<td class="text-center">'+
                                '<button class="btn btn-default btn-xs" title="Editar item" onClick="mostrarModalEditarDetalle('+r.data[i].id+','+total+')"><i class="fa fa-edit"></i></button> '+
                                '<button id="indicador_'+r.data[i].id+'" class="btn btn-warning btn-xs" title="Relacionar viajeros" onClick="cuadrarPasajeros('+r.data[i].id+',\''+r.data[i].servicio+'\')"><i class="fa fa-users"></i></button> '+
                                '<button class="btn btn-default btn-xs" title="Eliminar detalle" onClick="eliminarDetalle('+r.data[i].id+')"><i class="fa fa-close"></i></button>'+
                            '</td>'+
                        '</tr>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            calcularTotalesByPax()
            alertaPasajerosFaltantes()
            $('#contenidoItinerario').empty()
            construirItinerario(idViaje, function(r){
                console.log('Ok itinerario')
            })
        })
    }    

    function mostrarModalEditarDetalle(idDetalle, t){
        id = idDetalle
        $('#formularioDetalle')[0].reset()
        $('#total').val(t)
        llenarFormulario('formularioDetalle','viajesDetalle', 'select', {'id':idDetalle}, function(r){
            procesarRegistro('opciones', 'select', {id:r.data[0].fk_opciones}, function(r){
                valores[r.data[0].id] = {
                    tipo: r.data[0].tipo_valor,
                    costo: r.data[0].costo
                }
                opcionEscojida = r.data[0].id
                $('#fk_opciones').empty()
                $("#fk_opciones").append("<option value="+r.data[0].id+">"+r.data[0].opcion+"</option>")
                $('#indicativoTipo').text(r.data[0].tipo_valor)
                $('#indicativoCosto').val(r.data[0].costo)
            })
            $('#botonGuardarDetalle').hide('slow')
            $('#botonEditarDetalle').show('slow')
        })
    }

    function eliminarDetalle(idDetalle){
        swal('¿Seguro que desea cancelar el servicio?')
        .then((value) => {
            if (value) {
                procesarRegistro('viajesDetalle', 'update', {id:idDetalle, estado:'Cancelado'}, function(r){                    
                    $('#'+idDetalle).hide('slow')
                    calcularTotalesByPax()
                })
            }
        })
    }

    function calcularTotalDetalle(){
        //Aqui evaluo si es costo grupal o unitario para saber el valor individual
        let valor_local = valores[opcionEscojida].costo
        if(valores[opcionEscojida].tipo == 'Grupal'){        
            valor_local = valores[opcionEscojida].costo / $('#pasajeros').val()
        }
        $('#costo').val(valor_local)
        $('#total').val(valor_local * $('#pasajeros').val() * $('#cantidad').val())
    }    

    /*
    Este es el bloque para cuadrar pasajeros
    */
    function cuadrarPasajeros(idDetalle, servicio, dia){
        id = idDetalle
        $('#modalPasajerosTitulo').text(servicio) 
        $('#contenidoPasajeros').html('')
        cargarRegistrosPasajeros({fk_viajes_detalle: idDetalle})
        $('#modalPasajeros').modal('show')
    }

    function cargarRegistrosPasajeros(data){
        procesarRegistro('viajesDetallePax', 'select', data, function(r){
            let fila = ''
            for(let i = 0; i < r.data.length; i++){
                fila += '<tr id="p_'+r.data[i].id+'">'+
                    '<td>'+r.data[i].pasajero+'</td>'+
                    '<td class="text-center">'+
                        '<button class="btn btn-default btn-xs" onClick="borrarPasajeros('+r.data[i].id+')"><i class="fa fa-times"></i></button>'+
                    '</td>'+
                '</tr>'
            }            
            $('#contenidoPasajeros').append(fila)
        })
    }

    function borrarPasajeros(idPasajeros){
        swal('¿Esta seguro de borrar el pasajero?')
        .then((value) => {
            if (value) {
                procesarRegistro('viajesDetallePax', 'delete', {'id':idPasajeros}, function(r){
                    $('#p_'+idPasajeros).remove()
                    calcularTotalesByPax()
                    alertaPasajerosFaltantes()
                })
            }
        })
    }

    function calcularTotalesByPax(){
        let totalCOP = 0
        let OTRO = 0
        let totalOTRO = 0
        let margenLocal = 0
        let margenLocalTotal = 0
        procesarRegistro('viajesDetallePax', 'getTotalesByPax', {id: idViaje}, function(r){
            if(r.data.length != 0){
                $('#contenidoDetallePax').empty()
                for(let i = 0; i < r.data.length; i++){
                    OTRO = r.data[i].costo/tasa
                    margenLocal = OTRO/((100-margen)/100)
                    $('#contenidoDetallePax').append('<tr>'+
                        '<td class="text-center">'+r.data[i].pasajero+'</td>'+
                        '<td class="text-right">'+currency(r.data[i].costo,0)+'</td>'+
                        '<td class="text-right">'+currency(OTRO,0)+'</td>'+
                        '<td class="text-right">'+currency(margenLocal,0)+'</td>'+
                        '<td class="text-right"><input id="'+r.data[i].pasajero+'" name="'+r.data[i].pasajero+'" class="form-control redondeo" type="text" value=0></td>'+
                        '</tr>')
                    totalCOP += r.data[i].costo
                    totalOTRO += OTRO
                    margenLocalTotal += margenLocal 
                }
                $('#contenidoDetallePax').append('<tr>'+
                    '<td class="text-center">TOTAL</td>'+
                    '<td class="text-right">'+currency(totalCOP,0)+'</td>'+
                    '<td class="text-right">'+currency(totalOTRO,0)+'</td>'+
                    '<td class="text-right">'+currency(margenLocalTotal,0)+'</td>'+
                    '<td class="text-right"><input id="redondeo_total" class="form-control totalRedondeo" type="text" readonly="readonly"></td>'+
                    '</tr>')
                //$('.redondeo').number( true, 0,',','.')
                $('.totalRedondeo').number( true, 0,',','.')
                $('.redondeo').on('change', function(){
                    let totalRedondeo = 0
                    $('.redondeo').each(function(){                        
                        totalRedondeo += parseInt($(this).val())
                    })
                    $('#redondeo_total').val(totalRedondeo)
                })
                //Llenar redondeos
                procesarRegistro('viajes', 'select', {id:idViaje}, function(r){
                    if(r.data[0].redondeo_pasajeros != ''){
                        let redondeos = JSON.parse(r.data[0].redondeo_pasajeros)
                        $.each(redondeos, function(key, val){
                            $("[id='"+key+"']").val(val)                        
                        })
                        $('#redondeo_total').val(r.data[0].precio)    
                    }                    
                })
            }
        })        
    }

    function alertaPasajerosFaltantes(){
        let centinela = 0
        procesarRegistro('viajesDetallePax', 'getPaxFaltantes', {id: idViaje}, function(r){            
            if(r.data.length != 0){
                for(let i = 0; i < r.data.length; i++){                    
                    if(r.data[i].pasajeros == r.data[i].cantidad){
                        $('#indicador_'+r.data[i].id).removeClass('btn-warning').addClass('btn-default')                        
                    }else{
                        centinela = 1
                        $('#indicador_'+r.data[i].id).removeClass('btn-default').addClass('btn-warning')
                    }
                }
            }
        })
    }
</script>
</body>
</html>