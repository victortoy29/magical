<?php
require 'header.php';
?>

<div class="content-wrapper">
    <section class="content-header">        
        <div class="row">
            <div class="col-sm-3">
                <h4>
                    Dashboard
                    <small>Administración</small>
                </h4>        
            </div>            
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="fecha_inicio" class="col-sm-4 control-label">Fecha inicio</label>
                    <div class="col-sm-8">
                        <input id="fecha_inicio" class="form-control calendario" type="text">
                    </div>
                </div>
            </div>            
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="fecha_fin" class="col-sm-4 control-label">Fecha fin</label>
                    <div class="col-sm-8">
                        <input id="fecha_fin" class="form-control calendario" type="text">
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                <button class="btn btn-success" type="button" id="consultar">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>        
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-arrow-up"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Ingresos</span>
                        <span class="info-box-number" id="totalIngresos">$0</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-arrow-down"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Egresos</span>
                        <span class="info-box-number" id="totalEgresos">$0</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="info-box">
                    <span class="info-box-icon color-magical"><i class="fa fa-check"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Reservas efectivas</span>
                        <span class="info-box-number" id="totalReservas">0</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">            
            <div class="col-sm-4">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="estadoViajes" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="estadoServicios" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="seguimientoDia" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="causalesCancelacion" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="canales" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="paisesProcedencia" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="tiposServicios" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="proveedores" style="height: 700px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box box-info">                    
                    <div class="box-body">
                        <div class="chart">
                            <div id="pagoProveedores" style="height: 700px"></div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>
</div>
<?php
require 'footer.php';
?>
<script type="text/javascript">
    function initLogin(data){
        $('.calendario').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        })

        let info = {
            fecha_inicio: moment().format('YYYY-MM-01'),
            fecha_fin: moment().format("YYYY-MM-") + moment().daysInMonth()
        }
        cargarDashboard(info)

        $('#consultar').on('click', function(){
            info = {
                fecha_inicio: $('#fecha_inicio').val(),
                fecha_fin: $('#fecha_fin').val()
            }
            cargarDashboard(info)
        })        
    }

    function cargarDashboard(info){
        procesarRegistro('reportes', 'getIngresos', info, function(r){
            $('#totalIngresos').text('$'+currency(r.data[0].total,0))
        })

        procesarRegistro('reportes', 'getEgresos', info, function(r){
            $('#totalEgresos').text('$'+currency(r.data[0].total,0))
        })

        procesarRegistro('reportes', 'getReservas', info, function(r){
            $('#totalReservas').text(r.data[0].total)
        })

        procesarRegistro('reportes', 'estadoViajes', {'':''}, function(r){            
            let categorias = []
            let serie = {
                name: 'Valores',
                data: []                
            }
            for (let i = 0; i < r.data.length; i++){
                categorias.push(r.data[i].estado)
                serie.data.push(r.data[i].cantidad)
            }
            
            Highcharts.chart('estadoViajes', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Estado de solicitudes'
                },
                subtitle: {
                    text: 'Estado'
                },
                xAxis: {
                    categories: categorias,
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }
                },
                colors:['#3785EE'],
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [serie]
            })
        })

        procesarRegistro('reportes', 'estadoServicios', {'':''}, function(r){            
            let categorias = ['Cotización', 'Reserva']
            let posEstado = ['Gestionar', 'Bloqueado', 'Devuelto']
            let series = [
                {
                    name: 'Gestionar',
                    data: [0,0]
                },
                {
                    name: 'Bloqueado',
                    data: [0,0]
                },
                {
                    name: 'Devuelto',
                    data: [0,0]  
                }
            ]
            for (let i = 0; i < r.data.length; i++){
                pos1 = posEstado.indexOf(r.data[i].vd)
                pos2 = categorias.indexOf(r.data[i].ve)
                series[pos1].data[pos2] = r.data[i].cantidad
            }

            Highcharts.chart('estadoServicios', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Servicios por gestionar'
                },
                xAxis: {
                    categories: ['Cotización', 'Reserva']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Cantidad'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                colors:['#D2D6DE','#F4D03F','#DD4B39'],
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: series
            })
        })

        procesarRegistro('reportes','seguimientoDia',info,function(r){            
            let categorias = []
            let titulos = {
                Cotizaciones : 0,
                Reservas: 0,
                Canceladas: 0
            }
            let series = [
                {
                    name: 'Cotizaciones',
                    data: []
                },
                {
                    name: 'Reservas',
                    data: []
                },
                {
                    name: 'Canceladas',
                    data: []
                }
            ]
            let inicio = moment(info.fecha_inicio, 'YYYY-MM-DD')
            let fin = moment(info.fecha_fin, 'YYYY-MM-DD')
            let dias = fin.diff(inicio, 'days')
            let posiciones = ['Cotizaciones','Reservas','Canceladas']
            for (let i = 0; i <= dias; i++) {                
                categorias.push(inicio.format('YYYY-MM-DD'))
                series[0].data.push(0)
                series[1].data.push(0)
                series[2].data.push(0)
                inicio.add(1, 'days')
            }            
            for (let i = 0; i < r.data.length; i++){
                titulos[r.data[i].Cotizaciones] += r.data[i].cantidad
                pos1 = posiciones.indexOf(r.data[i].Cotizaciones)
                pos2 = categorias.indexOf(r.data[i].fecha)
                series[pos1].data[pos2] = r.data[i].cantidad
            }
            series[0].name = 'Cotizaciones ('+titulos.Cotizaciones+')'
            series[1].name = 'Reservas ('+titulos.Reservas+')'
            series[2].name = 'Canceladas ('+titulos.Canceladas+')'
            
            Highcharts.chart('seguimientoDia', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Historico por día'
                },
                subtitle: {
                    text: 'Comparativo entre estados'
                },
                xAxis: {
                    categories: categorias
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }               
                },
                colors:['#3785EE','#51D332','#C43328'],
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    },
                    series: {
                        label: {
                            connectorAllowed: false
                        }
                    }
                },
                series: series
            })
        })        

        //Anulaciones
        procesarRegistro('reportes', 'causalesCancelacion', info, function(r){
            datos = []
            for (let i = 0; i < r.data.length; i++){
                datos.push([r.data[i].nombre,r.data[i].cantidad])
            }

            Highcharts.chart('causalesCancelacion', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'Causales de anulación'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                colors:['#3785EE','#FA6F0F','#51D332','#C43328'],
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Total',
                    data: datos
                }]
            })
        })

        procesarRegistro('reportes', 'canales', info, function(r){
            let categorias = ['Cotización', 'Reserva', 'Terminada', 'Cancelada']
            let series = []
            let centinela = ''
            let posicion = -1;
            for (let i = 0; i < r.data.length; i++){
                if(centinela != r.data[i].nombre){
                    posicion++
                    series.push({
                        name: r.data[i].nombre,
                        data: []
                    })

                    //llenar con ceros
                    for(let j = 0; j < categorias.length; j++){
                        series[posicion].data.push(0)
                    }

                    centinela = r.data[i].nombre
                }                
                //Colocar la cantidad en la posición
                pos = categorias.indexOf(r.data[i].estado)
                series[posicion].data[pos] = r.data[i].cantidad
            }

            Highcharts.chart('canales', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Tipos de cliente'
                },
                xAxis: {
                    categories: ['Cotización', 'Reserva', 'Terminada', 'Cancelada']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Cantidad'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: series
            })
        })

        procesarRegistro('reportes', 'paisesProcedencia', info, function(r){
            let categorias = ['Cotización', 'Reserva', 'Terminada', 'Cancelada']
            let series = []
            let centinela = ''
            let posicion = -1;
            for (let i = 0; i < r.data.length; i++){
                if(centinela != r.data[i].nombre){
                    posicion++
                    series.push({
                        name: r.data[i].nombre,
                        data: []
                    })

                    //llenar con ceros
                    for(let j = 0; j < categorias.length; j++){
                        series[posicion].data.push(0)
                    }

                    centinela = r.data[i].nombre
                }                
                //Colocar la cantidad en la posición
                pos = categorias.indexOf(r.data[i].estado)
                series[posicion].data[pos] = r.data[i].cantidad
            }

            Highcharts.chart('paisesProcedencia', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Ciudades de procedencia'
                },
                xAxis: {
                    categories: ['Cotización', 'Reserva', 'Terminada', 'Cancelada']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Cantidad'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: series
            })
        })

        procesarRegistro('reportes', 'tiposServicios', info, function(r){
            let categorias = []
            let serie = {
                name: 'Valores',
                data: []                
            }
            for (let i = 0; i < r.data.length; i++){
                categorias.push(r.data[i].nombre)
                serie.data.push(r.data[i].cantidad)
            }
            
            Highcharts.chart('tiposServicios', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Cantidad de servicios'
                },
                subtitle: {
                    text: 'Por tipo'
                },
                xAxis: {
                    categories: categorias,
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }
                },
                colors:['#3785EE'],
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [serie]
            })
        })

        procesarRegistro('reportes', 'proveedores', info, function(r){
            let categorias = []
            let serie = {
                name: 'Proveedor',
                data: []                
            }
            for (let i = 0; i < r.data.length; i++){
                categorias.push(r.data[i].nombre)
                serie.data.push(r.data[i].cantidad)
            }
            
            Highcharts.chart('proveedores', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Cantidad de servicios'
                },
                subtitle: {
                    text: 'por proveedor'
                },
                xAxis: {
                    categories: categorias,
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }
                },
                colors:['#FA6F0F'],
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                series: [serie]
            })
        })

        procesarRegistro('reportes', 'pagoProveedores', info, function(r){
            let categorias = []
            let serie = {
                name: 'Proveedor',
                data: []                
            }
            for (let i = 0; i < r.data.length; i++){
                categorias.push(r.data[i].nombre)
                serie.data.push(r.data[i].valor)
            }
            
            Highcharts.chart('pagoProveedores', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Pago a proveedores'
                },
                subtitle: {
                    text: 'por proveedor'
                },
                xAxis: {
                    categories: categorias,
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                series: [serie]
            })
        })
    }   
</script>
</body>
</html>