    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Magical management
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2019 <a href="https://magicalcolombia.com/">Magical</a>.</strong> All rights reserved.
    </footer>
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- moment -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/moment/locale/es.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Datetimepicker 4.17.47 -->
<script src="bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Custom number -->
<script src="plugins/customd-jquery-number/jquery.number.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!--Higtcharts-->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<!--Sweet Alert-->
<script src="bower_components/sweetalert/docs/assets/sweetalert/sweetalert.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="assets/js/funciones.js"></script>
<script type="text/javascript">
    $(function(){
        procesarRegistro('helpers', 'sesion', {'':''}, function(r){
            if (r.ejecuto == true) {
                if(jQuery.isEmptyObject(r.data)){
                    //Cuando no esta logueado                    
                    window.location.href = "index.html"
                }else{
                    //Cuando esta logueado
                    initLogin(r.data)                    
                    $('.nombreUsuario').text(r.data.usuario.nombre)
                    validarExistenciaImagen('.menu_foto', 'assets/img/usuarios/'+r.data.usuario.id+'.jpg', 'assets/img/usuarios/default.png')
                    $('#perfilUsuario').text(r.data.usuario.perfil)
                    //espacio de permisos
                    $('#menu li').hide()
                    for(let i = 0; i < r.permisos.length; i++){
                        $('#'+r.permisos[i]).show()
                        $('#'+r.permisos[i]+' li').show()
                    }
                }   
            }
            $(':input[required]').css('box-shadow','1px 1px red')
        })
        /*$('#salir').on('click',function(){
            procesarRegistro('helpers', 'sesion', {destroy:1}, function(r){
                if (r.ejecuto == true) {
                    window.location.href = "index.html"   
                }
            })                
        })*/
    })
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
user experience. -->