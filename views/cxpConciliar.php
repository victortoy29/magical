<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            CxP
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> CxP</a></li>
            <li> Conciliar</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Reserva</th>
                                        <th class="text-center">Nombre reserva</th>
                                        <th class="text-center">Proveedor</th>
                                        <th class="text-center">Valor calculado</th>
                                        <th class="text-center">Valor conciliado</th>
                                        <th class="text-center">Saldo</th>                                        
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalConciliacion">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>
                <h3 class="modal-title" id="modalConciliacionTitulo"></h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioConciliacion" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="valor" class="control-label">Valor conciliado</label>
                        <input id="valor" name="valor" class="form-control numero" type="text" required="required">
                    </div>
                    <div class="form-group">
                        <label for="archivo" class="control-label">Cuenta por pagar</label>
                        <input  type="file" id="archivo" name="archivo" required="required">
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" form="formularioConciliacion">
                    <i class="fa fa-save"></i> Crear
                </button>
            </div>            
        </div>             
    </div>         
</div>

<div class="modal fade" id="modalProveedores">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span>Información proveedor</span>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody id="contenidoProveedores"></tbody>
                </table>
                <div class="text-center">
                    <button id="mostrarContactos" class="btn btn-primary" type="button">
                        <i class="fa fa-phone"></i>
                    </button>
                </div>
                <br>
                <div id="contenidoContactos"></div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id

    function initLogin(user){
        $('.numero').number( true, 0,',','.')

        cargarRegistros({estado:'Conciliar'}, 'crear')

        $('#formularioConciliacion').on('submit',function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            let archivo = $('#archivo')[0].files[0]
            let fd = new FormData()
            fd.append('objeto', 'cxp')
            fd.append('metodo','conciliar')
            fd.append('file',archivo)
            fd.append('datos[cxp]',id)
            fd.append('datos[valor]',data.valor)                
            $.ajax({
                url: 'class/frontController.php',
                type: 'post',
                dataType: 'json',
                data: fd,
                contentType: false,
                processData: false,
                success: function(r){
                    if(r.ejecuto == true){
                        $('#formularioConciliacion')[0].reset()
                         $('#'+id).hide('slow')
                        $('#modalConciliacion').modal('hide')
                    }    
                }
            })            
        })

        //Mostrar contactos de proveedor
        $('#mostrarContactos').on('click', function(){
            let fila = ''
            procesarRegistro('proveedoresContactos', 'select', {fk_proveedores: idPro}, function(r){
                fila = '<table class="table table-bordered"><tr><th>Nombre</th><th>Telefono</th><th>Correo</th><th>Observación</th></tr>'
                for(let i = 0; i < r.data.length; i++){
                    fila += '<tr>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].telefono+'</td>'+
                            '<td>'+r.data[i].correo+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+
                            '</tr>'
                }
                fila += '</table>'
                $('#contenidoContactos').append(fila)
            })
        })
    }

    function cargarRegistros(data, accion){
        procesarRegistro('cxp', 'getCuentas', data, function(r){
            let fila
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td class="text-center">R-'+r.data[i].codigo_reserva+'</td>'+
                            '<td class="text-center">'+r.data[i].reserva+'</td>'+
                            '<td><button type="button" class="btn btn-link" onClick="infoProveedores('+r.data[i].idp+')">'+r.data[i].nombre+'</button></td>'+
                            '<td class="text-right">$'+currency(r.data[i].valor_calculado,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].valor_conciliado,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].saldo,0)+'</td>'+                            
                            '<td class="text-center">'+
                                '<button class="btn btn-default btn-xs" title="Conciliar" onClick="cargarConciliacion('+r.data[i].id+','+r.data[i].codigo_reserva+','+r.data[i].saldo+',\''+r.data[i].reserva+'\')"><i class="fa fa-exchange"></i></button>'+
                            '</td>'+
                        '</tr>'
            }            
            $('#contenido').append(fila)            
        })
    }

    function infoProveedores(idProveedor){
        idPro = idProveedor
        $('#contenidoProveedores').empty()
        $('#contenidoContactos').empty()
        procesarRegistro('proveedores', 'select', {id:idProveedor}, function(r){
            $('#contenidoProveedores').append(
                '<tr><td>Nombre:</td><td>'+r.data[0].nombre+'</td></tr>'+
                '<tr><td>Tipo documento:</td><td>'+r.data[0].tipo_doc+'</td></tr>'+
                '<tr><td>Número:</td><td>'+r.data[0].numero+'</td></tr>'+
                '<tr><td>Telefono:</td><td>'+r.data[0].telefono+'</td></tr>'+
                '<tr><td>Correo:</td><td>'+r.data[0].correo+'</td></tr>'
            )
        })
        $('#modalProveedores').modal('show')
    }

    /*Logica de creación de Conciliacion*/
    function cargarConciliacion(idcxp, cr, max, nom){
        id = idcxp
        $('#modalConciliacionTitulo').html('<span>R-'+cr+'</span><small> '+nom+'</small>')
        //$('#modalConciliacionTitulo').text('Total para R-'+cr)
        $('#formularioConciliacion')[0].reset()
        $('#modalConciliacion').modal('show')
    }    
</script>
</body>
</html>