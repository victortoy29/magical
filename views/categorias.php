<?php
require_once 'database.php';
require_once 'model.php';

class categorias extends model{
	protected $tabla = 'categorias';

	public function getCategorias($datos){
		$sql = "SELECT *
				FROM categorias
				WHERE estado = 'Activo'
				ORDER BY nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}