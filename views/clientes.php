<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Comercial</a></li>
            <li class="active">Clientes</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">                        
                        <h3 class="box-title">Listado</h3>
                        <div class="pull-right">
                            <button id="botonMostrarModalClientes" class="btn color-magical btn-sm">
                                <i class="fa fa-plus"></i>
                            </button>                            
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Canal</th>
                                        <th class="text-center">País</th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Teléfono</th>
                                        <th class="text-center">Correo</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>    
</div>
<div class="modal fade" id="modalClientes">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalClientesTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioClientes">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fk_canales" class="control-label">Canal de venta:</label>
                                <select id="fk_canales" name="fk_canales" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fk_paises" class="control-label">País:</label>
                                <select id="fk_paises" name="fk_paises" class="form-control paises" required></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo_doc" class="control-label">Tipo identificación</label>
                                <select id="tipo_doc" name="tipo_doc" class="form-control">
                                    <option value="">Seleccione...</option>
                                    <option value="CC">CC</option>
                                    <option value="NIT">NIT</option>
                                    <option value="CE">CE</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="numero" class="control-label">Numero de identificación</label>
                                <input id="numero" name="numero" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="direccion" class="control-label">Dirección</label>
                        <input id="direccion" name="direccion" class="form-control" type="text">
                    </div>                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono" class="control-label">Teléfono</label>
                                <input id="telefono" name="telefono" class="form-control" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
	                        <div class="form-group">
		                        <label for="correo" class="control-label">Correo</label>
		                        <input id="correo" name="correo" class="form-control" type="email">
		                    </div>
		                </div>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarClientes" class="btn color-magical btn-submit" type="submit" form="formularioClientes">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarClientes" class="btn btn-default btn-submit" type="submit" form="formularioClientes">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalViajes">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span id="modalViajesTitulo"></span>
                    <small> Crear cotización</small>
                </h4>
            </div>
            <div class="modal-body">
                <form id="formularioViajes">
                    <div class="form-group">
                        <label for="nombre_viaje" class="control-label">Nombre del viaje</label>
                        <input id="nombre_viaje" name="nombre_viaje" class="form-control" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre de la reserva</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" required>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fk_paises" class="control-label">País de procedencia:</label>
                                <select id="fk_paises" name="fk_paises" class="form-control paises" required></select>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pasajeros" class="control-label">Cantidad de pasajeros</label>
                                <input id="pasajeros" name="pasajeros" class="form-control" type="number" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="idioma" class="control-label">Idioma</label>
                                <select id="idioma" name="idioma" class="form-control" required>                            
                                    <option value="es">Español</option>
                                    <option value="en">Ingles</option>
                                    <option value="it">Italiano</option>
                                </select>
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha_inicio" class="control-label">Fecha inicio</label>
                                <input id="fecha_inicio" name="fecha_inicio" class="form-control calendario" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha_fin" class="control-label">Fecha fin</label>
                                <input id="fecha_fin" name="fecha_fin" class="form-control calendario" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="moneda" class="control-label">Moneda</label>
                                <select id="moneda" name="moneda" class="form-control" required>                            
                                    <option value="Euros">Euro</option>
                                    <option value="Dolares">Dolar</option>
                                    <option value="Pesos">Peso</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tasa">Tasa:</label>
                                <input id="tasa" name="tasa" class="form-control numero" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="margen">Margen(%):</label>
                                <input id="margen" name="margen" class="form-control" type="number" min=0 max=100 required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observaciones</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-submit" type="submit" form="formularioViajes">
                    <i class="fa fa-save"></i> Crear
                </button>                
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton    

    function initLogin(user){
        $('.numero').number( true, 0,',','.')
        $('#fecha_inicio').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        })
        $('#fecha_fin').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es',
            useCurrent: false
        })
        $("#fecha_inicio").on("dp.change", function (e) {
            $('#fecha_fin').data("DateTimePicker").minDate(e.date);
        });
        $("#fecha_fin").on("dp.change", function (e) {
            $('#fecha_inicio').data("DateTimePicker").maxDate(e.date);
        })
        

        //Llenar paises
        llenarSelectByClass('paises', 'getPaises', {estado:'Activo'}, 'paises', 'nombre', 1)
        llenarSelect('canales', 'getCanales', {estado:'Activo'}, 'fk_canales', 'nombre', 1)
        
        cargarRegistros({estado: 'Activo'}, 'crear', function(){
            $('#tablaContenido').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
            
        })

        $('#botonMostrarModalClientes').on('click', function(){
            $('#formularioClientes')[0].reset()
            $('#modalClientesTitulo').text('Crear cliente')
            $('#botonGuardarClientes').show()
            $('#botonEditarClientes').hide()            
            $('#modalClientes').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        //Crear cliente
        $('#formularioClientes').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarClientes"){                
                procesarRegistro('clientes', 'insert', data, function(r){
                    swal('Perfecto!', 'Se creo correctamente', 'success')
                    cargarRegistros({id:r.insertId}, 'crear', function(){
                        $('#modalClientes').modal('hide')
                    })                    
                })    
            }else{                
                data.id = id
                procesarRegistro('clientes', 'update', data, function(r){                    
                    swal('Perfecto!', 'Se actualizo correctamente', 'success')
                    cargarRegistros({id:id}, 'actualizar', function(){
                        $('#modalClientes').modal('hide')
                    })                    
                })
            }            
        })
        
        //Crear cotización
        $('#formularioViajes').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.fk_clientes = id
            data.estado = 'Cotización'
            procesarRegistro('viajes', 'insert', data, function(r){
                window.location.href = "views/comercialCotizaciones.php"
            })
        })        
    }

    function cargarRegistros(data, accion, callback){
        procesarRegistro('clientes', 'getClientes', data, function(r){            
            let fila            
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td>'+r.data[i].canal+'</td>'+
                            '<td>'+r.data[i].pais+'</td>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].telefono+'</td>'+
                            '<td>'+r.data[i].correo+'</td>'+
                            '<td class="text-center">'+
                                '<button class="btn btn-default btn-xs" title="Editar cliente" onClick="mostrarModalEditarClientes('+r.data[i].id+')"><i class="fa fa-pencil-square-o"></i></button> '+                               
                                '<button class="btn btn-default btn-xs" title="Crear viaje" onClick="mostrarModalViajes('+r.data[i].id+',\''+r.data[i].nombre+'\')"><i class="fa fa-plane"></i></button>'+
                            '</td>'+
                        '</tr>'
            }            
            if(accion == 'crear'){                
                $('#contenido').append(fila)                        
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }    

    function mostrarModalEditarClientes(idCliente){
        id = idCliente
        llenarFormulario('formularioClientes','clientes', 'select', {'id':idCliente}, function(r){            
            $('#modalClientesTitulo').text('Editar cliente')
            $('#botonGuardarClientes').hide()
            $('#botonEditarClientes').show()            
            $('#modalClientes').modal('show')            
        })
    }    

    /*
    Este bloque es para el modulo de Viajes
    */
    function mostrarModalViajes(idCliente, nombre){
        id = idCliente
        $('#modalViajesTitulo').text(nombre)
        $('#formularioViajes')[0].reset()
        $('#modalViajes').modal('show')
    }    
</script>
</body>
</html>