<?php
require_once '../class/viajes.php';
require_once '../class/viajesDetalle.php';
require_once '../plugins/fpdf/fpdf.php';

$objViaje = new viajes();
$viaje = $objViaje->getViajes(['campo'=>'id', 'valor'=> $_GET['idV']]);

$paginaFinal = 0;

$itinerario = [];
$inicio = $viaje['data'][0]['fecha_inicio'];
$date1 = date_create($viaje['data'][0]['fecha_inicio']);
$date2 = date_create($viaje['data'][0]['fecha_fin']);
$diff = date_diff($date1,$date2);
$dias = $diff->format("%a");
for ($i=0; $i <= $dias; $i++) { 
	$itinerario[$inicio]=[];
	$inicio = date('Y-m-d', strtotime($inicio. ' + 1 days'));
}

$objDetalle = new viajesDetalle();
$detalle = $objDetalle->getItinerario(['viaje'=> $_GET['idV']]);

for ($i=0; $i < count($detalle['data']); $i++){
	$cantidad = $detalle['data'][$i]['di'] * $detalle['data'][$i]['cantidad'];
	$fecha = $detalle['data'][$i]['fecha_inicio'];
	for($j=0; $j < $cantidad; $j++){
		$itinerario[$fecha][] = [			
			'idDestino' => $detalle['data'][$i]['idDestino'],
			'ts'=>$detalle['data'][$i]['ts'],			
			'destino' => $detalle['data'][$i]['destino'],
			'desDestinoEs' => $detalle['data'][$i]['desDestinoEs'],
			'desDestinoEn' => $detalle['data'][$i]['desDestinoEn'],
			'desDestinoIt' => $detalle['data'][$i]['desDestinoIt'],
			'idServicio'=>$detalle['data'][$i]['idServicio'],
			'servicio' => $detalle['data'][$i]['servicio'],
			'desServicioEs'=>$detalle['data'][$i]['desServicioEs'],
			'desServicioEn'=>$detalle['data'][$i]['desServicioEn'],
			'desServicioIt'=>$detalle['data'][$i]['desServicioIt'],
			'opcion' => $detalle['data'][$i]['opcion']			
		];
		$fecha = date('Y-m-d', strtotime($fecha. ' + 1 days'));
	}
}

class PDF extends FPDF{	
	// Pie de página
	function Footer(){
		global $paginaFinal;		
	    if($this->PageNo() != 1 and $this->PageNo() != $paginaFinal){
	    	$this->Image('../assets/img/logo2.png',170,280,30);
	    }
	}
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->Image('../assets/img/portada.jpg',0,17,210);
$pdf->SetFont('Arial','B',20);
$pdf->Image('../assets/img/logo2.png',50,80);
$pdf->SetY(150);
$pdf->SetTextColor(63,72,79);
$pdf->SetFont('Arial','B',15);
$pdf->Cell(190,10,'Sr. '.utf8_decode($viaje['data'][0]['cliente']),0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(190,10,'ITINERARIO PERSONALIZADO',0,1,'C');
$pdf->SetFont('Arial','B',20);
$pdf->Cell(190,10,'Colombia',0,1,'C');
$pdf->Ln(70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,10,'www.magicalcolombia.com',0,1,'C');

$pdf->AddPage();
$pdf->Image('../assets/img/usuarios/'.$viaje['data'][0]['idVendedor'].'.jpg',10,30,65);
$pdf->SetFont('Arial','B',14);
$pdf->SetXY(80,33);
$pdf->Cell(120,5,utf8_decode($viaje['data'][0]['vendedor']),0,1);
$pdf->SetFont('Arial','',13);
$pdf->SetX(80);
$pdf->Cell(120,5,'PRIVATE TRAVEL DESIGN',0,1);
$pdf->Ln(8);
$pdf->SetFont('Arial','',13);
$pdf->SetX(80);
$pdf->Cell(130,5,'Magical Colombia S.A.S.',0,1);
$pdf->SetX(80);
$pdf->Cell(130,5,'Calle 6 # 76 - 70, Local 21, C.C. Capri',0,1);
$pdf->SetX(80);
$pdf->Cell(130,5,'Santiago de Cali - Colombia',0,1);
$pdf->Ln(8);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(22,5,utf8_decode('Teléfono:'));
$pdf->SetFont('Arial','',13);
$pdf->Cell(80,5,$viaje['data'][0]['telefono'],0,1);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(15,5,'Email:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['correo'],0,1);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(17,5,'Skype:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['skype'],0,1);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(23,5,'Sitio web:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,'http://www.magicalcolombia.com',0,1);
$pdf->Ln(10);

$pdf->SetDrawColor(203,170,52);
$pdf->Line(10,105,200,105);

$pdf->SetY(105);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,15,'MI VIAJE A COLOMBIA',0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,utf8_decode('Código:'));
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,'C-'.$viaje['data'][0]['id'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,'Fecha inicio:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['fecha_inicio'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,'Fecha fin:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['fecha_fin'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,utf8_decode('Número de personas:'));
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['pasajeros'],0,1);

$pdf->Line(10,145,200,145);

$pdf->SetY(145);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,15,'ETAPAS PRINCIPALES DE MI VIAJE',0,1);

//print_r($itinerario);

$dia = 1;
$etapas = [];
foreach ($itinerario as $key => $servicios) {	
	foreach ($servicios as $value) {
		$etapas[$dia]['fecha'] = $key;			
		if($value['ts'] == 3){//hospedaje
			$etapas[$dia]['ts'] = $value['ts'];
			$etapas[$dia]['idDestino'] = $value['idDestino'];
			$etapas[$dia]['destino'] = $value['destino'];
			$etapas[$dia]['desDestino'] = $value['desDestinoEs'];
			$etapas[$dia]['alojamiento'] =  $value['servicio'];
			continue;
		}
		if($value['ts'] == 4){//Paquete
			$etapas[$dia]['ts'] = $value['ts'];
			$etapas[$dia]['idDestino'] = $value['idDestino'];
			$etapas[$dia]['destino'] = $value['destino'];
			$etapas[$dia]['desDestino'] = $value['desDestinoEs'];
			$etapas[$dia]['alojamiento'] =  $value['servicio'];
			$etapas[$dia]['idImagen'] = $value['idServicio'];
			$etapas[$dia]['descripcion'] = $value['desServicioEs'];
		}			
		if($value['ts'] == 2){//Actividad
			$etapas[$dia]['actividad'] = $value['servicio'];
			$etapas[$dia]['idImagen'] = $value['idServicio'];
			$etapas[$dia]['descripcion'] = $value['desServicioEs'];
		}
	}
	$dia++;
}

//print_r($etapas);

foreach ($etapas as $key => $informacion) {
	if($key == 11){
		$pdf->SetY(160);
	}
	if($key >= 11){
		$pdf->SetX(105);
	}
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(25,10,utf8_decode('Día ').$key.':');
	$pdf->SetFont('Arial','',13);
	$pdf->Cell(6,10,$pdf->Image('../assets/img/location.png',$pdf->GetX(),$pdf->GetY()+2));
	$pdf->Cell(60,10,utf8_decode($informacion['destino']),0,1);
}

setlocale(LC_ALL,"es_ES");
$pagina = 0;
foreach ($etapas as $key => $informacion) {
	if($pagina % 2 == 0){
		$pdf->AddPage();
		$pdf->SetY(20);
	}
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(20,7,utf8_decode('Día ').$key.':');
	$pdf->Cell(7,7,$pdf->Image('../assets/img/location.png',$pdf->GetX(),$pdf->GetY()+1));
	$pdf->SetFont('Arial','',12);
	$pdf->Cell(48,7,utf8_decode($informacion['destino']));	
	$date = DateTime::createFromFormat("Y-m-d", $informacion['fecha']);
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(110,7,ucfirst(utf8_decode(strftime("%A, %d de %B de %Y",$date->getTimestamp()))),0,1);
	$y = $pdf->GetY();
	$pdf->Ln();
	if(isset($informacion['idDestino'])){
		$pdf->Image('../assets/img/destinos/'.$informacion['idDestino'].'.jpg',$pdf->GetX(),$pdf->GetY(),70);
	}	
	$pdf->SetX(85);
	$pdf->SetFont('Arial','',12);
	$pdf->MultiCell(110,7,utf8_decode($informacion['desDestino']));
	
	//Se evalua si coloca alojamiento o paquete
	$pdf->Ln(5);
	$pdf->SetX(85);
	$pdf->SetFont('Arial','B',10);
	if($informacion['ts'] == 3){
		$pdf->Cell(7,7,$pdf->Image('../assets/img/hospedaje2.png',$pdf->GetX(),$pdf->GetY()));
		$pdf->Cell(25,7,'Alojamiento:');
	}else{
		$pdf->Cell(7,7,$pdf->Image('../assets/img/paquete.png',$pdf->GetX(),$pdf->GetY()));
		$pdf->Cell(25,7,'Paquete:');
	}	
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(75,7,utf8_decode($informacion['alojamiento']),0,1);

	if($informacion['ts'] == 4){
		$pdf->SetX(85);
		$pdf->MultiCell(110,7,utf8_decode($informacion['descripcion']));
		$pdf->Image('../assets/img/servicios/'.$informacion['idImagen'].'.jpg',10,$y+65,70);
	}
	if(isset($informacion['actividad'])){
		$pdf->Ln(23);
		$pdf->SetX(85);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(30,7,'Actividad:');
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(80,7,utf8_decode($informacion['actividad']),0,1);
		$pdf->Image('../assets/img/servicios/'.$informacion['idImagen'].'.jpg',10,$y+65,70);
	}
	$pdf->Line(10,150,200,150);
	$pdf->SetY(155);
	$pagina++;
}

$pdf->AddPage();
$pdf->SetY(33);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,5,'PRECIO POR PERSONA',0,1,'C');
$pdf->SetFont('Arial','',13);
$pdf->Cell(190,5,number_format(($viaje['data'][0]['precio']/$viaje['data'][0]['pasajeros']),0,',','.').' '.$viaje['data'][0]['moneda'],0,1,'C');
$pdf->Ln(10);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,5,'PRECIO TOTAL DEL VIAJE',0,1,'C');
$pdf->SetFont('Arial','',13);
$pdf->Cell(190,5,number_format($viaje['data'][0]['precio'],0,',','.').' '.$viaje['data'][0]['moneda'],0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(190,5,'*Precio incluye todas las tasas durante este viaje',0,1,'C');

$pdf->Line(10,75,200,75);

$pdf->SetY(75);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,'Condiciones de pago',0,1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(190,5,utf8_decode('Para confirmar la reserva es necesario un abono del 30% y el saldo total 45 días antes del viaje'),0,1);

$pdf->Line(10,95,200,95);

$pdf->SetY(95);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode('Condiciones de cancelación'),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode('Todas las cancelaciones tienen que ser comunicadas por escrito al email info@magicalcolombia.com. Los tiquetes aéreos nacionales tienen una penalidad del 100%. Para los servicios terrestres aplican las siguientes condiciones:'),0);
$pdf->Cell(190,5,utf8_decode('A más de 45 días de la fecha de inicio del viaje: 25%'),0,1);
$pdf->Cell(190,5,utf8_decode('Entre 44 y 30 días de la fecha de inicio del viaje: 50 %'),0,1);
$pdf->Cell(190,5,utf8_decode('Entre 30 y 22 días de la fecha de inicio del viaje: 75 %'),0,1);
$pdf->Cell(190,5,utf8_decode('A menos de 21 días de la fecha de inicio del viaje: 100 %'),0,1);

$pdf->Line(10,145,200,145);

$pdf->SetY(145);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode('Pasaportes y visados'),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode('Verifica los documentos de viaje requeridos: (Dni, pasaporte y visado) documentos para la entrada y estancia en el país de destino con la Embajada o Consulado de ese país en su país de residencia de viaje. Tratándose del pasaporte, algunos países exigen una validez mínima. Los trámites pueden variar segñun el país de destino y la nacionalidad de los viajeros. Verifica, por tanto, todos estos datos antes de tu viaje.'),0);

$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode('Seguros'),0,1);
$pdf->SetFont('Arial','',14);
$pdf->Cell(190,10,utf8_decode('Antes de tu viaje'),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode('Al comprar un viaje, es muy recomendable estar asegurado. De hecho, un seguro de cancelación cubre el riesgo financiero si se ve obligado a cancelar su viaje. En caso de cancelación, las agencias locales no pueden reembolsarte la totalidad de su viaje porque ellos mismos están comprometidos con hoteles, transportistas, guías ... Por lo tanto, aplicarán un programa de reembolso que tiene en cuenta la proximidad de su salida (las condiciones de reembolso en caso de cancelación varían según las agencias locales: las encontrará en el presupuesto o en el formulario de inscripción que te comunicaron).'),0);
$pdf->Ln();

$pdf->SetFont('Arial','',14);
$pdf->Cell(190,10,utf8_decode('Durante tu viaje'),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode('Una asistencia de repatriación te protege en caso de un accidente en su destino: permite tu atención médica y, si es necesario, tu repatriación. Para obtener más información, inicia sesión en tu cuenta de Evaneos, ve a tu viaje, sección Servicios. Puedes suscribir un seguro al pagar el viaje. Si la opción no aparece, no dudes en ponerte en contacto con nuestro socio Chapka.'),0);

$pdf->AddPage();
$paginaFinal = $pdf->PageNo();
$pdf->Image('../assets/img/contraportada.jpg',0,17,210);
$pdf->Image('../assets/img/logo2.png',60,25,80);
$pdf->SetY(200);
$pdf->SetFont('Arial','B',20);
$pdf->Cell(190,7,'MAGICAL COLOMBIA',0,1,'C');
$pdf->SetFont('Arial','',14);
$pdf->Cell(190,7,utf8_decode('OPERADOR TURÍSTICO RECEPTIVO'),0,1,'C');
$pdf->Ln();
$pdf->Cell(190,5,utf8_decode('Magical Colombia S.A.S. NIT: 900902837 - 3 / RNT: 41848'),0,1,'C');
$pdf->Cell(190,5,utf8_decode('Télefono: +57 2 3451220 / E-mail: info@magicalcolombia.com'),0,1,'C');
$pdf->Cell(190,5,utf8_decode('Calle 6 # 76 - 70, Local 21, C.C. Capri'),0,1,'C');
$pdf->Cell(190,5,utf8_decode('Santiago de Cali - Colombia'),0,1,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode('www.magicalcolombia.com'),0,1,'C');

$pdf->Output();
?>