<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Usuarios
            <small>Administración</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Configuración</a></li>
            <li class="active">Usuarios</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" id="t1">Activos</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" id="t2">Cancelados</a>
                        </li>
                        <li class="pull-right">
                            <button id="botonMostrarModalUsuarios" class="btn color-magical btn-sm"><i class="fa fa-plus"></i></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="table-responsive">
                                <table id="tablaContenidoActivos" class="table table-bordered">
                                    <thead>
                                        <tr>                                        
                                            <th class="text-center">Perfil</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Login</th>                                        
                                            <th class="text-center">Ultimo acceso</th>                                            
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoActivos"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <table id="tablaCancelados" class="table table-bordered">
                                    <thead>
                                        <tr>                                        
                                            <th class="text-center">Perfil</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Login</th>                                        
                                            <th class="text-center">Ultimo acceso</th>                                            
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenidoCancelados"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalUsuarios">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalUsuariosTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioUsuarios">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="login" class="control-label">Login</label>
                                <input id="login" name="login" class="form-control" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password" class="control-label">Contraseña</label>
                                <input id="password" name="password" class="form-control" type="password" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="perfil" class="control-label">Perfil</label>                        
                        <select id="perfil" name="perfil" class="form-control" required>
                            <option value="">Seleccione...</option>
                            <option value="Administrador">Administrador</option>                            
                            <option value="Comercial">Comercial</option>                            
                            <option value="Operaciones">Operaciones</option>
                            <option value="Contabilidad">Contabilidad</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="control-label">Estado</label>                        
                        <select id="estado" name="estado" class="form-control" required>                            
                            <option value="Activo">Activo</option>
                            <option value="Cancelado">Cancelado</option>
                        </select>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button id="botonGuardarUsuarios" class="btn color-magical btn-submit" type="submit" form="formularioUsuarios">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button id="botonEditarUsuarios" class="btn btn-default btn-submit" type="submit" form="formularioUsuarios">
                    <i class="fa fa-refresh"></i> Actualizar
                </button>
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalPerfil">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
                    </div>
                    <img class="profile-user-img img-responsive img-circle" id="imagen">            
                    <h3 class="profile-username text-center" id="perfil_nombre"></h3>
                    <p class="text-muted text-center" id="perfil_profesion"></p>
                    <hr>
                    <strong>
                        <i class="fa fa-user margin-r-5"></i> Usuario
                    </strong>
                    <p class="text-muted" id="perfil_login"></p>
                    <hr>
                    <strong>
                        <i class="fa fa-asterisk margin-r-5"></i> Perfil
                    </strong>
                    <p class="text-muted" id="perfil_perfil"></p>
                    <hr>
                    <strong>
                        <i class="fa fa-phone margin-r-5"></i> Telefono
                    </strong>
                    <p class="text-muted" id="perfil_telefono"></p>
                    <hr>
                    <strong>
                        <i class="fa fa-phone margin-r-5"></i> Correo
                    </strong>
                    <p class="text-muted" id="perfil_correo"></p>
                    <hr>
                    <strong>
                        <i class="fa fa-pencil margin-r-5"></i> Sobre mí
                    </strong>
                    <p class="text-muted" id="perfil_sobre_mi"></p>
                </div>
            </div>            
        </div>
    </div>
</div>
<?php require 'footer.php'; ?>   
<script type="text/javascript">
    var id 
    var boton
    var tabla

    function initLogin(user){
        cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
            $('#tablaContenidoActivos').DataTable({
                "lengthMenu": [ 50, 100, 200, 300 ],
                "language":{
                    "decimal":        "",
                    "emptyTable":     "Sin datos para mostrar",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
                    "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "Ningún registro encontrado",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Sig",
                        "previous":   "Ant"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "bDestroy": true
                }
            })
        })

        $('#botonMostrarModalUsuarios').on('click', function(){
            $('#formularioUsuarios')[0].reset()
            $('#modalUsuariosTitulo').text('Crear usuario')
            $('#password').prop('disabled', false)
            $('#botonEditarUsuarios').hide()
            $('#botonGuardarUsuarios').show()
            $('#modalUsuarios').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioUsuarios').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            if(boton == "botonGuardarUsuarios"){
                procesarRegistro('usuarios', 'insert', data, function(r){
                    swal('Perfecto!', 'El usuario se creo correctamente', 'success')
                    cargarRegistros({'id':r.insertId}, 'crear', 'contenidoActivos', function(){
                        $('#modalUsuarios').modal('hide')
                    })
                })    
            }else{                
                data.id = id
                procesarRegistro('usuarios', 'update', data, function(r){                    
                    swal('Perfecto!', 'El usuario se actualizo correctamente', 'success')
                    if(data.estado == 'Cancelado'){
                        $('#'+data.id).remove()
                    }else{
                        cargarRegistros({'id':id}, 'actualizar', 'contenidoActivos', function(){
                            console.log('ok')
                        })
                    }
                    $('#modalUsuarios').modal('hide')
                })
            }
        })

        $('#t1').on('click', function(){
            $('#contenidoActivos').html('')
            $('#botonMostrarModalUsuarios').show()
            cargarRegistros({'estado':'Activo'}, 'crear', 'contenidoActivos', function(){
                console.log('ok')
            })
        })
        $('#t2').on('click', function(){
            $('#contenidoCancelados').html('')
            $('#botonMostrarModalUsuarios').hide()
            cargarRegistros({'estado':'Cancelado'}, 'crear', 'contenidoCancelados', function(){
                console.log('ok')
            })
        })
    }

    function cargarRegistros(data, accion, elemento, callback){
        procesarRegistro('usuarios', 'select', data, function(r){            
            let fila
            let opciones
            for(let i = 0; i < r.data.length; i++){                
                opciones = '<button class="btn btn-default btn-xs" onClick="mostrarModalEditarUsuarios('+r.data[i].id+')" title="Editar usuario"><i class="fa fa-pencil-square-o"></i></button> '
                opciones += '<button class="btn btn-default btn-xs" onClick="cambiarPassword('+r.data[i].id+',\''+r.data[i].login+'\')" title="Cambiar password"><i class="fa fa-key"></i></button> '
                opciones += '<button class="btn btn-default btn-xs" onClick="mostrarDetalle('+r.data[i].id+')" title="Ver perfil"><i class="fa fa-navicon"></i></button>'
                if(r.data[i].estado == 'Cancelado'){
                    opciones = '<button class="btn btn-default btn-xs" onClick="funcionReactivarUsuario('+r.data[i].id+')" title="Reactivar usuario"><i class="fa fa-reply"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'">'+                            
                            '<td>'+r.data[i].perfil+'</td>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].login+'</td>'+                            
                            '<td class="text-center">'+marcarAcceso(r.data[i].ultimo_acceso)+'</td>'+                            
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>'                
            }
            if(accion == 'crear'){
                $('#'+elemento).append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            callback()
        })
    }

    function mostrarModalEditarUsuarios(idUsuario){
        id = idUsuario
        llenarFormulario('formularioUsuarios','usuarios', 'select', {'id':idUsuario}, function(r){
            $('#modalUsuariosTitulo').text('Modificar usuario')
            $('#botonGuardarUsuarios').hide()
            $('#botonEditarUsuarios').show()
            $('#password').prop('disabled', 'disabled')
            $('#modalUsuarios').modal('show')            
        })
    }

    function marcarAcceso(fecha){
        let x = fecha.split(" ")
        if(x[0] == '0000-00-00'){
            return '<span class="label label-default">'+fecha+'</span>'
        }else{
            let diff = moment(moment(x[0])).diff(moment().format('YYYY-MM-DD'), 'days')
            if(diff < 0){
                return '<span class="label label-default">'+fecha+'</span>'
            }else{
                return '<span class="label label-success">'+fecha+'</span>'
            }
        }
    }

    function mostrarDetalle(idUsuario){        
        procesarRegistro('usuarios', 'select', {'id':idUsuario}, function(r){
            $('#imagen').attr('src','assets/img/usuarios/'+r.data[0].id+'.jpg')
            $('#perfil_nombre').text(r.data[0].nombre)
            $('#perfil_profesion').text(r.data[0].profesion)
            $('#perfil_login').text(r.data[0].login)
            $('#perfil_perfil').text(r.data[0].perfil)
            $('#perfil_telefono').text(r.data[0].telefono)
            $('#perfil_correo').text(r.data[0].correo)
            $('#perfil_sobre_mi').text(r.data[0].sobre_mi)
            $('#modalPerfil').modal('show')
        })        
    }

    function cambiarPassword(idUsuario, login){
        swal("Escriba el nuevo password para "+login, {
            content: {
                element: "input",
                attributes: {
                    type: "password"
                }
            },
            buttons: true
        })
        .then((value) => {
            if(value){
                procesarRegistro('usuarios', 'setPassword', {id:idUsuario, password: value}, function(r){
                    swal('Perfecto!', 'Cambio se password correcto', 'success')
                })
            }
        })
    }

    function funcionReactivarUsuario(idUsuario){
        swal('¿Esta seguro de reactivar el usuario?')
        .then((value) => {
            if (value) {
                procesarRegistro('usuarios', 'update', {'id':idUsuario, 'estado':'Activo'}, function(r){                    
                    $('#'+idUsuario).remove()
                })
            }
        })
    }
</script>
</body>
</html>