<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            CxC
            <small>Histórico</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> CxC</a></li>            
            <li class="active">Histórico</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filtros</h3>
                    </div>
                    <div class="box-body">
                        <form id="formulario" class="form-horizontal">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-3 control-label">Buscar cliente:</label>
                                <div class="col-sm-4">
                                    <div class="input-group input-group-sm">
                                        <input class="form-control" type="text" id="valor">
                                        <div class="input-group-btn">
                                            <button id="botonBuscar" class="btn btn-primary" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-3 control-label">Clientes</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="cliente" id="cliente"></select>                                    
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-3 control-label">Cotización</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="number" name="cotizacion" id="cotizacion">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-3 control-label">Reserva</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="number" name="reserva" id="reserva">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-3 control-label">Fecha inicial</label>
                                <div class="col-sm-4">
                                    <input class="form-control calendario" type="text" name="fechai" id="fechai">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-3 control-label">Fecha final</label>
                                <div class="col-sm-4">
                                    <input class="form-control calendario" type="text" name="fechaf" id="fechaf">
                                </div>
                            </div>
                        </form>                     
                    </div>
                    <div class="box-footer text-center">
                        <button class="btn btn-primary" type="submit" form="formulario">
                            <i class="fa fa-search"></i> Buscar
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de cuentas</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered table-condensed">
                                <thead>
                                    <tr>                                        
                                        <th class="text-center">Cotización</th>
                                        <th class="text-center">Reserva</th>
                                        <th class="text-center">Cliente</th>                                        
                                        <th class="text-center">Valor</th>                                        
                                        <th class="text-center">Saldo</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalHistoricoPagos">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span id="modalHistoricoPagosTitulo"></span>
                    <small> Historico de pagos</small>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Valor</th>                                
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Comprobante</th>
                        </tr>
                    </thead>
                    <tbody id="contenidoHistoricoPagos"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    function initLogin(user){
        $('.calendario').datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        })

        //Buscar clientes
        $('#botonBuscar').on('click',function(){            
            procesarRegistro('clientes','getLike',{valor: $('#valor').val()},function(r){                
                $('#cliente').empty();
                $('#cliente').append("<option value=''>Seleccione</option>");
                for(let i = 0; i < r.data.length; i++){
                    $('#cliente').append('<option value='+r.data[i].id+'>'+r.data[i].nombre+'</option>')
                }
            })
        })

        $('#formulario').on('submit',function(e){
            e.preventDefault()
            $('#contenido').empty()
            let data = parsearFormulario($(this))
            cargarRegistros(data)
        })
    }

    function cargarRegistros(data){
        procesarRegistro('cxc', 'getHistorico', data, function(r){
            let fila            
            let color = {
                Pendiente: 'text-red',
                Finalizado: 'text-green'
            }
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr id="'+r.data[i].id+'">'+                            
                            '<td class="text-center">C-'+r.data[i].viaje+'</td>'+
                            '<td class="text-center">R-'+r.data[i].codigo_reserva+'</td>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].saldo,0)+'</td>'+
                            '<td class="text-center '+color[r.data[i].estado]+'">'+r.data[i].estado+'</td>'+
                            '<td class="text-center">'+
                                '<button class="btn btn-default btn-xs" title="Historico pagos" onClick="verHistorico('+r.data[i].id+','+r.data[i].viaje+')"><i class="fa fa-history"></i></button>'+
                            '</td>'+
                        '</tr>'
            }
            $('#contenido').append(fila)
        })
    }

    function verHistorico(idcxc, idViaje){
        $('#modalHistoricoPagosTitulo').text('C-'+idViaje)
        $('#contenidoHistoricoPagos').empty()
        cargarRegistrosPagos({'fk_cxc':idcxc})
        $('#modalHistoricoPagos').modal('show')
    }

    function cargarRegistrosPagos(data){
        procesarRegistro('cxcPagos', 'select', data, function(r){
            let fila = ''            
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr>'+                            
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+                            
                            '<td class="text-center">'+r.data[i].fecha_creacion+'</td>'+
                            '<td class="text-center">'+                                
                                '<a class="btn btn-default btn-xs" href="assets/img/comprobantes_cxc/'+r.data[i].id+'.jpg" target="_blank"><i class="fa fa-image"></i></a>'+
                            '</td>'+
                        '</tr>'
            }            
            $('#contenidoHistoricoPagos').append(fila)
        })
    }
</script>
</body>
</html>