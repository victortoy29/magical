<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            CxC
        </h1>
        <ol class="breadcrumb">
            <li><a href="intranet/index.php"><i class="fa fa-dashboard"></i> CxC</a></li>
            <li> Pendientes</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Cotización</th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Cliente</th>
                                        <th class="text-center">Moneda</th>
                                        <th class="text-center">Valor</th>
                                        <th class="text-center">Saldo</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalPagos">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>
                <h3 class="modal-title" id="modalPagosTitulo"></h3>
            </div> 
            <div class="modal-body"> 
                <form id="formularioPagos" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="valor" class="control-label">Valor</label>
                                <input id="valor" name="valor" class="form-control numero" type="text" required="required">
                            </div>    
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label for="tasa" class="control-label">Tasa</label>
                                <input id="tasa" name="tasa" class="form-control numero" type="text" required="required">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="factor" class="control-label">Factor de conversión</label>
                                <input id="factor" name="factor" class="form-control" type="text" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="archivo" class="control-label">Comprobante de ingreso</label>
                        <input  type="file" id="archivo" name="archivo" required="required">
                    </div>
                </form>                     
            </div> 
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" form="formularioPagos">
                    <i class="fa fa-save"></i> Crear
                </button>
            </div>            
        </div>             
    </div>         
</div>

<div class="modal fade" id="modalHistoricoPagos">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span id="modalHistoricoPagosTitulo"></span>
                    <small> Historico de pagos</small>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Valor</th>                                
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Comprobante</th>
                        </tr>
                    </thead>
                    <tbody id="contenidoHistoricoPagos"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalClientes">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span>Información cliente</span>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody id="contenidoClientes"></tbody>
                </table>                
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id
    var idV
    var maximo

    function initLogin(user){
        $('.numero').number( true, 0,',','.')

        cargarRegistros({estado:'Pendiente'}, 'crear')

        $('#formularioPagos').on('submit',function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            if(data.valor <= maximo){
                let archivo = $('#archivo')[0].files[0]
                let fd = new FormData()
                fd.append('objeto', 'cxcPagos')
                fd.append('metodo','acentarPago')
                fd.append('file',archivo)
                fd.append('datos[cxc]',id)
                fd.append('datos[viaje]',idV)
                fd.append('datos[valor]',data.valor)
                fd.append('datos[tasa]',data.tasa)
                fd.append('datos[factor]',data.factor)                
                $.ajax({
                    url: 'class/frontController.php',
                    type: 'post',
                    dataType: 'json',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(r){
                        if(r.ejecuto == true){                        
                            $('#formularioPagos')[0].reset()
                            cargarRegistros({id:id}, 'actualizar')
                            $('#modalPagos').modal('hide')
                        }    
                    }
                })
            }else{
                swal('Ups!', 'No se puede crear el pago, supero el saldo', 'error')
            }
        })
    }

    function cargarRegistros(data, accion){
        procesarRegistro('cxc', 'getCuentas', data, function(r){
            let fila
            let opciones
            let color = {
                Pendiente: 'text-red',
                Finalizado: 'text-green'
            }
            for(let i = 0; i < r.data.length; i++){
                opciones = ''
                if(r.data[i].estado == 'Pendiente'){
                    opciones = '<button class="btn btn-default btn-xs" title="Cargar pago" onClick="cargarPago('+r.data[i].id+','+r.data[i].viaje+','+r.data[i].saldo+',\''+r.data[i].nombre_viaje+'\')"><i class="fa fa-plus"></i></button> '
                }
                fila += '<tr id="'+r.data[i].id+'">'+                            
                            '<td class="text-center">C-'+r.data[i].viaje+'</td>'+
                            '<td class="text-center">'+r.data[i].nombre_viaje+'</td>'+                            
                            '<td><button type="button" class="btn btn-link" onClick="infoClientes('+r.data[i].idc+')">'+r.data[i].nombre+'</button></td>'+
                            '<td class="text-center">'+r.data[i].moneda+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+
                            '<td class="text-right">$'+currency(r.data[i].saldo,0)+'</td>'+
                            '<td class="text-center '+color[r.data[i].estado]+'">'+r.data[i].estado+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                                '<button class="btn btn-default btn-xs" title="Historico pagos" onClick="verHistorico('+r.data[i].id+','+r.data[i].viaje+')"><i class="fa fa-history"></i></button>'+
                            '</td>'+
                        '</tr>'
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
        })
    }

    function infoClientes(idCliente){
        $('#contenidoClientes').empty()        
        procesarRegistro('clientes', 'select', {id:idCliente}, function(r){
            $('#contenidoClientes').append(
                '<tr><td>Nombre:</td><td>'+r.data[0].nombre+'</td></tr>'+
                '<tr><td>Tipo documento:</td><td>'+r.data[0].tipo_doc+'</td></tr>'+
                '<tr><td>Número:</td><td>'+r.data[0].numero+'</td></tr>'+
                '<tr><td>Telefono:</td><td>'+r.data[0].telefono+'</td></tr>'+
                '<tr><td>Correo:</td><td>'+r.data[0].correo+'</td></tr>'
            )
        })
        $('#modalClientes').modal('show')
    }

    /*Logica de creación de pagos*/
    function cargarPago(idcxc, idViaje, max, nom){
        id = idcxc
        idV = idViaje
        maximo = max
        $('#modalPagosTitulo').html('<span>C-'+idcxc+'</span><small> '+nom+'</small>')
        $('#formularioPagos')[0].reset()
        $('#modalPagos').modal('show')
    }

    function verHistorico(idcxc, idViaje){
        $('#modalHistoricoPagosTitulo').text('C-'+idViaje)
        $('#contenidoHistoricoPagos').empty()
        cargarRegistrosPagos({'fk_cxc':idcxc})
        $('#modalHistoricoPagos').modal('show')
    }

    function cargarRegistrosPagos(data){
        procesarRegistro('cxcPagos', 'select', data, function(r){
            let fila = ''            
            for(let i = 0; i < r.data.length; i++){                
                fila += '<tr>'+                            
                            '<td class="text-right">$'+currency(r.data[i].valor,0)+'</td>'+                            
                            '<td class="text-center">'+r.data[i].fecha_creacion+'</td>'+
                            '<td class="text-center">'+                                
                                '<a class="btn btn-default btn-xs" href="assets/img/comprobantes_cxc/'+r.data[i].id+'.jpg" target="_blank"><i class="fa fa-image"></i></a>'+
                            '</td>'+
                        '</tr>'
            }            
            $('#contenidoHistoricoPagos').append(fila)
        })
    }
</script>
</body>
</html>