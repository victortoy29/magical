<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Reservas
            <small>Gestión</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Comercial</a></li>
            <li class="active">Reservas</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>                                        
                                        <th class="text-center">Código</th>
                                        <th class="text-center">Nombre reserva</th>
                                        <th class="text-center">Cliente</th>
                                        <th class="text-center">Pasajeros</th>
                                        <th class="text-center">Fecha inicio</th>
                                        <th class="text-center">Fecha fin</th>
                                        <th class="text-center">Servicios</th>
                                        <th class="text-center">Precio</th>
                                        <th class="text-center">Moneda</th>
                                        <th class="text-center">Vendedor</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalProcesar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalProcesarTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioProcesar">
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-submit btn-success" type="submit" form="formularioProcesar">
                    <i class="fa fa-save"></i> Guardar
                </button>                
            </div>
        </div>            
    </div>        
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id
    var idP

    function initLogin(user){        
        cargarRegistros({campo:'estado', valor:'Reserva'})
    }

    function cargarRegistros(data, accion, elemento){
        procesarRegistro('viajes', 'getViajes', data, function(r){
            let fila            
            let opciones
            for(let i = 0; i < r.data.length; i++){
                if(r.data[i].diasRestantes > 0){
                    opciones = '<a class="btn btn-default btn-xs" title="Revisar itinerario" href="views/comercialReservasDetalle.php?idV='+r.data[i].id+'"><i class="fa fa-shopping-cart"></i></a> '
                    opciones += '<a class="btn btn-default btn-xs" title="Identificar viajeros" href="views/comercialReservasPasajeros.php?idV='+r.data[i].id+'&s=c"><i class="fa fa-users"></i></a>'
                }else{
                    opciones = '<button class="btn btn-success btn-xs" title="Terminar reserva" onClick="procesar('+r.data[i].id+',\''+r.data[i].nombre+'\')"><i class="fa fa-sign-in"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td class="text-center">R-'+r.data[i].cr+'</td>'+
                            '<td class="text-center">'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].cliente+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_fin+'</td>'+
                            '<td class="text-center">'+
                                '<div class="progress">'+
                                    '<div id=indicador_'+r.data[i].id+' class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="text-muted">0/0</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-right">'+currency(r.data[i].precio,0)+'</td>'+
                            '<td class="text-center">'+r.data[i].moneda+'</td>'+
                            '<td class="text-center">'+r.data[i].vendedor+'</td>'+
                            '<td class="text-center">'+
                                opciones+        
                            '</td>'+
                        '</tr>' 
            }            
            $('#contenido').append(fila)            
            cargarIndicador()
        })
        
        //Esta es la parte del cierre de la reserva
        $('#formularioProcesar').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.id = id
            data.estado = 'Terminada'
            procesarRegistro('viajes', 'update', data, function(r){
                $('#'+id).hide('slow')
                $('#modalProcesar').modal('hide')
            })
        })        
    }    

    function cargarIndicador(){
        procesarRegistro('viajesDetalle', 'getIndicador', {estado: 'Activo'}, function(r){
            if(r.data.length != 0){
                let centinela = r.data[0].fk_viajes
                let numerador = 0
                let denominador = 0
                let indicador_texto
                let indicador_grafico
                for(let i = 0; i < r.data.length; i++){
                    if(centinela == r.data[i].fk_viajes){
                        if(r.data[i].estado == 'Reservado'){
                            numerador += r.data[i].cantidad
                            denominador += r.data[i].cantidad
                        }else{
                            denominador += r.data[i].cantidad
                        }                    
                    }
                    if(centinela != r.data[i].fk_viajes){
                        porcentaje = (numerador / denominador)*100
                        if(porcentaje == 0)
                            $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html('<span class="text-muted">'+(numerador+'/'+denominador)+'</span>')
                        else
                            $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html(numerador+'/'+denominador)
                        centinela = r.data[i].fk_viajes
                        numerador = 0
                        denominador = 0
                        i--
                    }
                }
                porcentaje = (numerador / denominador)*100
                if(porcentaje == 0)
                    $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html('<span class="text-muted">'+(numerador+'/'+denominador)+'</span>')
                else
                    $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html(numerador+'/'+denominador)
            }
        })
    }

    function procesar(idViaje, nombre){
        id = idViaje
        $('#modalProcesarTitulo').text('Cerrar reserva '+nombre)
        $('#formularioProcesar')[0].reset()
        $('#modalProcesar').modal('show')
    }
</script>
</body>
</html>