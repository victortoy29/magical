<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Reservas
            <small>Gestión</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Operaciones</a></li>
            <li class="active">Reservas</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tablaContenido" class="table table-bordered">
                                <thead>
                                    <tr>                                        
                                        <th class="text-center">Código</th>
                                        <th class="text-center">Nombre reserva</th>
                                        <th class="text-center">Cliente</th>
                                        <th class="text-center">Pasajeros</th>
                                        <th class="text-center">Fecha inicio</th>
                                        <th class="text-center">Fecha fin</th>
                                        <th class="text-center">Servicios</th>
                                        <th class="text-center">Proximo servicio</th>
                                        <th class="text-center">Vendedor</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var id 
    var boton

    function initLogin(user){
        cargarRegistros({campo:'estado', valor:'Reserva'}, 'crear')        
    }

    function cargarRegistros(data, accion, elemento){
        procesarRegistro('viajes', 'getViajes', data, function(r){
            let fila            
            let opciones
            let i
            for(i = 0; i < r.data.length; i++){
                fila += '<tr id="'+r.data[i].id+'">'+
                            '<td class="text-center">R-'+r.data[i].cr+'</td>'+
                            '<td class="text-center">'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].cliente+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center">'+r.data[i].fecha_fin+'</td>'+
                            '<td class="text-center">'+
                                '<div class="progress">'+
                                    '<div id=indicador_'+r.data[i].id+' class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="text-muted">0/0</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-center" id="proximo_'+r.data[i].id+'"></td>'+
                            '<td class="text-center">'+r.data[i].vendedor+'</td>'+
                            '<td class="text-center">'+
                                '<a id="reservar_'+r.data[i].id+'" class="btn color-magical btn-xs" href="views/operacionesReservasDetalle.php?idV='+r.data[i].id+'" title="Reservar servicios"><i class="fa fa-gear"></i></a> '+
                                '<a id="checkin_'+r.data[i].id+'" class="btn btn-default btn-xs ocultar" href="views/operacionesReservasCheckin.php?idV='+r.data[i].id+'" title="Checkin"><i class="fa fa-check"></i></a> '+
                                '<a class="btn btn-default btn-xs" title="Identificar viajeros" href="views/comercialReservasPasajeros.php?idV='+r.data[i].id+'&s=o"><i class="fa fa-users"></i></a> '+
                                '<a class="btn btn-default btn-xs" title="Checkin de documentos" href="views/operacionesReservasDocumentos.php?idV='+r.data[i].id+'"><i class="fa fa-file-o"></i></a>'+
                            '</td>'+
                        '</tr>'
                //Traer servicios proximo a vencerse
                getProximoServicio(r.data[i].id)
            }            
            $('#contenido').append(fila)

            //Se ocultan los botones y los que esten al 100% se muestran en la siguiente función
            if(i != 0){
                $('.ocultar').hide()
                cargarIndicador()                
            }
        })
    }    

    function cargarIndicador(){
        procesarRegistro('viajesDetalle', 'getIndicador', {estado: 'Activo'}, function(r){
            if(r.data.length != 0){
                let centinela = r.data[0].fk_viajes
                let numerador = 0
                let denominador = 0
                let indicador_texto
                let indicador_grafico
                for(let i = 0; i < r.data.length; i++){
                    if(centinela == r.data[i].fk_viajes){
                        if(r.data[i].estado == 'Reservado' || r.data[i].estado == 'Devuelto'){
                            numerador += r.data[i].cantidad
                            denominador += r.data[i].cantidad
                        }else{
                            denominador += r.data[i].cantidad
                        }
                    }
                    if(centinela != r.data[i].fk_viajes){
                        porcentaje = (numerador / denominador)*100
                        if(porcentaje == 0)
                            $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html('<span class="text-muted">'+(numerador+'/'+denominador)+'</span>')
                        else
                            $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html(numerador+'/'+denominador)
                        if(porcentaje == 100){
                            $('#reservar_'+centinela).hide()
                            $('#checkin_'+centinela).show()
                        }                    
                        centinela = r.data[i].fk_viajes
                        numerador = 0
                        denominador = 0
                        i--
                    }
                }
                porcentaje = (numerador / denominador)*100
                $('#indicador_'+centinela).css('width', porcentaje+'%').attr('aria-valuenow', porcentaje).html('<span class="text-muted">'+(numerador+'/'+denominador)+'</span>')
                if(porcentaje == 100){
                    $('#reservar_'+centinela).hide()
                    $('#checkin_'+centinela).show()
                }
            }
        })
    }

    function getProximoServicio(idv){
        procesarRegistro('viajesDetalle', 'getProximoServicio', {id:idv}, function(r){            
            if(r.data.length == 1){                
                if(r.data[0].tipo == 6){
                    $('#proximo_'+idv).html(alertaPorFecha(r.data[0].fecha_inicio)+' <i class="fa fa-plane"></i>')
                }else{
                    $('#proximo_'+idv).html(alertaPorFecha(r.data[0].fecha_inicio))
                }
            }
        })
    }
</script>
</body>
</html>