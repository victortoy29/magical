<?php require 'header.php'; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <span id="nombreViaje"></span>
            <small>
                <span class="label bg-black" id="tituloReserva"></span>
                <span class="label bg-black"><i class="fa fa-users"></i> X <span id="tituloPasajeros"></span></span>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="views/index.php"><i class="fa fa-dashboard"></i> Operaciones</a></li>
            <li><a href="views/operacionesReservas.php"> Reservas</a></li>
            <li class="active"> Detalle</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Servicios</h3>                        
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Fecha</th>
                                        <th class="text-center">Hora</th>
                                        <th class="text-center">Destino</th>
                                        <th class="text-center">Proveedor</th>
                                        <th class="text-center">Servicio</th>
                                        <th class="text-center">Opción</th>
                                        <th class="text-center">Detalle</th>
                                        <th class="text-center">Costo</th>
                                        <th class="text-center">Pasajeros</th>
                                        <th class="text-center">Cantidad</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Observaciones</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody id="contenido"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="timeline" id="contenidoItinerario"></ul>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalProveedores">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <span>Información proveedor</span>
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody id="contenidoProveedores"></tbody>
                </table>
                <div class="text-center">
                    <button id="mostrarContactos" class="btn color-magical" type="button">
                        <i class="fa fa-phone"></i>
                    </button>
                </div>
                <br>
                <div id="contenidoContactos"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalProcesar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 id="modalProcesarTitulo" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="formularioProcesar">
                    <div class="form-group">
                        <label for="observaciones" class="control-label">Observación</label>
                        <textarea id="observaciones" name="observaciones" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-submit" type="submit" form="formularioProcesar" id="boton">
                    <i class="fa fa-save"></i> Guardar
                </button>                
            </div>
        </div>            
    </div>        
</div>

<div class="modal fade" id="modalHora">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Modificar hora</h4>
            </div>
            <div class="modal-body">
                <form id="formularioHora">
                    <div class="form-group">
                        <label>Hora:</label>
                        <div class="input-group">
                            <div class='input-group date hora_inicio'>
                                <input id="hora_inicio" name="hora_inicio" class="form-control" type='text'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-submit" type="submit" form="formularioHora">
                    <i class="fa fa-save"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php'; ?>
<script type="text/javascript">
    var idViaje = <?=$_REQUEST['idV']?>;
    var id
    var estado
    var idPro
    var costo

    function initLogin(user){
        $('.hora_inicio').datetimepicker({
            format: 'HH:mm'
        })
        //consultar el viaje para obtener nombre y fechas para armar los días
        procesarRegistro('viajes', 'select', {id: idViaje}, function(r){
            //Coloco el nombre de la reserva en el titulo
            $('#nombreViaje').text(r.data[0].nombre)
            $('#tituloPasajeros').text(r.data[0].pasajeros)
            $('#tituloReserva').text('R-'+r.data[0].codigo_reserva)
        })

        cargarRegistros({viaje: idViaje, estado: 'Activo'}, 'crear')

        $('#formularioProcesar').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))
            data.id = id
            data.estado = estado
            procesarRegistro('viajesDetalle', 'update', data, function(r){
                cargarRegistros({id: id}, 'actualizar')
                $('#modalProcesar').modal('hide')
                //Si la reserva fue efectiva se crea la cuenta de cobro o se le suma en nuevo valor
                if(estado == 'Reservado'){
                    procesarRegistro('cxp', 'crearCuenta', {viaje: idViaje, proveedor: idPro, valor: costo}, function(r){
                        console.log('ok cuenta')
                    })
                }
            })
        })

        $('#formularioHora').on('submit', function(e){
            e.preventDefault()
            let data = parsearFormulario($(this))            
            data.id = id            
            procesarRegistro('viajesDetalle', 'update', data, function(r){                
                cargarRegistros({id: id}, 'actualizar')
                $('#modalHora').modal('hide')
            })            
        })

        //Mostrar contactos de proveedor
        $('#mostrarContactos').on('click', function(){
            let fila = ''
            procesarRegistro('proveedoresContactos', 'select', {fk_proveedores: idPro}, function(r){
                fila = '<table class="table table-bordered"><tr><th>Nombre</th><th>Telefono</th><th>Correo</th><th>Observación</th></tr>'
                for(let i = 0; i < r.data.length; i++){
                    fila += '<tr>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+r.data[i].telefono+'</td>'+
                            '<td>'+r.data[i].correo+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+
                            '</tr>'
                }
                fila += '</table>'
                $('#contenidoContactos').append(fila)
            })
        })
    }    

    function cargarRegistros(data, accion){
        procesarRegistro('viajesDetalle', 'getDetalle', data, function(r){
            let fila
            let total
            let fondo = {
                Gestionar: 'bg-gray',
                Bloqueado: 'bg-amarillo',
                Reservado: 'bg-green',
                Devuelto: 'bg-red'
            }
            let opciones = ''
            for(let i = 0; i < r.data.length; i++){
                total = r.data[i].pasajeros * r.data[i].costo * r.data[i].cantidad
                opciones = ''
                if(r.data[i].estado == 'Bloqueado'){
                    opciones += '<button class="btn btn-success btn-xs" onClick="procesar('+r.data[i].id+',\'Reservado\',\''+r.data[i].servicio+'\','+r.data[i].idp+','+total+')"><i class="fa fa-check"></i></button> '
                    opciones += '<button class="btn btn-danger btn-xs" onClick="procesar('+r.data[i].id+',\'Devuelto\',\''+r.data[i].servicio+'\','+r.data[i].idp+','+total+')"><i class="fa fa-close"></i></button>'
                }else if(r.data[i].estado == 'Reservado'){
                    //opciones += '<button class="btn btn-danger btn-xs" onClick="procesar('+r.data[i].id+',\'Devuelto\',\''+r.data[i].servicio+'\')"><i class="fa fa-close"></i></button>'
                }else{
                    opciones += '<button class="btn btn-success btn-xs" onClick="procesar('+r.data[i].id+',\'Reservado\',\''+r.data[i].servicio+'\','+r.data[i].idp+','+total+')"><i class="fa fa-check"></i></button>'
                }
                fila += '<tr id="'+r.data[i].id+'" class="'+fondo[r.data[i].estado]+'">'+
                            '<td class="text-center">'+r.data[i].fecha_inicio+'</td>'+
                            '<td class="text-center"><button class="btn btn-default" onClick="cambiarHora('+r.data[i].id+',\''+r.data[i].hora_inicio+'\')">'+r.data[i].hora_inicio+'</button></td>'+
                            '<td class="text-center">'+r.data[i].destino+'</td>'+
                            '<td class="text-center"><button type="button" class="btn btn-link" onClick="infoProveedores('+r.data[i].idp+')">'+r.data[i].proveedor+'</button></td>'+
                            '<td>'+r.data[i].servicio+'</td>'+
                            '<td>'+r.data[i].opcion+'</td>'+
                            '<td>'+r.data[i].detalle+'</td>'+                            
                            '<td class="text-center">$'+currency(r.data[i].costo,0)+'</td>'+
                            '<td class="text-center">'+r.data[i].pasajeros+'</td>'+
                            '<td class="text-center">'+r.data[i].cantidad+'</td>'+
                            '<td class="text-center">$'+currency(total,0)+'</td>'+
                            '<td>'+r.data[i].observaciones+'</td>'+
                            '<td class="text-center">'+
                                opciones+
                            '</td>'+
                        '</tr>'                
            }
            if(accion == 'crear'){
                $('#contenido').append(fila)
            }else{
                $('#'+r.data[0].id).replaceWith(fila)
            }
            $('#contenidoItinerario').empty()
            construirItinerario(idViaje, function(){
                console.log('Ok Itinerario')
            })
        })
    }

    function procesar(idDetalle, est, servicio, idProveedor, pcosto){
        id = idDetalle
        estado = est
        idPro = idProveedor
        costo = pcosto
        if(est == 'Reservado'){
            $('#modalProcesarTitulo').html('<p class="text-green">Reservado servicio: '+servicio+'</p>')
            $('#boton').removeClass('btn-danger').addClass('btn-success')
        }else{
            $('#modalProcesarTitulo').html('<p class="text-red">Devuelto servicio: '+servicio+'</p>')
            $('#boton').removeClass('btn-success').addClass('btn-danger')
        }
        $('#formularioProcesar')[0].reset()        
        $('#modalProcesar').modal('show')
    }

    function infoProveedores(idProveedor){
        idPro = idProveedor
        $('#contenidoProveedores').empty()
        $('#contenidoContactos').empty()
        procesarRegistro('proveedores', 'select', {id:idProveedor}, function(r){
            $('#contenidoProveedores').append(
                '<tr><td>Nombre:</td><td>'+r.data[0].nombre+'</td></tr>'+
                '<tr><td>Tipo documento:</td><td>'+r.data[0].tipo_doc+'</td></tr>'+
                '<tr><td>Número:</td><td>'+r.data[0].numero+'</td></tr>'+
                '<tr><td>Telefono:</td><td>'+r.data[0].telefono+'</td></tr>'+
                '<tr><td>Correo:</td><td>'+r.data[0].correo+'</td></tr>'
            )
        })
        $('#modalProveedores').modal('show')
    }

    function cambiarHora(idDetalle, hora){
        id = idDetalle
        $('#hora_inicio').val(hora)
        $('#modalHora').modal('show')   
    }
</script>
</body>
</html>