<?php
require_once 'database.php';
require_once 'model.php';

class proveedores extends model{
	protected $tabla = 'proveedores';

	public function getProveedoresPorFiltro($datos){
		$filtro = '1';
		if($datos['destino']==''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_destinos = $datos[destino]";
		}

		if($datos['tipo_servicio']==''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_tipos_servicio = $datos[tipo_servicio]";
		}		
		$sql = "SELECT
					proveedores.id,
					proveedores.nombre
				FROM 
					(proveedores INNER JOIN proveedores_oferta ON proveedores.id = proveedores_oferta.fk_proveedores)
					INNER JOIN proveedores_destinos ON proveedores.id = proveedores_destinos.fk_proveedores
				WHERE
					$filtro
				ORDER BY 
					proveedores.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}
}