<?php
require_once 'database.php';
require_once 'model.php';

class viajesDetallePax extends model{
	protected $tabla = 'viajes_detalle_pax';

	public function delete($datos){
		$sql = "DELETE FROM viajes_detalle_pax WHERE id = $datos[id]";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getTotalesByPax($datos){
		$sql = "SELECT
					pasajero,
					SUM(costo * cantidad) AS costo
				FROM
					viajes_detalle INNER JOIN viajes_detalle_pax ON viajes_detalle.id = fk_viajes_detalle
				WHERE
					fk_viajes = $datos[id]
					AND viajes_detalle.estado != 'Cancelado'
				GROUP BY 
					pasajero";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getPaxFaltantes($datos){
		$sql = "SELECT 
					viajes_detalle.id,
					viajes_detalle.pasajeros,
					COUNT(1) AS cantidad
				FROM 
					viajes_detalle INNER JOIN viajes_detalle_pax ON viajes_detalle.id = fk_viajes_detalle
				WHERE
					fk_viajes = $datos[id]
					AND viajes_detalle.estado IN ('Gestionar', 'Bloqueado', 'Reservado', 'Devuelto')
				GROUP BY 
					fk_viajes_detalle";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function agregarTodos($datos){
		$respuesta = [];
		$info;
		for ($i = 1; $i <= $datos['cantidad']; $i++) {
			$info = [				
				'fk_viajes_detalle' => $datos['detalle'],
				'pasajero' => 'Pasajero '.$i
			];
			$respuesta = parent::insert($info);
		}
		return $respuesta;
	}
}