<?php
require_once 'database.php';
require_once 'model.php';

class servicios extends model{
	protected $tabla = 'servicios';
	
	public function getServicios($datos){
		$sql = "SELECT 
					servicios.id,
					destinos.nombre AS destino,
					tipos_servicio.icono,
					tipos_servicio.id AS idts,
					tipos_servicio.nombre AS ts,					
					servicios.nombre AS servicio,
					servicios.web,
					servicios.dias_en_itinerario AS dias,
					servicios.descripciones,
					servicios.estado
				FROM
					(servicios
					INNER JOIN tipos_servicio ON servicios.fk_tipos_servicio = tipos_servicio.id) 
					INNER JOIN destinos ON fk_destinos = destinos.id
				WHERE 1";
				foreach ($datos as $key => $value) {
					$sql .= " AND servicios.$key = '$value' ";
				}
				$sql .= "AND servicios.id != 1
				ORDER BY servicios.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getServiciosPorFiltro($datos){
		$filtro = '1';
		if($datos['destino'] == ''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_destinos = $datos[destino]";
		}

		if($datos['tipo_servicio'] == ''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_tipos_servicio = $datos[tipo_servicio]";
		}

		if($datos['proveedor'] == ''){
			$filtro .= ' AND 1';
		}else{
			$filtro .= " AND fk_proveedores = $datos[proveedor]";
		}
		$sql = "SELECT 
					servicios.id,
					servicios.nombre
				FROM 
					servicios
				WHERE 
					$filtro
					AND servicios.id != 1
				ORDER BY servicios.nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getUnidad($datos){
		$sql = "SELECT 
					um
				FROM
					servicios INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id
				WHERE
					servicios.id = $datos[id]";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function guardarDescripciones($datos){		
		$datos = [
			'id' => $datos['id'],
			'descripciones' => 	json_encode($datos['descripciones'], JSON_UNESCAPED_UNICODE)
		];		
		return parent::update($datos);		
	}

	public function getDescripciones($datos){
		$respuesta = parent::select($datos);
		$respuesta['data'][0]['descripciones'] = str_replace("\r\n", "\\n", $respuesta['data'][0]['descripciones']);
		return $respuesta;
	}
}