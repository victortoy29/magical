<?php
require_once 'database.php';
require_once 'model.php';
require_once 'viajes.php';
require_once 'cxc.php';

class cxcPagos extends model{
	protected $tabla = 'cxc_pagos';

	public function acentarPago($datos){		
		//Primero verifico si cotizacion todavia no es reserva confirmada
		$viaje = new viajes();
		$resultado = $viaje->select(['id'=>$datos['viaje']]);		
		if($resultado['data'][0]['estado'] == 'Cotización'){
			$respuesta = $viaje->crearReserva(['id'=>$datos['viaje']]);
		}
		//Descuento de la cuenta principal
		$cxc = new cxc();
		$resultado = $cxc->select(['id'=>$datos['cxc']]);
		if($resultado['data'][0]['saldo'] != 0){
			$saldo = $resultado['data'][0]['saldo']-$datos['valor'];
			if($saldo == 0){
				$info = [
					'id'=> $datos['cxc'],
					'saldo'=>$saldo,
					'estado'=>'Finalizado'
				];
			}else{
				$info = [
					'id'=> $datos['cxc'],
					'saldo'=>$saldo
				];
			}			
			$respuesta = $cxc->update($info);
		}

		//Luego creo el pago
		$info = [
			'fk_cxc'=>$datos['cxc'],
			'valor' => $datos['valor'],
			'tasa' => $datos['tasa'],
			'factor' => $datos['factor']
		];
		$resultado = parent::insert($info);
		if($resultado['ejecuto']){
			//Guardo la imagen
			if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/comprobantes_cxc/'.$resultado['insertId'].'.jpg')){
				return $resultado;
			}
		}
	}
}