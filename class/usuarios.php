<?php
require_once 'database.php';
require_once 'model.php';

class usuarios extends model{
	protected $tabla = 'usuarios';

	public function insert($datos){		
		$datos['password'] = md5($datos['password']);
		return parent::insert($datos);
	}

	public function update($datos){
		$_SESSION['usuario']['nombre'] = $datos['nombre'];
		return parent::update($datos);
	}

	public function loginService($datos){
		$datos['password'] = md5($datos['password']);
		$registro = $this->select($datos);
		if (count($registro['data']) == 0) {
			return [
				'ejecuto' => false,
				'msgError' => 'Credenciales erróneas'
			];
		}elseif($registro['data'][0]['estado'] == 'Cancelado'){
			return [
				'ejecuto' => false,
				'msgError' => 'Su usuario ha sido cancelado'
			];
		}else{
			$usuario = [
				'id' => $registro['data'][0]['id'],
				'nombre' => $registro['data'][0]['nombre'],
				'login' => $registro['data'][0]['login'],
				'perfil' => $registro['data'][0]['perfil']
			];
			$_SESSION['usuario'] = $usuario;

			//registrar fecha de utimo acceso
			$acceso = [
				'id' => $registro['data'][0]['id'],
				'ultimo_acceso' => date("Y-m-d H:i:s"),
				'nombre' => $usuario['nombre']
			];
			$this->update($acceso);

			return [
				'ejecuto' => true,
				'msgError' => 'Ha iniciado sesión correctamente'
			];
		}
	}

	public function setPassword($datos){
		$datos['password'] = md5($datos['password']);
		return parent::update($datos);
	}

	public function subirImagen($datos){
		$filename = $_FILES['file']['name'];		
		$location = "../assets/img/usuarios/".$filename;		
		$imageFileType = pathinfo($location,PATHINFO_EXTENSION);
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
 			return [
				'ejecuto' => false,
				'msgError' => 'Tipo de archivo no permitido'
			];
		}		
		if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/usuarios/'.$datos.'.jpg')){
			return [
				'ejecuto' => true,
				'msg' => 'Subir correctamente'
			];
		}
	}
}