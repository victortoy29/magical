<?php
require_once 'database.php';
require_once 'model.php';

class clientes extends model{
	protected $tabla = 'clientes';

	public function getClientes($datos){
		$sql = "SELECT 
					clientes.*,
					canales.nombre AS canal,
					paises.nombre AS pais
				FROM 
					(clientes 
						INNER JOIN paises ON fk_paises = paises.id) 
						INNER JOIN canales ON fk_canales = canales.id
				WHERE 
					1 ";
				foreach ($datos as $key => $value) {
					$sql .= "AND clientes.$key = '$value' ";
				}
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getLike($datos){
		$sql = "SELECT 
					clientes.*,
					canales.nombre AS canal
				FROM 
					clientes INNER JOIN canales ON fk_canales = canales.id
				WHERE (clientes.nombre LIKE '%$datos[valor]%'					
					OR clientes.numero LIKE '%$datos[valor]%')
					AND clientes.id != 1";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}