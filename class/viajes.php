<?php
require_once 'database.php';
require_once 'model.php';
require_once 'viajesLogs.php';
require_once 'documentos.php';
require_once 'cxc.php';
require_once 'cxcPagos.php';

class viajes extends model{
	protected $tabla = 'viajes';

	public function select($datos){
		$respuesta = parent::select($datos);
		$respuesta['data'][0]['itinerario'] = str_replace("\r\n", "\\n", $respuesta['data'][0]['itinerario']);
		return $respuesta;
	}

	public function insert($datos){
		$resultado = parent::insert($datos);
		if($resultado['ejecuto']){
			$arreglo = [
				'tipo'=>'Creación',
				'fk_viajes'=>$resultado['insertId'],
				'estado'=>$datos['estado'],
				'observaciones'=>implode("|", $datos)
			];
			$p = new viajesLogs();
			$p->insert($arreglo);
		}
		return $resultado;
	}

	public function update($datos){		
		$resultado = parent::update($datos);
		if($resultado['ejecuto']){
			$arreglo = [
				'tipo'=>'Actualización',
				'fk_viajes'=>$datos['id'],
				'estado'=>$datos['estado'],
				'observaciones'=>implode("|", $datos)
			];
			$p = new viajesLogs();
			$p->insert($arreglo);
		}
		return $resultado;
	}

	public function fijarValores($datos){
		$datos['redondeo_pasajeros'] =  json_encode($datos['redondeo_pasajeros'], JSON_UNESCAPED_UNICODE);		
		$resultado = parent::update($datos);
		if($resultado['ejecuto']){
			$arreglo = [
				'tipo'=>'Actualización',
				'fk_viajes'=>$datos['id'],
				'estado'=>$datos['estado'],
				'observaciones'=>implode("|", $datos)
			];
			$p = new viajesLogs();
			$p->insert($arreglo);
		}
		//Verificar si existe cuenta de cobro		
		$objcxc = new cxc();
		$cxc = $objcxc->select(['fk_viajes'=>$datos['id']]);				
		if(count($cxc['data'])){
			$infocxc = [
				'id'=>$cxc['data'][0]['id'],
				'valor'=>$datos['precio'],
				'saldo'=>$datos['precio'],
				'estado'=>'Pendiente'
			];
			//busco si tiene abonos, si es asi los sumo y modifico los valores de la cxc
			$objpagos = new cxcPagos();
			$pagos = $objpagos->select(['fk_cxc'=>$cxc['data'][0]['id']]);
			if(count($pagos['data'])){
				//Sumo los pagos
				$suma_abonos = 0;
				foreach ($pagos['data'] as $key => $value) {
					$suma_abonos += $value['valor'];
				}
				$infocxc['saldo'] = $datos['precio'] - $suma_abonos;
				if($infocxc['saldo'] <= 0){
					$infocxc['estado'] = 'Finalizado';
				}
			}
			$resultado = $objcxc->update($infocxc);			
		}
		return $resultado;
	}	

	public function getViajes($datos){
		$sql = "SELECT
					viajes.id,
					viajes.codigo_reserva AS cr,
					viajes.nombre,
					viajes.nombre_viaje,
					clientes.nombre AS cliente,
					clientes.tipo_doc,
					clientes.numero,
					viajes.pasajeros,
					viajes.fecha_inicio,
					viajes.fecha_fin,
					viajes.idioma,
					viajes.moneda,
					viajes.tasa,
					viajes.margen,
					viajes.redondeo_pasajeros,
					viajes.precio,					
					viajes.estado,
					viajes.itinerario,
					viajes.fecha_creacion,
					usuarios.id AS idVendedor,
					usuarios.nombre AS vendedor,
					usuarios.telefono,
					usuarios.correo,
					usuarios.skype,
					DATEDIFF(fecha_fin,now()) AS diasRestantes
				FROM (viajes 
					INNER JOIN usuarios ON viajes.creado_por = usuarios.id) 
					INNER JOIN clientes ON fk_clientes = clientes.id
				WHERE viajes.$datos[campo] = '$datos[valor]'
					AND viajes.id != 1
				ORDER BY viajes.fecha_inicio";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function crearReserva($datos){
		//Primero se crea el codigo de reserva, este se hace autoincremental
		$sql = "UPDATE 
					viajes AS dest,
					(SELECT MAX(codigo_reserva)+1 AS codigo FROM viajes) AS src
				SET 
					dest.codigo_reserva = src.codigo,
					dest.fecha_reserva = DATE(NOW()),
					dest.estado = 'Reserva'
				WHERE dest.id = $datos[id]";
		$db = new database();
		$resultado = $db->ejecutarConsulta($sql);
		if($resultado['ejecuto']){
			//Se crean los registros para el checkin de documentos
			$documentos = ['Vuelos internacionales', 'Ficha de contratación', 'Seguro de viaje', 'Programa', 'Carta de acompañante', 'Tiquetes de vuelos nacionales', 'Voucher general', 'Aviso de bienvenida', 'Guía general de Colombia', 'Carta de bienvenida'];
			$d = new documentos();
			foreach ($documentos as $value) {				
				$d->insert([
					'fk_viajes' => $datos['id'],
					'nombre' => $value					
				]);
			}

		}
		return $resultado;
	}

	public function guardarItinerario($datos){
		$info = [
			'id' => $datos['id'],
			'itinerario' =>	json_encode($datos['itinerario'], JSON_UNESCAPED_UNICODE)
		];
		return parent::update($info);
	}
}