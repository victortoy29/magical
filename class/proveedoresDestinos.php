<?php
require_once 'database.php';
require_once 'model.php';

class proveedoresDestinos extends model{
	protected $tabla = 'proveedores_destinos';

	public function delete($datos){
		$sql = "DELETE FROM proveedores_destinos WHERE id = $datos[id]";				
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getDestinos($datos){
		$sql = "SELECT
					proveedores_destinos.id,
					nombre
				FROM proveedores_destinos INNER JOIN destinos ON fk_destinos = destinos.id
				WHERE proveedores_destinos.$datos[campo] = $datos[valor]";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getDestinos2($datos){
		$sql = "SELECT
					destinos.id,
					nombre
				FROM proveedores_destinos INNER JOIN destinos ON fk_destinos = destinos.id
				WHERE fk_proveedores = $datos[p]";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}
}
