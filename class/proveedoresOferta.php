<?php
require_once 'database.php';
require_once 'model.php';

class proveedoresOferta extends model{
	protected $tabla = 'proveedores_oferta';

	public function delete($datos){
		$sql = "DELETE FROM proveedores_oferta WHERE id = $datos[id]";				
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getOferta($datos){
		$sql = "SELECT
					proveedores_oferta.id,
					nombre
				FROM proveedores_oferta INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id
				WHERE proveedores_oferta.$datos[campo] = $datos[valor]";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getOferta2($datos){
		$sql = "SELECT
					tipos_servicio.id,
					nombre
				FROM proveedores_oferta INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id
				WHERE fk_proveedores = $datos[p]";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}
}