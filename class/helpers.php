<?php
class helpers{
	public function sesion(){
		$permisos = [
			'Administrador' => [
				'menuConfiguracion',				
				'menuClientes',				
				'menuComercial',				
				'menuOperaciones',
				'menuCxC',
				'menuCxP'
			],
			'Comercial' => [
				'menuClientes',				
				'menuComercial'
			],
			'Operaciones' => [
				'menuOperaciones'
			],
			'Contabilidad' => [
				'menuCxC',
				'menuCxP'
			]
		];

		return [
			'ejecuto' => true,
			'data' => $_SESSION,
			'permisos' => $permisos[$_SESSION['usuario']['perfil']]
		];
	}
}