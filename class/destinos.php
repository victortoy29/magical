<?php
require_once 'database.php';
require_once 'model.php';

class destinos extends model{
	protected $tabla = 'destinos';

	public function insert($datos){
		$respuesta = parent::insert($datos);
		if($respuesta['ejecuto']){
			//Crear directorio
			mkdir("../assets/img/destinos/destino_".$respuesta['insertId']);
		}
		return $respuesta;
	}

	public function getDestinos($datos){
		$sql = "SELECT *
				FROM destinos
				WHERE estado = 'Activo'
				ORDER BY nombre";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function subirImagen($datos){
		$filename = $_FILES['file']['name'];		
		$location = "../assets/img/destinos/destino_".$datos."/".$filename;
		$imageFileType = pathinfo($location,PATHINFO_EXTENSION);
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "JPG") {
 			return [
				'ejecuto' => false,
				'msgError' => 'Tipo de archivo no permitido'
			];
		}		
		if(move_uploaded_file($_FILES['file']['tmp_name'],"../assets/img/destinos/destino_".$datos."/".$filename)){
			return [
				'ejecuto' => true,
				'msg' => 'Subio correctamente'
			];
		}
	}

	public function getImages($datos){
		$respuesta = [
			'ejecuto' => true,
			'data' => []
		];
		$fotos = array_diff(scandir("../assets/img/destinos/destino_".$datos['id']), array('..', '.'));
		foreach ($fotos as $value) {
			$respuesta['data'][] = $value;
		}		
		return $respuesta;
	}

	public function borrarImagen($datos){
		if(file_exists("../assets/img/destinos/destino_$datos[id]/$datos[imagen]")){
    		unlink("../assets/img/destinos/destino_$datos[id]/$datos[imagen]");
    		$respuesta = [
				'ejecuto' => true,
				'msg' => 'Se borro correctamente'
			];
		}else{
    		$respuesta = [
				'ejecuto' => true,
				'msg' => 'No existia la imagen'
			];
		}
		return $respuesta;
	}
}