<?php
require_once 'database.php';
require_once 'model.php';

class causales extends model{
	protected $tabla = 'causales';

	public function getCausales($datos){
		$sql = "SELECT 
					* 
				FROM 
					causales
				WHERE 
					estado = 'Activo'
				ORDER BY 
					nombre";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}
}