<?php

class database extends mysqli{
	/*private $DB_HOST = 'localhost';
	private $DB_NAME = 'magical_management';
	private $DB_USER = 'magicalAdmin';
	private $DB_PASS = 'm4g1c4l4dm1n*';	*/

	/*private $DB_HOST = 'localhost';
	private $DB_USER = 'ktsonlin_magicu';
	private $DB_PASS = 'm4G1c4L*#';
	private $DB_NAME = 'ktsonlin_magical';*/

	private $DB_HOST = 'localhost';
	private $DB_USER = 'root';
	private $DB_PASS = '';
	private $DB_NAME = 'magical';

	public function __construct()
	{
		//Realizo la conexión a la base de datos
		parent::__construct($this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME);

		//Se valida si la conexion se realizo
		if (mysqli_connect_errno())
		{
    		printf("Falló la conexión: %s\n", mysqli_connect_error());
    		exit();
		}

		//Se setea el conjunto de caracteres		
		if (!$this->set_charset("utf8")) 
		{
    		printf("Error cargando el conjunto de caracteres utf8: %s\n", $this->error);
    		exit();
		}		
	}

	public function select($tabla, $datos)
	{
		$sql = "SELECT * FROM $tabla WHERE 1 ";
		foreach ($datos as $key => $value) {
			$sql .= "AND $key = '$value' ";
		}
		$sql.="AND id != 1";
    	return $this->ejecutarConsulta($sql);
	}

	public function insert($tabla,$datos)
	{
		$sql = "INSERT INTO $tabla SET ";
    	foreach ($datos as $clave=>$valor)
    	{       	
    		$sql .= "$clave = '$valor',";    		
    	}    	
		$sql .= "creado_por =".$_SESSION['usuario']['id'].",fecha_creacion=DATE_ADD(NOW(), INTERVAL 2 HOUR),fecha_modificacion=DATE_ADD(NOW(), INTERVAL 2 HOUR)";
		//$sql .= "creado_por =".$_SESSION['usuario']['id'].",fecha_creacion=NOW(),fecha_modificacion=NOW()";
    	return $this->ejecutarConsulta($sql);
	}

	public function update($tabla,$datos)
	{
		$sql = "UPDATE $tabla SET ";
    	foreach ($datos as $clave=>$valor)
    	{        	
			$sql .= "$clave = '$valor',";        	
    	}
    	$sql .= "modificado_por =".$_SESSION['usuario']['id'].",fecha_modificacion=DATE_ADD(NOW(), INTERVAL 2 HOUR)";
    	//$sql .= "modificado_por =".$_SESSION['usuario']['id'].",fecha_modificacion=NOW()";
    	$sql .= " WHERE id = ".$datos['id'];
    	return $this->ejecutarConsulta($sql);
	}

	public function ejecutarConsulta($sql)
	{	
		$respuesta = array();
		$result = $this->query($sql);		
		if($result===TRUE)			
		{				
			//Devuelve true en caso de que sea un INSERT, UPDATE o DELETE
			$respuesta['ejecuto'] = true;
			$respuesta['insertId'] = $this->insert_id;
			$respuesta['affectedRows'] = $this->affected_rows;		
		}
		elseif(is_object($result))
		{	
			//Este caso cuando sea un query que devuelva resultados
			$respuesta['ejecuto'] = true;
			$respuesta['data'] = array();			
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$respuesta['data'][] = $row;
			}			
			$result->free();
		}
		else
		{			
			$respuesta['ejecuto'] = false;
			$respuesta['codigoError'] = $this->errno;
			$respuesta['msgError'] = $this->error;
		}	
		$this->close();	
		return $respuesta;
	}	
}