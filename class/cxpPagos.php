<?php
require_once 'database.php';
require_once 'model.php';
require_once 'cxp.php';

class cxpPagos extends model{
	protected $tabla = 'cxp_pagos';

	public function acentarPago($datos){		
		//Descuento de la cuenta principal
		$cxp = new cxp();
		$resultado = $cxp->select(['id'=>$datos['cxp']]);
		if($resultado['data'][0]['saldo'] != 0){
			$saldo = $resultado['data'][0]['saldo']-$datos['valor'];
			if($saldo == 0){
				$info = [
					'id'=> $datos['cxp'],
					'saldo'=>$saldo,
					'estado'=>'Finalizado'
				];
			}else{
				$info = [
					'id'=> $datos['cxp'],
					'saldo'=>$saldo
				];
			}			
			$respuesta = $cxp->update($info);
		}

		//Luego creo el pago
		$info = [
			'fk_cxp'=>$datos['cxp'],
			'valor' => $datos['valor']
		];
		$resultado = parent::insert($info);
		if($resultado['ejecuto']){
			//Guardo la imagen
			if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/comprobantes_cxp/'.$resultado['insertId'].'.jpg')){
				return $resultado;
			}
		}
	}
}