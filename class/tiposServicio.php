<?php
require_once 'database.php';
require_once 'model.php';

class tiposServicio extends model{
	protected $tabla = 'tipos_servicio';

	public function getTiposServicio($datos){
		$sql = "SELECT *
				FROM tipos_servicio
				WHERE estado = 'Activo'
				ORDER BY id";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}
}