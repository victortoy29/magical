<?php
require_once 'database.php';
require_once 'model.php';

class pasajeros extends model{
	protected $tabla = 'pasajeros';

	public function getPasajeros($datos){
		$sql = "SELECT
					pasajeros.*,
					DATEDIFF(fecha_vencimiento, fecha_inicio) AS dias
				FROM 
					pasajeros INNER JOIN viajes ON fk_viajes = viajes.id
				WHERE
					1 ";
		foreach ($datos as $key => $value) {
			$sql .= "AND pasajeros.$key = '$value' ";
		}
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}	

	public function subirImagen($datos){
		$filename = $_FILES['file']['name'];		
		$location = "../assets/img/documentos/".$filename;		
		$imageFileType = pathinfo($location,PATHINFO_EXTENSION);
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "JPG") {
 			return [
				'ejecuto' => false,
				'msgError' => 'Tipo de archivo no permitido'
			];
		}		
		if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/documentos/'.$datos.'.jpg')){
			return [
				'ejecuto' => true,
				'msg' => 'Subio correctamente'
			];
		}
	}
}