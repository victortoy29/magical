<?php

require_once 'viajes.php';
require_once 'viajesDetalle.php';
require_once '../plugins/fpdf/fpdf.php';

$idiomas = [
	'es' => [
		"miviajeacolombia" => "Mi viaje a Colombia",
		"codigo" => "Código",
		"fechainicio" => "Fecha inicio",
		"fechafin" => "Fecha fin",
		"numero" => "Número de personas",
		"etapas" => "Etapas principales de mi viaje",
		"dia" => "Día",
		"etapas" => "Etapas",
		"comidas" => "Comidas",
		"alojamiento" => "Alojamiento",
		"precio" => "Precio",
		"preciototal" => "Precio total del viaje",
		"condicionespago" => "Condiciones de pago",
		"condicionesCancel" => "Condiciones de cancelación",
		"pasaportes" => "Pasaportes y visado",
		"pasaportesdetalle" => "Verifica los documentos de viaje requeridos: (Dni, pasaporte y visado) documentos para la entrada y estancia en el país de destino con la Embajada o onsulado de ese país en su país de residencia de viaje. Tratándose del pasaporte, algunos países exigen una validez mínima. Los trámites pueden variar segñun el país de destino y la nacionalidad de los viajeros. Verifica, por tanto, todos estos datos antes de tu viaje.",
		"seguros" => "Seguros",
		"antes" => "Antes de tu viaje",
		"antesdetalle" => "Al comprar un viaje, es muy recomendable estar asegurado. De hecho, un seguro de cancelación cubre el riesgo financiero si se ve obligado a cancelar su viaje. En caso de cancelación, las agencias locales no pueden reembolsarte la totalidad de su viaje porque ellos mismos están comprometidos con hoteles, transportistas, guías ... Por lo tanto, aplicarán un programa de reembolso que tiene en cuenta la proximidad de su salida (las condiciones de reembolso en caso de cancelación varían según las agencias locales: las encontrará en el presupuesto o en el formulario de inscripción que te comunicaron).",
		"durante" => "Durante tu viaje",
		"durantedetalle" => "Una asistencia de repatriación te protege en caso de un accidente en su destino: permite tu atención médica y, si es necesario, tu repatriación. Para obtener más información, inicia sesión en tu cuenta de Evaneos, ve a tu viaje, sección Servicios. Puedes suscribir un seguro al pagar el viaje. Si la opción no aparece, no dudes en ponerte en contacto con nuestro socio Chapka."
	],
	'en' => [
		"miviajeacolombia" => "My travel to Colombia",
		"codigo" => "Code",
		"fechainicio" => "Start date",
		"fechafin" => "End date",
		"numero" => "Number of people",
		"etapas" => "Main stages of my trip",
		"dia" => "Day",
		"etapas" => "Stages",
		"comidas" => "foods",
		"alojamiento" => "accommodation",
		"precio" => "Price",
		"preciototal" => "Total price of the trip",
		"condicionespago" => "Payment conditions",
		"condicionesCancel" => "Cancelation conditions",
		"pasaportes" => "passports and visa",
		"pasaportesdetalle" => "Verify the required travel documents: (ID, passport and visa) documents for entry and stay in the country of destination with the embassy or ensulate of that country in your country of travel residence. In the case of a passport, some countries require a minimum validity. The procedures may vary depending on the country of destination and the nationality of the travelers. Verify, therefore, all this information before your trip.",
		"seguros" => "Insurance",
		"antes" => "Before your trip",
		"antesdetalle" => "When buying a trip, it is highly recommended to be insured. In fact, cancellation insurance covers financial risk if you are forced to cancel your trip. In case of cancellation, local agencies cannot reimburse you for their entire trip because they themselves are committed to hotels, carriers, guides ... Therefore, they will apply a reimbursement program that takes into account the proximity of their departure (the refund conditions in case of cancellation vary according to local agencies: you will find them in the budget or in the registration form that they communicated to you).",
		"durante" => "During your trip",
		"durantedetalle" => "A repatriation assistance protects you in the event of an accident at your destination: allow your medical attention and, if necessary, your repatriation. For more information, log in to your Evaneos account, go to your trip, Services section. You can subscribe insurance when paying for the trip. If the option does not appear, do not hesitate to contact our partner Chapka."
	],
	'it' => [
		"miviajeacolombia" => "il mio viaggio in colombia",
		"codigo" => "Codice",
		"fechainicio" => "Data d'inizio",
		"fechafin" => "Data di fine",
		"numero" => "Numero di persone",
		"etapas" => "Fasi principali del mio viaggio",
		"dia" => "Giorno",
		"etapas" => "stadi",
		"comidas" => "pasti",
		"alojamiento" => "alloggio",
		"precio" => "Prezzo",
		"preciototal" => "Prezzo totale del viaggio",
		"condicionespago" => "Condizioni di pagamento",
		"condicionesCancel" => "Condizioni di annullamento",
		"pasaportes" => "Passaporti e visti",
		"pasaportesdetalle" => "Verificare i documenti di viaggio richiesti: documenti (documento di identità, passaporto e visto) per l'ingresso e il soggiorno nel paese di destinazione presso l'ambasciata o l'insieme di tale paese nel paese di residenza del viaggio. Nel caso di un passaporto, alcuni paesi richiedono una validità minima. Le procedure possono variare a seconda del paese di destinazione e della nazionalità dei viaggiatori. Verificare, quindi, tutte queste informazioni prima del viaggio.",
		"seguros" => "Assicurazione",
		"antes" => "Prima del viaggio",
		"antesdetalle" => "Al momento dell'acquisto di un viaggio, si consiglia vivamente di essere assicurato. In effetti, l'assicurazione annullamento copre i rischi finanziari se si è costretti a cancellare il viaggio. In caso di cancellazione, le agenzie locali non possono rimborsarti per l'intero viaggio perché si impegnano direttamente in hotel, vettori, guide ... Pertanto, applicheranno un programma di rimborso che tenga conto della vicinanza della loro partenza (il le condizioni di rimborso in caso di cancellazione variano a seconda delle agenzie locali: le troverai nel budget o nel modulo di registrazione che ti hanno comunicato).",
		"durante" => "Durante il tuo viaggio",
		"durantedetalle" => "Un'assistenza di rimpatrio ti protegge in caso di incidente a destinazione: consenti alle tue cure mediche e, se necessario, al tuo rimpatrio. Per ulteriori informazioni, accedi al tuo account Evaneos, vai al tuo viaggio, sezione Servizi. È possibile sottoscrivere un'assicurazione quando si paga il viaggio. Se l'opzione non viene visualizzata, non esitare a contattare il nostro partner Chapka."
	]
];

$objViaje = new viajes();
$viaje = $objViaje->getViajes(['campo'=>'id', 'valor'=> $_GET['idV']]);

$paginaFinal = 0;

$itinerario = json_decode(str_replace("\r\n", "\\n", $viaje['data'][0]['itinerario']));

//print_r($itinerario);

class PDF extends FPDF{	
	// Pie de página
	function Footer(){
		global $paginaFinal;		
	    if($this->PageNo() != 1 and $this->PageNo() != $paginaFinal){
	    	$this->Image('../assets/img/logo2.png',170,280,30);
	    }
	}
}

$pdf = new PDF();
$pdf->AddFont('Anderson The Secret Service','','Anderson The Secret Service.php');
$pdf->AddPage();
$pdf->Image('../assets/img/portada.jpg',0,17,210);
$pdf->SetFont('Arial','B',20);
$pdf->Image('../assets/img/logo2.png',50,80);
$pdf->SetY(150);
$pdf->SetTextColor(63,72,79);
$pdf->SetFont('Anderson The Secret Service','',10);
$pdf->Cell(190,10,'Sr. '.utf8_decode($viaje['data'][0]['cliente']),0,1,'C');
$pdf->SetFont('Anderson The Secret Service','',15);
$pdf->Cell(190,20,utf8_decode($viaje['data'][0]['nombre_viaje']),0,1,'C');
$pdf->SetFont('Anderson The Secret Service','',30);
$pdf->Cell(190,10,'Colombia',0,1,'C');
$pdf->Ln(70);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,10,'www.magicalcolombia.com',0,1,'C');

$pdf->AddPage();
$pdf->Image('../assets/img/usuarios/'.$viaje['data'][0]['idVendedor'].'.jpg',10,30,65);
$pdf->SetFont('Arial','B',14);
$pdf->SetXY(80,33);
$pdf->Cell(120,5,utf8_decode($viaje['data'][0]['vendedor']),0,1);
$pdf->SetFont('Arial','',13);
$pdf->SetX(80);
$pdf->Cell(120,5,'PRIVATE TRAVEL DESIGNER',0,1);
$pdf->Ln(8);
$pdf->SetFont('Arial','',13);
$pdf->SetX(80);
$pdf->Cell(130,5,'Magical Colombia S.A.S.',0,1);
$pdf->SetX(80);
$pdf->Cell(130,5,'Calle 6 # 76 - 70, Local 21, C.C. Capri',0,1);
$pdf->SetX(80);
$pdf->Cell(130,5,'Santiago de Cali - Colombia',0,1);
$pdf->Ln(8);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(22,5,utf8_decode('Teléfono:'));
$pdf->SetFont('Arial','',13);
$pdf->Cell(80,5,$viaje['data'][0]['telefono'],0,1);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(15,5,'Email:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['correo'],0,1);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(17,5,'Skype:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['skype'],0,1);
$pdf->SetX(80);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(23,5,'Sitio web:');
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,'http://www.magicalcolombia.com',0,1);
$pdf->Ln(10);

$pdf->SetDrawColor(203,170,52);
$pdf->Line(10,105,200,105);

$pdf->SetY(105);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,15,$idiomas[$viaje['data'][0]['idioma']]['miviajeacolombia'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['codigo']));
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,'C-'.$viaje['data'][0]['id'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['fechainicio']));
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['fecha_inicio'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['fechafin']));
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['fecha_fin'],0,1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(50,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['numero']));
$pdf->SetFont('Arial','',13);
$pdf->Cell(90,5,$viaje['data'][0]['pasajeros'],0,1);

$pdf->Line(10,145,200,145);

$pdf->SetY(145);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,15,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['etapas']),0,1);

$dia = 1;
foreach ($itinerario->itinerario as $detalle) {	
	if($dia == 11){
		$pdf->SetY(160);
	}
	if($dia >= 11){
		$pdf->SetX(105);
	}
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(25,10,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['dia']).' '.$dia.':');
	$pdf->SetFont('Arial','',13);
	$pdf->Cell(6,10,$pdf->Image('../assets/img/location.png',$pdf->GetX(),$pdf->GetY()+2));
	$pdf->Cell(60,10,utf8_decode($detalle->etapa),0,1);
	$dia++;
}


//Bloque de codigo para dia a dia
setlocale(LC_ALL,"es_ES");
$dia = 1;
$pagina = 0;
foreach ($itinerario->itinerario as $detalle) {
	if($pagina % 2 == 0){
		$pdf->AddPage();
		$pdf->SetY(20);
	}
	$y = $pdf->GetY();
	if($detalle->foto1 != ''){
		$pdf->Cell(85,62,$pdf->Image('../'.$detalle->foto1,$pdf->GetX(),$pdf->GetY(),80),0,1);
	}else{
		$pdf->Cell(85,62,'Sin imagen',0,1,'C');
	}
	if($detalle->foto2 != ''){
		$pdf->Cell(85,60,$pdf->Image('../'.$detalle->foto2,$pdf->GetX(),$pdf->GetY(),80),0,1);
	}else{
		$pdf->Cell(85,60,'Sin imagen',0,0,'C');
	}
	$pdf->SetXY(95, $y);
	//Titulo
	$pdf->SetFont('Arial','B',14);
	$pdf->MultiCell(95,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['dia'].' '.$dia.': '.$detalle->titulo));	
	//Fecha
	$pdf->SetX(95);
	$date = DateTime::createFromFormat("Y-m-d", $detalle->fecha);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(95,7,ucfirst(utf8_decode(strftime("%A, %d de %B de %Y",$date->getTimestamp()))),0,1);
	//Descripción
	$pdf->SetX(95);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(95,5,utf8_decode($detalle->descripcion));
	//Etapa
	$pdf->SetXY(95,$y+101);	
	$pdf->Cell(7,7,$pdf->Image('../assets/img/location2.png',$pdf->GetX(),$pdf->GetY()));
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,7,$idiomas[$viaje['data'][0]['idioma']]['etapas']);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(72,7,utf8_decode($detalle->etapa),0,1);
	//Comidas
	if(isset($detalle->comidas)){
		$pdf->SetX(95);
		$pdf->Cell(7,7,$pdf->Image('../assets/img/comidas.png',$pdf->GetX(),$pdf->GetY()+1));
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,7,$idiomas[$viaje['data'][0]['idioma']]['comidas']);
		$x = '';
		foreach ($detalle->comidas as $comida){
			$x .= $comida.', ';
		}
		$x = rtrim($x, ', ');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(69,7,$x,0,1);
	}	
	//Alojamiento
	$pdf->SetX(95);
	$pdf->Cell(7,7,$pdf->Image('../assets/img/hospedaje.png',$pdf->GetX(),$pdf->GetY()+1));
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,7,$idiomas[$viaje['data'][0]['idioma']]['alojamiento']);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(60,7,utf8_decode($detalle->alojamiento),0,1);

	$pdf->Line(10,145,200,145);
	$pdf->SetY(150);
	$dia++;
	$pagina++;
}

$pdf->AddPage();
$pdf->SetY(33);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,5,$idiomas[$viaje['data'][0]['idioma']]['precio'],0,1);

$pdf->Line(10,40,200,40);

$pdf->SetFont('Arial','',13);
$pdf->SetY(40);

if($viaje['data'][0]['redondeo_pasajeros'] != ''){
	$valoresPorPersona = json_decode($viaje['data'][0]['redondeo_pasajeros']);
	foreach ($valoresPorPersona as $pasajero => $valor) {
		$pdf->Cell(90,10,$pasajero);
		$pdf->Cell(100,10,number_format($valor,0,',','.').' '.$viaje['data'][0]['moneda'],0,1,'R');
	}
}

$pdf->Line(10,$pdf->GetY(),200,$pdf->GetY());
$pdf->Ln(3);

$pdf->Cell(190,5,$idiomas[$viaje['data'][0]['idioma']]['preciototal'],0,1,'R');
$pdf->Cell(190,5,number_format($viaje['data'][0]['precio'],0,',','.').' '.$viaje['data'][0]['moneda'],0,1,'R');

$pdf->SetY($pdf->GetY()+5);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,$idiomas[$viaje['data'][0]['idioma']]['condicionespago'],0,1);
$pdf->SetFont('Arial','',12);
$pdf->Cell(190,5,utf8_decode($itinerario->condiciones_pago),0,1);

$pdf->Line(10,$pdf->GetY()+5,200,$pdf->GetY()+5);

$pdf->SetY($pdf->GetY()+5);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['condicionesCancel']),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode($itinerario->condiciones_cancelacion),0);

$pdf->Line(10,$pdf->GetY()+5,200,$pdf->GetY()+5);

$pdf->SetY($pdf->GetY()+5);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['pasaportes']),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['pasaportesdetalle']),0);

$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['seguros']),0,1);
$pdf->SetFont('Arial','',14);
$pdf->Cell(190,10,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['antes']),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['antesdetalle']),0);
$pdf->Ln();

$pdf->SetFont('Arial','',14);
$pdf->Cell(190,10,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['durante']),0,1);
$pdf->SetFont('Arial','',12);
$pdf->MultiCell(190,5,utf8_decode($idiomas[$viaje['data'][0]['idioma']]['durantedetalle']),0);

$pdf->AddPage();
$paginaFinal = $pdf->PageNo();
$pdf->Image('../assets/img/contraportada.jpg',0,17,210);
$pdf->Image('../assets/img/logo2.png',60,25,80);
$pdf->SetY(200);
$pdf->SetFont('Arial','B',20);
$pdf->Cell(190,7,'MAGICAL COLOMBIA',0,1,'C');
$pdf->SetFont('Arial','',14);
$pdf->Cell(190,7,utf8_decode('OPERADOR TURÍSTICO RECEPTIVO'),0,1,'C');
$pdf->Ln();
$pdf->Cell(190,5,utf8_decode('Magical Colombia S.A.S. NIT: 900902837 - 3 / RNT: 41848'),0,1,'C');
$pdf->Cell(190,5,utf8_decode('Télefono: +57 2 3451220 / E-mail: info@magicalcolombia.com'),0,1,'C');
$pdf->Cell(190,5,utf8_decode('Calle 6 # 76 - 70, Local 21, C.C. Capri'),0,1,'C');
$pdf->Cell(190,5,utf8_decode('Santiago de Cali - Colombia'),0,1,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(190,10,utf8_decode('www.magicalcolombia.com'),0,1,'C');

$pdf->Link(90, 270, 10, 28, "https://www.facebook.com/magicalcolombia/");
$pdf->Image('../assets/img/facebook.png',90,270);
$pdf->Link(110, 270, 10, 28, "https://www.instagram.com/magicalcolombiatravel/");
$pdf->Image('../assets/img/instagram.png',110,270);

$pdf->Output();
?>