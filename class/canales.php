<?php
require_once 'database.php';
require_once 'model.php';

class canales extends model{
	protected $tabla = 'canales';

	public function getCanales($datos){
		$sql = "SELECT * 
				FROM canales
				WHERE estado = 'Activo'
				ORDER BY nombre";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}
}