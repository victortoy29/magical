<?php
require_once 'database.php';
require_once 'model.php';

class cxc extends model{
	protected $tabla = 'cxc';

	public function crearCuenta($datos){
		$sql = "SELECT 
					*
				FROM 
					cxc
				WHERE
					fk_viajes = $datos[viaje]
					AND estado != 'Cancelada'";
		$db = new database();
		$resultado = $db->ejecutarConsulta($sql);
		if (count($resultado['data']) == 0) {
			return parent::insert([
				'fk_viajes'=>$datos['viaje'],
				'valor'=>$datos['valor'],
				'saldo'=>$datos['valor']
			]);
		}else{
			return [
				'ejecuto' => false,
				'msgError' => 'Ya existe una cuenta de cobro activa'
			];
		}
	}

	public function getCuentas($datos){
		$sql = "SELECT
					cxc.id,
					viajes.id AS viaje,
					viajes.nombre AS nombre_viaje,
					viajes.moneda,
					clientes.id AS idc,
					clientes.nombre,
					cxc.valor,
					cxc.saldo,
					cxc.estado
				FROM
					cxc INNER JOIN (viajes INNER JOIN clientes ON fk_clientes = clientes.id) ON fk_viajes = viajes.id
				WHERE
					1 ";
				foreach ($datos as $key => $value) {
					$sql .= "AND cxc.$key = '$value' ";
				}
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getHistorico($datos){
		$filtro = 1;
		if(isset($datos['cliente']) and $datos['cliente'] != ''){
			$filtro .= " AND clientes.id = ".$datos['cliente'];
		}
		if($datos['cotizacion'] != ''){
			$filtro .= " AND viajes.id = ".$datos['cotizacion'];
		}
		if($datos['reserva'] != ''){
			$filtro .= " AND viajes.codigo_reserva = ".$datos['reserva'];
		}
		if($datos['fechai'] != ''){
			$filtro .= " AND cxc.fecha_creacion >= '$datos[fechai]'";
		}
		if($datos['fechaf'] != ''){
			$filtro .= " AND cxc.fecha_creacion <= '$datos[fechaf] 23:59:59'";
		}
		$sql = "SELECT
					cxc.id,
					viajes.id AS viaje,
					codigo_reserva,
					clientes.id AS idc,
					clientes.nombre,
					cxc.valor,
					cxc.saldo,
					cxc.estado
				FROM
					cxc INNER JOIN (viajes INNER JOIN clientes ON fk_clientes = clientes.id) ON fk_viajes = viajes.id					
				WHERE 
					$filtro
				ORDER BY 
					cxc.fecha_creacion";
		$db = new database();		
       	return $db->ejecutarConsulta($sql);
	}
}