<?php
require_once 'database.php';
require_once 'model.php';

class cxp extends model{
	protected $tabla = 'cxp';

	public function crearCuenta($datos){
		$resultado = parent::select([
			'fk_viajes'=>$datos['viaje'],
			'fk_proveedores'=>$datos['proveedor']
		]);
		if (count($resultado['data']) == 0) {
			return parent::insert([
				'fk_viajes'=>$datos['viaje'],
				'fk_proveedores'=>$datos['proveedor'],
				'valor_calculado'=>$datos['valor']
			]);
		}else{
			return parent::update([
				'id'=>$resultado['data'][0]['id'],
				'valor_calculado'=>$resultado['data'][0]['valor_calculado']+$datos['valor']				
			]);
		}
	}

	public function conciliar($datos){
		$resultado = parent::update([
			'id'=>$datos['cxp'],
			'valor_conciliado'=>$datos['valor'],
			'saldo'=>$datos['valor'],
			'estado'=>'Pendiente'
		]);
		if($resultado['ejecuto']){
			if(move_uploaded_file($_FILES['file']['tmp_name'],'../assets/img/cxp/'.$datos['cxp'].'.jpg')){
				return $resultado;
			}
		}
	}

	public function getCuentas($datos){
		$sql = "SELECT
					cxp.id,
					codigo_reserva,
					viajes.nombre AS reserva,
					proveedores.id AS idp,
					proveedores.nombre,
					cxp.valor_calculado,
					cxp.valor_conciliado,
					cxp.saldo,
					cxp.estado
				FROM 
					(cxp INNER JOIN proveedores ON fk_proveedores = proveedores.id) INNER JOIN viajes ON fk_viajes = viajes.id
				WHERE
					1 ";
				foreach ($datos as $key => $value) {
					$sql .= "AND cxp.$key = '$value' ";
				}
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getHistorico($datos){
		$filtro = 1;
		if(isset($datos['proveedor']) and $datos['proveedor'] != ''){
			$filtro .= " AND proveedores.id = ".$datos['proveedor'];
		}		
		if($datos['reserva'] != ''){
			$filtro .= " AND viajes.codigo_reserva = ".$datos['reserva'];
		}
		if($datos['fechai'] != ''){
			$filtro .= " AND cxp.fecha_creacion >= '$datos[fechai]'";
		}
		if($datos['fechaf'] != ''){
			$filtro .= " AND cxp.fecha_creacion <= '$datos[fechaf] 23:59:59'";
		}
		$sql = "SELECT
					cxp.id,
					codigo_reserva,
					proveedores.id AS idp,
					proveedores.nombre,
					cxp.valor,
					cxp.saldo,
					cxp.estado
				FROM
					(cxp INNER JOIN proveedores ON fk_proveedores = proveedores.id) INNER JOIN viajes ON fk_viajes = viajes.id
				WHERE 
					$filtro
				ORDER BY 
					cxp.fecha_creacion";
		$db = new database();		
       	return $db->ejecutarConsulta($sql);
	}
}