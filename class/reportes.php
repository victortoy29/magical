<?php
require_once 'database.php';

class reportes{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//DASBOARD ADMINISTRATIVO
	//Ingresos
	public function getIngresos($datos){
		$sql = "SELECT
					SUM(valor*tasa*factor) AS total
				FROM
					cxc_pagos
				WHERE
					fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	//Egresos
	public function getEgresos($datos){
		$sql = "SELECT
					SUM(valor) AS total
				FROM
					cxp_pagos
				WHERE
					fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	//Rseservas efectivas
	public function getReservas($datos){
		$sql = "SELECT
					COUNT(1) AS total
				FROM
					viajes
				WHERE
					estado IN ('Reserva', 'Terminada')
					AND fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	public function estadoViajes($datos){
		$sql = "SELECT
					estado,
					COUNT(1) AS cantidad
				FROM
					viajes
				WHERE
					estado IN ('Cotización','Reserva')
				GROUP BY
					estado";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	public function estadoServicios($datos){
		$sql = "SELECT
					viajes.estado AS ve,
					viajes_detalle.estado AS vd,					
					COUNT(1) AS cantidad
				FROM
					viajes INNER JOIN viajes_detalle ON viajes.id = fk_viajes
				WHERE
					(viajes.estado = 'Cotizacion' AND viajes_detalle.estado IN ('Gestionar','Devuelto'))
					OR (viajes.estado = 'Reserva' AND viajes_detalle.estado IN ('Bloqueado','Devuelto'))					
				GROUP BY
					viajes.estado,
					viajes_detalle.estado";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	public function estadoServicios2($datos){
		$sql = "SELECT
					viajes_detalle.estado AS vd,
					viajes.estado AS ve,
					COUNT(1) AS cantidad
				FROM
					viajes INNER JOIN viajes_detalle ON viajes.id = fk_viajes
				WHERE
					(viajes.estado = 'Cotizacion' AND viajes_detalle.estado IN ('Gestionar','Devuelto'))
					OR (viajes.estado = 'Reserva' AND viajes_detalle.estado IN ('Bloqueado','Devuelto'))					
				GROUP BY
					viajes.estado,
					viajes_detalle.estado
				ORDER BY
					viajes_detalle.estado";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	//Seguimiento diario entre cotizaciones, reservas y canceladas
	public function seguimientoDia($datos){
		$sql = "SELECT
					'Cotizaciones',
					DATE(fecha_creacion) AS fecha,
					COUNT(1) AS cantidad
				FROM 
					viajes
				WHERE
					viajes.id != 1
					AND fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY fecha
				UNION
				SELECT
					'Reservas',
					DATE(fecha_creacion) AS fecha,
					COUNT(1) AS cantidad
				FROM 
					viajes
				WHERE
					viajes.estado IN ('Reserva','Terminada')
					AND fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY fecha
				UNION
				SELECT
					'Canceladas',
					DATE(fecha_creacion) AS fecha,
					COUNT(1) AS cantidad
				FROM 
					viajes
				WHERE
					viajes.id != 1
					AND viajes.estado = 'Cancelada'
					AND fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY fecha";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}	

	public function causalesCancelacion($datos){
		$sql = "SELECT
					causales.nombre,
					COUNT(1) AS cantidad
				FROM
					viajes INNER JOIN causales ON fk_causales = causales.id
				WHERE
					viajes.id != 1
					AND viajes.estado = 'Cancelada'
					AND viajes.fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY
					fk_causales";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	//Grafico por tipo de cliente
	public function canales($datos){
		$sql = "SELECT
					canales.nombre,
					viajes.estado,					
					COUNT(1) AS cantidad
				FROM
					viajes INNER JOIN (clientes 
					INNER JOIN canales
						ON fk_canales = canales.id) 
						ON fk_clientes = clientes.id
				WHERE
					viajes.id != 1
					AND viajes.fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY
					canales.nombre,
					viajes.estado
				ORDER BY
					canales.nombre,
					viajes.estado";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	//Grafico de clientes por procedencia
	public function paisesProcedencia($datos){
		$sql = "SELECT
					paises.nombre,
					viajes.estado,
					COUNT(1) AS cantidad
				FROM
					viajes INNER JOIN paises ON fk_paises = paises.id
				WHERE
					viajes.id != 1
					AND viajes.fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY
					paises.nombre,
					viajes.estado";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	public function TiposServicios($datos){
		$sql = "SELECT
					tipos_servicio.nombre,
					COUNT(1) AS cantidad
				FROM
					(viajes_detalle 
					INNER JOIN viajes ON fk_viajes = viajes.id) 
					INNER JOIN (opciones 
					INNER JOIN (servicios
					INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE
					viajes.estado IN ('Reserva', 'Terminada')
					AND viajes_detalle.estado = 'Reservado'
					AND viajes_detalle.fecha_inicio BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY
					tipos_servicio.nombre";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	public function proveedores($datos){
		$sql = "SELECT
					proveedores.nombre,
					COUNT(1) AS cantidad
				FROM
					(viajes_detalle 
					INNER JOIN viajes ON fk_viajes = viajes.id) 
					INNER JOIN (opciones 
					INNER JOIN (servicios
					INNER JOIN proveedores ON fk_proveedores = proveedores.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE
					viajes.estado IN ('Reserva', 'Terminada')
					AND viajes_detalle.estado = 'Reservado'
					AND viajes_detalle.fecha_inicio BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY
					proveedores.nombre
				ORDER BY
					cantidad DESC";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}

	public function pagoProveedores($datos){
		$sql = "SELECT
					proveedores.nombre,
					SUM(cxp_pagos.valor) AS valor
				FROM
					proveedores INNER JOIN (cxp INNER JOIN cxp_pagos ON cxp.id = fk_cxp) ON proveedores.id = fk_proveedores
				WHERE
					cxp_pagos.id != 1
					AND cxp_pagos.fecha_creacion BETWEEN '$datos[fecha_inicio]' AND '$datos[fecha_fin] 23:59:59'
				GROUP BY
					fk_proveedores
				ORDER BY
					valor DESC";
		$db = new database();
       	return $db->ejecutarConsulta($sql);
	}
}