<?php
require_once 'database.php';
require_once 'model.php';
require_once 'viajesDetalleLogs.php';

class viajesDetalle extends model{
	protected $tabla = 'viajes_detalle';

	public function insert($datos){
		$resultado = parent::insert($datos);
		if($resultado['ejecuto']){			
			$arreglo = [
				'tipo'=>'Creación',
				'fk_viajes_detalle'=>$resultado['insertId'],
				'estado'=>$datos['estado'],
				'observaciones'=>implode("|", $datos)
			];
			$p = new viajesDetalleLogs();
			$p->insert($arreglo);
		}
		return $resultado;
	}

	public function update($datos){		
		$resultado = parent::update($datos);
		if($resultado['ejecuto']){
			$arreglo = [
				'tipo'=>'Actualización',
				'fk_viajes_detalle'=>$datos['id'],
				'estado'=>$datos['estado'],
				'observaciones'=>implode("|", $datos)
			];
			$p = new viajesDetalleLogs();
			$p->insert($arreglo);
		}
		return $resultado;
	}

	public function getDetalle($datos){
		$filtro = '';
		if(isset($datos['estado']) and $datos['estado'] == 'Activo'){
			$filtro = "AND viajes_detalle.estado IN ('Gestionar', 'Bloqueado', 'Reservado', 'Devuelto') AND fk_viajes = $datos[viaje]";
		}else{
			foreach ($datos as $key => $value) {
				$filtro .= " AND viajes_detalle.$key = '$value'";
			}
		}
		$sql = "SELECT 
					viajes_detalle.id,
					destinos.nombre AS destino,
					um,
					icono,
					servicios.nombre AS servicio,
					opciones.opcion,					
					proveedores.id AS idp,
					proveedores.nombre AS proveedor,					
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio,
					viajes_detalle.detalle,					
					viajes_detalle.costo,
					viajes_detalle.pasajeros,
					viajes_detalle.cantidad,
					viajes_detalle.verificado,
					viajes_detalle.obs_verificado,
					viajes_detalle.estado,
					viajes_detalle.observaciones
				FROM
					viajes_detalle 
					INNER JOIN (opciones 
					INNER JOIN (((servicios INNER JOIN tipos_servicio ON fk_tipos_servicio = tipos_servicio.id) INNER JOIN proveedores ON fk_proveedores = proveedores.id) 
					INNER JOIN destinos ON fk_destinos = destinos.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE 1 
					$filtro					
				ORDER BY
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getItinerario($datos){		
		$sql = "SELECT					
					viajes_detalle.id,
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio,
					destinos.id AS idDestino,
					destinos.nombre AS destino,
					servicios.fk_tipos_servicio AS ts,
					servicios.nombre AS servicio,
					servicios.dias_en_itinerario AS di,
					servicios.descripciones,
					opciones.opcion,					
					viajes_detalle.pasajeros,
					viajes_detalle.cantidad,
					viajes_detalle.estado
				FROM
					viajes_detalle
					INNER JOIN (opciones
					INNER JOIN (servicios 
					INNER JOIN destinos ON fk_destinos = destinos.id) ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE 
					viajes_detalle.estado IN ('Gestionar', 'Bloqueado', 'Reservado') 
					AND fk_viajes = $datos[viaje]
				ORDER BY
					viajes_detalle.fecha_inicio,
					viajes_detalle.hora_inicio";
		$db = new database();
		$respuesta = $db->ejecutarConsulta($sql);
		for ($i=0; $i < count($respuesta['data']); $i++) { 
			if($respuesta['data'][$i]['descripciones'] != '{}'){
				$respuesta['data'][$i]['descripciones'] = str_replace("\r\n", "\\n", $respuesta['data'][$i]['descripciones']);
			}
		}
		return $respuesta;
	}

	public function getIndicador($datos){
		$sql = "SELECT
					fk_viajes,
					estado,
					COUNT(1) AS cantidad
				FROM 
					viajes_detalle
				WHERE 
					estado IN ('Gestionar','Bloqueado', 'Reservado','Devuelto')
				GROUP BY 
					fk_viajes,estado
				ORDER BY 
					fk_viajes";
		$db = new database();
		return $db->ejecutarConsulta($sql);
	}

	public function getProximoServicio($datos){
		$sql = "SELECT
					fecha_inicio,
					fk_tipos_servicio AS tipo
				FROM 
					viajes_detalle INNER JOIN (opciones INNER JOIN servicios ON fk_servicios = servicios.id) ON fk_opciones = opciones.id
				WHERE
					fk_viajes = $datos[id]
					AND verificado = 0
					AND viajes_detalle.estado = 'Reservado'
				ORDER BY
					fecha_inicio
				LIMIT 1";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}

	public function getValorTotal($datos){
		$sql = "SELECT
					fk_viajes,
					SUM(costo * pasajeros * cantidad) AS valor_total
				FROM
					viajes_detalle
				WHERE
					fk_viajes = $datos[id]
					AND viajes_detalle.estado != 'Cancelado'
				GROUP BY 
					fk_viajes";
		$db = new database();
		return $db->ejecutarConsulta($sql);	
	}
}