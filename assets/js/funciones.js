function procesarRegistro(objeto,metodo,datos,callback){    
    $('#preload').modal('show')
    $.ajax({
        url: 'class/frontController.php',        
        data:{
            objeto: objeto,
            metodo: metodo,
            datos: datos
        },
        type: 'POST',
        dataType: 'json',
        success: function(resultado){
            if(resultado.ejecuto == false){
                if(resultado.codigoError == 1062){
                    swal('Ups!', 'Registro duplicado!!!', 'error')
                }else{
                    swal('Ups!', resultado.msgError, 'error')
                }
            }else{
                callback(resultado)
            }                    
        },
        error: function(xhr,status){
            console.log('Disculpe, existio un problema procesando '+objeto);
        },
        complete: function(xhr,status){
            $('#preload').modal('hide')
        }
    });
}

function llenarFormulario(formulario, objeto, metodo, datos, callback){    
    procesarRegistro(objeto, metodo, datos,function(r){        
        $.each(r.data[0],function(campo,valor){                
            if($('#'+formulario+' #'+campo).is(':checkbox')){
                if(valor == 1){
                    $('#'+formulario+' #'+campo).prop("checked",true)
                }
            }else{
                $('#'+formulario+' #'+campo).val(valor)    
            }
        })
        callback(r)        
    })    
}

function llenarSelect(objeto, metodo, datos, elemento, campo, defecto){
    procesarRegistro(objeto,metodo,datos,function(r){
        if(defecto == 1){
            $("#"+elemento).empty();
            $("#"+elemento).append("<option value=''>Seleccione...</option>");
        }        
        for (i = 0; i < r.data.length; i++){        
            $("#"+elemento).append("<option value="+r.data[i].id+">"+r.data[i][campo]+"</option>")
        }
    })
}

function llenarSelectByClass(objeto, metodo, datos, elemento, campo, defecto){
    procesarRegistro(objeto,metodo,datos,function(r){
        if(defecto == 1){
            $("."+elemento).empty();
            $("."+elemento).append("<option value=''>Seleccione...</option>");
        }        
        for (i = 0; i < r.data.length; i++){        
            $("."+elemento).append("<option value="+r.data[i].id+">"+r.data[i][campo]+"</option>")
        }
    })
}

function parsearFormulario(form) {    
    var arrayForm = $(form).serializeArray();
    var objectForm = {};
    arrayForm.forEach(function (obj, index) {
        objectForm[obj.name] = obj.value;
    });    
    return objectForm;
}

function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (3 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

function validarExistenciaImagen(elemento, imagen, imagenPorDefecto){    
    $.ajax({
        url: imagen,
        type: 'HEAD',
        error: function(){
            $(elemento).attr('src', imagenPorDefecto)
        },
        success: function(){
            $(elemento).attr('src',imagen)
        }    
    })
}

function alertaPorFecha(fecha){
    let x = fecha.split(" ")
    let diff = moment(moment(x[0])).diff(moment().format('YYYY-MM-DD'), 'days')
    if(diff <= 2){
        return '<span class="label label-danger">'+fecha+'</span>'
    }else if(diff <= 5){
        return '<span class="label label-warning">'+fecha+'</span>'
    }else{
        return '<span class="label label-default">'+fecha+'</span>'
    }
}

function pintarTabla(elemento){
    $('#'+elemento).dataTable().fnDestroy();
    tabla = $('#'+elemento).DataTable({
        "lengthMenu": [ 50, 100, 200, 300 ],
        "language":{
            "decimal":        "",
            "emptyTable":     "Sin datos para mostrar",
            "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
            "infoFiltered":   "(Filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "Ningún registro encontrado",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Sig",
                "previous":   "Ant"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "bDestroy": true
        }
    })
}

function sumarElementosArray(array){
    let total = 0
    for(i in array){
        total += array[i]
    }
    return total
}

function construirItinerario(viaje, callback){
    let respuesta = []
    procesarRegistro('viajes', 'select', {id: viaje}, function(r){
        respuesta.push(r)
        let inicio = moment(r.data[0].fecha_inicio, 'YYYY-MM-DD')
        let fin = moment(r.data[0].fecha_fin, 'YYYY-MM-DD')
        let dias = fin.diff(inicio, 'days')
        for (let i = 0; i <= dias; i++) {
            $('#contenidoItinerario').append('<li class="time-label">'+
                                        '<span class="bg-black">'+inicio.format('DD-MMM-YYYY')+
                                        '</span>'+
                                        '<li>'+
                                            '<i class="fa fa-check bg-black"></i>'+
                                            '<div class="timeline-item text-center">'+
                                                '<div class="timeline-body">'+
                                                    '<table class="table table-bordered">'+
                                                        '<thead>'+
                                                            '<tr>'+
                                                                '<th class="text-center">Hora</th>'+
                                                                '<th class="text-center">Destino</th>'+
                                                                '<th class="text-center">Servicio</th>'+
                                                                '<th class="text-center">Opción</th>'+
                                                                '<th class="text-center">Pasajeros</th>'+                                                                
                                                            '</tr>'+
                                                        '</thead>'+
                                                        '<tbody id="contenidoDia_'+inicio.format('YYYY-MM-DD')+'"></tbody>'+
                                                    '</table>'+
                                                '</div>'+
                                            '</div>'+
                                        '</li>'+
                                    '</li>')
            inicio.add(1, 'days')
        }
        procesarRegistro('viajesDetalle', 'getItinerario', {viaje: viaje, estado:'Activo'}, function(r){
            respuesta.push(r)
            let dias_en_itinerario
            let labels = {
                Gestionar: 'bg-gray',                
                Bloqueado: 'bg-amarillo',
                Reservado: 'bg-green',
                Devuelto: 'bg-red'
            }
            let bufferHospedajes = []
            let bufferPaquetes = []
            let centinela
            for(let i = 0; i < r.data.length; i++){
                //Buffer para mostrar hoteles al final
                if(r.data[i].ts == 3){
                    bufferHospedajes.push(r.data[i])
                }else{
                    dias_en_itinerario = r.data[i].di * r.data[i].cantidad
                    inicio_iti = moment(r.data[i].fecha_inicio, 'YYYY-MM-DD')
                    centinela = 0
                    for(j = 0; j< dias_en_itinerario; j++){
                        //Logica para colocar el paquete al final solo el primer día
                        if(r.data[i].ts == 4 && centinela == 0){
                            bufferPaquetes.push(r.data[i])
                            centinela = 1
                        }else{
                            $('#contenidoDia_'+inicio_iti.format('YYYY-MM-DD')).append('<tr class="'+labels[r.data[i].estado]+'">'+
                                '<td>'+r.data[i].hora_inicio+'</td>'+
                                '<td>'+r.data[i].destino+'</td>'+
                                '<td>'+r.data[i].servicio+'</td>'+
                                '<td>'+r.data[i].opcion+'</td>'+
                                '<td>'+r.data[i].pasajeros+'</td>'+                            
                            '</tr>')                            
                        }
                        inicio_iti.add(1, 'days')
                    }    
                }                
            }
            //Se agregan primero los paquetes al final del primer día
            for(let i = 0; i < bufferPaquetes.length; i++){                
                inicio_iti = moment(bufferPaquetes[i].fecha_inicio, 'YYYY-MM-DD')
                $('#contenidoDia_'+inicio_iti.format('YYYY-MM-DD')).append('<tr class="'+labels[bufferPaquetes[i].estado]+'">'+
                        '<td>'+bufferPaquetes[i].hora_inicio+'</td>'+
                        '<td>'+bufferPaquetes[i].destino+'</td>'+
                        '<td>'+bufferPaquetes[i].servicio+'</td>'+
                        '<td>'+bufferPaquetes[i].opcion+'</td>'+
                        '<td>'+bufferPaquetes[i].pasajeros+'</td>'+                            
                    '</tr>')                
            }

            //Aqui se vuelve a ejecutar el codigo que agrega la info, pero como se guardo temporalmente entonces se adiciona al final
            for(let i = 0; i < bufferHospedajes.length; i++){
                dias_en_itinerario = bufferHospedajes[i].di * bufferHospedajes[i].cantidad
                inicio_iti = moment(bufferHospedajes[i].fecha_inicio, 'YYYY-MM-DD')                
                for(j = 0; j< dias_en_itinerario; j++){
                    $('#contenidoDia_'+inicio_iti.format('YYYY-MM-DD')).append('<tr class="'+labels[bufferHospedajes[i].estado]+'">'+
                            '<td>'+bufferHospedajes[i].hora_inicio+'</td>'+
                            '<td>'+bufferHospedajes[i].destino+'</td>'+
                            '<td>'+bufferHospedajes[i].servicio+'</td>'+
                            '<td>'+bufferHospedajes[i].opcion+'</td>'+
                            '<td>'+bufferHospedajes[i].pasajeros+'</td>'+                            
                        '</tr>')
                    inicio_iti.add(1, 'days')
                }    
            }
            callback(respuesta)
        })
    })
}